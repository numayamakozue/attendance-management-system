function restoration(){
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('復元しても宜しいですか？')){
		location.href = "userDisplay";
	}
	// 「キャンセル」時の処理開始
	else{
		window.alert('キャンセルされました');
		return false;
	}
}

function del(){
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('削除しても宜しいですか？')){
		location.href = "userDisplay";
	}
	// 「キャンセル」時の処理開始
	else{
		window.alert('キャンセルされました');
		return false;
	}
}
