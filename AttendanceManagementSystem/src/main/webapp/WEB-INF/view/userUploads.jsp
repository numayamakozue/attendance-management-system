<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>

<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<title>一括登録</title>
<link href="<c:url value="/resources/css/userUploads.css"/>" rel="stylesheet">
<script src="<c:url value="/resources/js/userUploads.js"/>"></script>
<%@ include file="header.jsp"%>
<%@ include file="subMenuUserManagement.jsp" %>
</head>

<body>
	<fieldset>
		<div class="error" id="error">
		<b>${unRegisterMessage}</b>
		</div>

		<div class="register" id="register">
		<b>${userRegisterMessage}</b>
		</div>

		<div class="mainTable" >
			 ユーザ情報をCSVで登録&nbsp;&nbsp;
			<form action="useruploads" method="post" enctype="multipart/form-data">
			<input type="file" name="upload_file"  id ="upload_file" value="" accept=".csv" size="50"/>
			<button type="submit" class="btn" id="upload" onclick="clickBtn1()">CSV登録</button>
			</form>



		<div class="uploadUsers" id="uploadUsers">
		<c:if test = "${register !=null}" >
			<table>
				<thead>
					<tr>
						<th nowrap>社員番号</th>
						<th nowrap>入社日</th>
						<th nowrap>名前</th>
						<th nowrap>部署</th>
						<th nowrap>役職</th>
						<th nowrap>権限</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach items="${register}" var="register">
						<tr>
							<td nowrap><div class ="idNo"><c:out value="${register.employeeNumber}"></c:out></div></td>
							<td nowrap><div class ="joinDay"><c:out value="${register.stringDireDate}"></c:out></div></td>
							<td nowrap><div class="name"><c:out value="${register.name}"></c:out></div></td>
							<td nowrap><div class="department"><c:out value="${register.department}"></c:out></div></td>
							<td nowrap><div class="position"><c:out value="${register.position}"></c:out></div></td>
							<c:if test="${register.roleId==1}">
							<td nowrap><c:out value="〇"></c:out></td>
							</c:if>
							<c:if test="${register.roleId==2}">
								<td nowrap><c:out value="　"></c:out></td>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</c:if>
			</div>
		</div>
	</fieldset>
</body>
</html>