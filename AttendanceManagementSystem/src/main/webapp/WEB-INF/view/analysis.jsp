<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>分析画面</title>
<link href="<c:url value="/resources/css/analysis.css"/>" rel="stylesheet">

<script type="text/javascript" language="javascript">

var pretime = ${pretime};
var over1 = ${over1};
var over2 = ${over2};

window.onload = function timeCheck(){

	for (var i = 1; i < pretime; i++) {
		var timeCheck0 = document.getElementById('actual' + i);
		var actStr = timeCheck0.textContent;

			if (over1 <= actStr){
				var timeCheck1 = document.getElementById('employeeNumber' + i);
				var timeCheck2 = document.getElementById('name' + i);
				var timeCheck3 = document.getElementById('actual' + i);
				var timeCheck4 = document.getElementById('late' + i);
				var timeCheck5 = document.getElementById('rest' + i);
				var timeCheck6 = document.getElementById('sum1' + i);
				var timeCheck7 = document.getElementById('sum2' + i);
				var timeCheck8 = document.getElementById('sum3' + i);
				var timeCheck9 = document.getElementById('sum4' + i);
				var timeCheck10 = document.getElementById('sum5' + i);
				var timeCheck11 = document.getElementById('sum6' + i);
				var timeCheck12 = document.getElementById('sum7' + i);
				var timeCheck13 = document.getElementById('sum8' + i);
				timeCheck1.style.background = '#FFFF66';
				timeCheck2.style.background = '#FFFF66';
				timeCheck3.style.background = '#FFFF66';
				timeCheck4.style.background = '#FFFF66';
				timeCheck5.style.background = '#FFFF66';
				timeCheck6.style.background = '#FFFF66';
				timeCheck7.style.background = '#FFFF66';
				timeCheck8.style.background = '#FFFF66';
				timeCheck9.style.background = '#FFFF66';
				timeCheck10.style.background = '#FFFF66';
				timeCheck11.style.background = '#FFFF66';
				timeCheck12.style.background = '#FFFF66';
				timeCheck13.style.background = '#FFFF66';
			}

			if (over2 <= actStr){
				var timeCheck1 = document.getElementById('employeeNumber' + i);
				var timeCheck2 = document.getElementById('name' + i);
				var timeCheck3 = document.getElementById('actual' + i);
				var timeCheck4 = document.getElementById('late' + i);
				var timeCheck5 = document.getElementById('rest' + i);
				var timeCheck6 = document.getElementById('sum1' + i);
				var timeCheck7 = document.getElementById('sum2' + i);
				var timeCheck8 = document.getElementById('sum3' + i);
				var timeCheck9 = document.getElementById('sum4' + i);
				var timeCheck10 = document.getElementById('sum5' + i);
				var timeCheck11 = document.getElementById('sum6' + i);
				var timeCheck12 = document.getElementById('sum7' + i);
				var timeCheck13 = document.getElementById('sum8' + i);
				timeCheck1.style.background = '#FFB6C1';
				timeCheck2.style.background = '#FFB6C1';
				timeCheck3.style.background = '#FFB6C1';
				timeCheck4.style.background = '#FFB6C1';
				timeCheck5.style.background = '#FFB6C1';
				timeCheck6.style.background = '#FFB6C1';
				timeCheck7.style.background = '#FFB6C1';
				timeCheck8.style.background = '#FFB6C1';
				timeCheck9.style.background = '#FFB6C1';
				timeCheck10.style.background = '#FFB6C1';
				timeCheck11.style.background = '#FFB6C1';
				timeCheck12.style.background = '#FFB6C1';
				timeCheck13.style.background = '#FFB6C1';
			}
	}
};
</script>
<%@ include file="header.jsp" %><br>
<%@ include file="subMenuAttendanceManagement.jsp" %>
</head>

<body>
	<fieldset>
		<div class="mainTable">
            <div class = "pullDown" >
			<table border="1">


				<!-- カレンダー情報を取得し、プルダウン表示 -->
				<form:form modelAttribute="selectMonth1">
				<b class = "selectMonth1"><span>カレンダー選択：</span></b>
				<select name="monthKey">
						<c:forEach items="${selectMonthPullDowns}" var="selectMonthPullDowns">
							<c:if test="${selectMonthPullDowns.nowMonth == selectMonthPullDowns.monthKey}">
								<option value="${selectMonthPullDowns.monthKey}" selected><c:out value="${selectMonthPullDowns.monthName}"></c:out></option>
							</c:if>
							<c:if test="${selectMonthPullDowns.nowMonth != selectMonthPullDowns.monthKey }">
								<option value="${selectMonthPullDowns.monthKey}"><c:out value="${selectMonthPullDowns.monthName}"></c:out></option>
							</c:if>
						</c:forEach>
				</select>&nbsp;
					<input class = "updateButton" type="submit" formaction="selectMonth1" value="更新">
					</form:form>

				<div class = "aggregateTime">
				<table class ="table1">
					<thead>
						<tr>
							<th nowrap><span>所定時間</span></th>
							<th nowrap><span>所定日数</span></th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td><c:out value="${pretime}時間"></c:out></td>
							<td><c:out value="${preday}日"></c:out></td>
						</tr>
					</tbody>
				</table>
			</table><br>&nbsp;<br>&nbsp;<br>&nbsp;<br>

			<form:form modelAttribute="userForm" method="get">
					<div class="button_wrapper">
					<div class ="btnLeft">
					<input type="hidden" name="pageNumber" value="${pageNumber}">
						<button  class="b1" type="submit" name="isPrevPage" value="isPrevPage">＜前のページ</button></div>
						&nbsp;&nbsp;
						<div class ="btnRight">
						<button  class="b1" type="submit" name="isNextPage" value="isNextPage">次のページ＞</button></div>
					</div>
				</form:form>
					<table>
					<thead class = "header1">
						<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th colspan="7"> 日数 </th>
						</tr>
						</thead>
						<thead class = "header2">
						<tr>
							<th nowrap>社員番号</th>
						    <th nowrap>名前</th>
						    <th nowrap>合計勤務時間</th>
							<th nowrap>深夜時間</th>
							<th nowrap>休憩時間</th>
							<th nowrap>合計勤務日数</th>
							<th nowrap><div class ="day">欠勤</div></th>
							<th nowrap><div class = "day">有給</div></th>
							<th nowrap><div class = "day">夏季休暇</div></th>
							<th nowrap><div class = "day">半休</div></th>
							<th nowrap><div class = "day">育児休暇</div></th>
							<th nowrap><div class = "day">代休</div></th>
							<th nowrap><div class = "day">慶弔休暇</div></th>
						</tr>
						</thead>
						<tbody>
						<% int i = 1;%>
						<c:forEach items="${size}" var="size">
						<tr>
						    <td id= "employeeNumber<%=i%>"><c:out value="${size.employeeNumber}"></c:out></td>
							<td id= "name<%=i%>"><div class="name"><c:out value="${size.name}"></c:out></div></td>
						    <td id= "actual<%=i%>"><c:out value="${size.actual}"></c:out></td>
							<td id= "late<%=i%>"><c:out value="${size.late}"></c:out></td>
							<td id= "rest<%=i%>"><c:out value="${size.rest}"></c:out></td>
							<td id= "sum1<%=i%>"><c:out value="${size.sum1}"></c:out></td>
							<td id= "sum2<%=i%>"><c:out value="${size.sum2}"></c:out></td>
							<td id= "sum3<%=i%>"><c:out value="${size.sum3}"></c:out></td>
							<td id= "sum4<%=i%>"><c:out value="${size.sum4}"></c:out></td>
							<td id= "sum5<%=i%>"><c:out value="${size.sum5}"></c:out></td>
							<td id= "sum6<%=i%>"><c:out value="${size.sum6}"></c:out></td>
							<td id= "sum7<%=i%>"><c:out value="${size.sum7}"></c:out></td>
							<td id= "sum8<%=i%>"><c:out value="${size.sum8}"></c:out></td>
						</tr>
						<%i++; %>
						</c:forEach>
					</tbody>
				</table>
				</div>
		</div>
		</div>
	</fieldset>
</body>

</html>