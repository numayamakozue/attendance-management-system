<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link href="<c:url value="/resources/css/userDisplay.css"/>" rel="stylesheet">
<script src="<c:url value="/resources/js/userDisplay.js"/>"></script>
<%@ include file="header.jsp"%><br>
<%@ include file="subMenuUserManagement.jsp" %>
</head>

<body>
	<fieldset>
		<div class="mainTable">


				<form:form modelAttribute="userForm" method="get">
					<div class="button_wrapper">
					<c:if test="${isDelete == 1}">
					<input type="checkbox" name="isDelete" value="1" checked="checked" />削除されたユーザーの表示はチェックを入れて検索<br />
					</c:if>
					<c:if test="${isDelete == null}">
					<input type="checkbox" name="isDelete" value="1" />削除されたユーザーの表示はチェックを入れて検索<br />
					</c:if>
					社員番号&nbsp;&nbsp;<form:input path="employeeNumber" />&nbsp;&nbsp;&nbsp;
					名前&nbsp;&nbsp;<form:input path="name" />&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;
					<input class="p1" type="submit" value="検索"><br />
					<div class ="btnLeft">
					<input type="hidden" name="pageNumber" value="${pageNumber}">
						<button  class="b1" type="submit" name="isPrevPage" value="isPrevPage">＜前のページ</button></div>
					<div class ="btnRight">
						<button  class="b1" type="submit" name="isNextPage" value="isNextPage">次のページ＞</button></div>
					</div>
				</form:form>

				<table>
				<thead>
					<tr>
						<th nowrap>社員番号</th>
						<th nowrap>入社日</th>
						<th nowrap>名前</th>
						<th nowrap>部署</th>
						<th nowrap>役職</th>
						<th nowrap>権限</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach items="${users}" var="users">
						<c:if test="${users.isDelete == 0}">
							<tr>
								<td nowrap><div class ="idNo"><c:out value="${users.employeeNumber}"></c:out></div></td>
								<td nowrap><div class ="joinDay"><c:out value="${users.direDate}"></c:out></div></td>
								<td nowrap><div class="name"><c:out value="${users.name}"></c:out></div></td>
								<td nowrap><div class="department"><c:out value="${users.department}"></c:out></div></td>
								<td nowrap><div class="position"><c:out value="${users.position}"></c:out></div></td>
								<c:if test="${users.roleId==1}">
								<td nowrap><c:out value="〇"></c:out></td>
								</c:if>
								<c:if test="${users.roleId==2}">
								<td nowrap><c:out value="　"></c:out></td>
								</c:if>
								<td nowrap><a href="${pageContext.request.contextPath}/userDetails/update/${users.id}/">編集</a></td>
								<form:form modelAttribute="users" method="post" onsubmit="return del()">
									<input  type="hidden" name="id" value="${users.id}" />
									<input type="hidden" name="loginUser" value="${loginUser.id}"/>
									<td nowrap><input class="s1" type="submit" formaction="delete" value="削除" ></td>
								</form:form>
							</tr>
						</c:if>
						<c:if test="${users.isDelete == 1}">
							<tr>
								<td nowrap><c:out value="${users.employeeNumber}"></c:out></td>
								<td nowrap><c:out value="${users.direDate}"></c:out></td>
								<td nowrap><div class="name"><c:out value="${users.name}"></c:out></div></td>
								<td nowrap><div class="department"><c:out value="${users.department}"></c:out></div></td>
								<td nowrap><div class="position"><c:out value="${users.position}"></c:out></div></td>
								<c:if test="${users.roleId==1}">
								<td nowrap><c:out value="〇"></c:out></td>
								</c:if>
								<c:if test="${users.roleId==2}">
								<td nowrap><c:out value="　"></c:out></td>
								</c:if>
								<form:form modelAttribute="users" method="post" onsubmit="return restoration()">
									<input  type="hidden" name="id" value="${users.id}" />
									<input type="hidden" name="loginUser" value="${loginUser.id}"/>
									<td nowrap><input class="s1" type="submit" formaction="restorastion" value="復元" ></td>
								</form:form>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</fieldset>
</body>
</html>