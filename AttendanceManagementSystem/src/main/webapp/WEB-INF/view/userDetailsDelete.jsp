<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除画面</title>
<link href="<c:url value="/resources/css/userDetailsDelete.css"/>" rel="stylesheet">
</head>
<body>
	<%@ include file="header.jsp"%>
	<h2 class="delete_title">このユーザを削除してもよろしいですか?</h2>
	<form:form modelAttribute="userInfoDeleteForm">
		<dl>
			<dt>■社員番号:</dt>
			<dd>
				<c:out value="${deleteForm.employeeNumber}"></c:out>
			</dd>

			<dt>■名前:</dt>
			<dd>
				<c:out value="${deleteForm.name}"></c:out>
			</dd>
		</dl>
		<input class="delete_submit" type="submit" value="削除">
	</form:form>
</body>
</html>