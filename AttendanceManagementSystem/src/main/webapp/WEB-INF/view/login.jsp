<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link href="<c:url value="/resources/css/login.css"/>" rel="stylesheet">
</head>
<body>
	<fieldset>
		<h1>
			<c:out value="ログイン画面"></c:out>
		</h1>
		<f:form modelAttribute="loginForm">
			<div class="error">${errormessage}</div>
			<table>
				<tbody>
					<tr>
						<th>社員番号</th>
						<td><f:input path="employeeNumber" type="text" value="${loginForm.employeeNumber}"/>
					</tr>
					<tr>
						<th>パスワード</th>
						<td><input name="password" id="password" type="password"  /></td>
					</tr>
				</tbody>
			</table>
			<div class="button_wrapper">
				<input type="submit" value="ログイン" />
			</div>
		</f:form>
	</fieldset>
</body>
</html>

