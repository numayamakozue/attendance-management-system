<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ管理</title>
<link href="<c:url value="/resources/css/subMenuAttendanceManagement.css"/>" rel="stylesheet">
<script type="text/javascript" language="javascript">
</script>
</head>
<body>
<br><br><br><br><br>
<h3>勤怠管理画面</h3>
<form:form modelAttribute="userForm" method="get">
	<div class="tab-wrap">
		<c:if test="${checkedFlg == 3}">
			<input id="TAB-02" type="radio" name="TAB" class="tab-switch" value="1" checked="checked" onClick="window.location.href='${pageContext.request.contextPath}/userAttendance'"/>
   			<label class="tab-label" for="TAB-02">各社員勤怠登録</label>
        	<input id="TAB-01" type="radio" name="TAB" class="tab-switch" value="2" onClick="window.location.href='${pageContext.request.contextPath}/analysis'" />
       		<label class="tab-label" for="TAB-01">勤怠分析</label>
   			<input id="TAB-03" type="radio" name="TAB" class="tab-switch" value="3" onClick="#'" />
    		<label class="tab-label" for="TAB-03">勤怠未入力者チェック</label>
    	</c:if>
    	<c:if test="${checkedFlg == 4}">
			<input id="TAB-02" type="radio" name="TAB" class="tab-switch" value="1" onClick="window.location.href='${pageContext.request.contextPath}/userAttendance'"/>
   			<label class="tab-label" for="TAB-02">各社員勤怠登録</label>
        	<input id="TAB-01" type="radio" name="TAB" class="tab-switch" value="2" checked="checked" onClick="window.location.href='${pageContext.request.contextPath}/analysis'" />
       		<label class="tab-label" for="TAB-01">勤怠分析</label>
   			<input id="TAB-03" type="radio" name="TAB" class="tab-switch" value="3" onClick="#'" />
    		<label class="tab-label" for="TAB-03">勤怠未入力者チェック</label>
    	</c:if>
    	<c:if test="${checkedFlg == 5}">
			<input id="TAB-02" type="radio" name="TAB" class="tab-switch" value="1" onClick="window.location.href='${pageContext.request.contextPath}/userAttendance'"/>
   			<label class="tab-label" for="TAB-02">各社員勤怠登録</label>
        	<input id="TAB-01" type="radio" name="TAB" class="tab-switch" value="2" onClick="window.location.href='${pageContext.request.contextPath}/analysis'" />
       		<label class="tab-label" for="TAB-01">勤怠分析</label>
   			<input id="TAB-03" type="radio" name="TAB" class="tab-switch" value="3" checked="checked" onClick="#'" />
    		<label class="tab-label" for="TAB-03">勤怠未入力者チェック</label>
    	</c:if>
</div>
</form:form>
</body>
</html>