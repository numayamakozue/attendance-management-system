<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>

<html>
<head>
<meta charset="utf-8">
<title>ユーザ詳細画面</title>
<link href="<c:url value="/resources/css/userDetails.css"/>" rel="stylesheet">
<script src="<c:url value="/resources/js/userRegister.js"/>"></script>
</head>
<body>
	<%@ include file="header.jsp"%>
	<h2 class="userDetails_title">ユーザ詳細画面</h2>
	<dl class="details_dl">
		<dt>■社員番号：</dt>
		<dd>
			<c:out value="${details.employeeNumber}"></c:out>
		</dd>

		<dt>■名前 :</dt>
		<dd>
			<c:out value="${details.name}"></c:out>
		</dd>
		<dt>■入社日：</dt>
		<dd>
			<c:out value="${details.dire_date}"></c:out>
		</dd>

		<dt>■部署：</dt>
		<dd>
			<c:out value="${details.department}"></c:out>
		</dd>

		<dt>■役職：</dt>
		<dd>
			<c:out value="${details.position}"></c:out>
		</dd>

		<dt>■権限：</dt>
		<dd>
			<c:out value="${details.role}"></c:out>
		</dd>
	</dl>

	<c:if test="${loginUser.id != details.id}">
		<p>

			<a class="userDetails_button"
				href="${pageContext.request.contextPath}/userDetails/update/${details.id}/">ユーザ編集画面</a>
		</p>
		<p>
			<a class="userDetails_button"
				href="${pageContext.request.contextPath}/userDetails/delete/${details.id}/">ユーザ削除画面</a>
		</p>
	</c:if>

</body>
</html>