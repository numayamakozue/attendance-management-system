<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>
<html>
<head>
<meta charset="utf-8">
<title>ユーザ登録</title>
<link href="<c:url value="/resources/css/userRegister.css"/>" rel="stylesheet">
<script src="<c:url value="/resources/js/userRegister.js"/>"></script>
<%@ include file="header.jsp"%><br>
<%@ include file="subMenuUserManagement.jsp" %>
</head>
<body>
	<fieldset>
	<div class = "wrapper">
		<f:form modelAttribute="userRegisterForm">
		<form action="userRegister" method="post">
			<h4>
				<c:out value="ユーザ新規登録"></c:out>
			</h4>
			<div class="error" id="error">
				<b>${title}</b><br> <b>${unRegisterMessage}</b>

			</div>

			<table>
				<tbody>

					<tr>
						<th>社員番号</th>
						<td><input name="employeeNumber" id="employeeNumber"  onblur="employeeNumberError();" type="text" value="${userRegisterForm.employeeNumber}" autofocus title="社員番号は半角数字6文字で入力してください"></td>
						<f:errors path="employeeNumber" element="div" />
					</tr>
					<tr>
						<th>パスワード</th>
						<td><input name="password" id="password" onblur="passwordError();" type="password" value="" autofocus title="パスワードは半角英数字(記号含む)6文字以上20文字以内で入力してください"></td>
						<f:errors path="password" element="div" />
					</tr>
					<tr>
						<th>名前</th>
						<td><input name="name" id="name" onblur="nameError();" type="text" value="${userRegisterForm.name}" autofocus title="全角文字(ひらがな、カタカナ、漢字)1文字以上30文字以内で入力してください"></td>
						<f:errors path="name" element="div" />
					</tr>
					<tr>
						<th>入社日</th>
						<td><input name="direDate" id="direDate" onblur="direDateError();" type="date" value="${userRegisterForm.direDate}" autofocus title="入社日を入力してください"></td>
						<f:errors path="direDate" element="div" />
					</tr>
					<tr>
						<th>部署</th>
						<td><select name="departmentId">
								<c:forEach items="${departments}" var="departments">
									<c:if test="${departments.id == userRegisterForm.departmentId}">
										<option value="${departments.id}" selected>${departments.department}</option>

									</c:if>
									<c:if
										test="${departments.id != userRegisterForm.departmentId }">
										<option value="${departments.id}">${departments.department}</option>
									</c:if>
								</c:forEach>
						</select></td>
					</tr>
					<tr>
						<th>役職</th>
						<td><select name="positionId">
								<c:forEach items="${positions}" var="positions">
									<c:if test="${positions.id == userRegisterForm.positionId}">
										<option value="${positions.id}" selected>${positions.position}</option>

									</c:if>
									<c:if test="${positions.id != userRegisterForm.positionId }">
										<option value="${positions.id}">${positions.position}</option>
									</c:if>
								</c:forEach>
						</select></td>
					</tr>
					<tr>
						<th>権限</th>
						<td><select name="roleId">
								<c:forEach items="${roles}" var="roles">
									<c:if test="${roles.id == userRegisterForm.roleId}">
										<option value="${roles.id}" selected>${roles.role}</option>
								</c:if>
								<c:if test="${roles.id != userRegisterForm.roleId }">
									<option value="${roles.id}">${roles.role}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
				</tbody>
			</table>
			<div class="button_wrapper">
				<input type="submit" value="登録" />
			</div>
		</form>
		</f:form>
		</div>
	</fieldset>
</body>
</html>