<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>

<html>
<head>
<meta charset="utf-8">
<title>ユーザ編集画面</title>
<link href="<c:url value="/resources/css/userDetailsEdit.css"/>" rel="stylesheet">
<script src="<c:url value="/resources/js/userDetailsEdit.js"/>"></script>
<%@ include file="header.jsp"%><br>
<%@ include file="subMenuUserManagement.jsp" %>
</head>
<body>
	<div class = "wrapper">
	<fieldset>
		<form:form modelAttribute="userInfoUpdateForm">
		<form:input type="hidden" path="loginUser" value="${loginUser.id}"/>

			<h4 class="edit_title">ユーザ編集</h4>
			<div class="error" id="error">

			<b style="color: red;">${error_message}</b>
			<br><form:errors  path="*" />
			</div>

			<dl>
			<table>
			<tbody>
			<dt>社員番号</dt>
			<dd>
				<form:input path="employeeNumber" onblur="employeeNumberError();" value="${userInfoUpdate.employeeNumber}"/>
			</dd>

			<dt>パスワード</dt>
			<dd>
				<form:input path="password" onblur="passwordError();" value="${userInfoUpdate.password}"/>
			</dd>


			<dt>名前</dt>
			<dd>
				<form:input path="name" onblur="nameError();" value="${userInfoUpdate.name}" />
			</dd>

			<dt>入社日</dt>
			<dd>
				<form:input type="date" path="dire_date" id="direDate" onblur="direDateError();" value="${userInfoUpdate.direDate}" />
			</dd>

			<dt>部署</dt>
			<dd>
				<select name="department_id">
					<c:forEach items="${departments}" var="de">
						<c:if test="${de.id == userInfoUpdateForm.department_id}">
							<option value="${de.id}" selected>${de.department}</option>
						</c:if>
						<c:if test="${de.id != userInfoUpdateForm.department_id}">
							<option value="${de.id}">${de.department}</option>
						</c:if>
					</c:forEach>
				</select>
			</dd>
			<dt>役職</dt>
			<dd>
				<select name="position_id">
					<c:forEach items="${positions}" var="po">
						<c:if test="${po.id == userInfoUpdateForm.position_id}">
							<option value="${po.id}" selected>${po.position}</option>

						</c:if>
						<c:if test="${po.id != userInfoUpdateForm.position_id }">
							<option value="${po.id}">${po.position}</option>
						</c:if>
					</c:forEach>
				</select>
			</dd>
			<dt>権限</dt>
			<dd>
				<select name="role_id">
					<c:forEach items="${roles}" var="ro">
						<c:if test="${ro.id == userInfoUpdateForm.role_id}">
							<option value="${ro.id}" selected>${ro.role}</option>
						</c:if>
						<c:if test="${ro.id != userInfoUpdateForm.role_id}">
							<option value="${ro.id}">${ro.role}</option>
						</c:if>
					</c:forEach>
				</select>
			</dd>
			</tbody>
			</table>
		</dl>
		<div class="button_wrapper">
		<input type="submit" value="編集">
		</div>
	</form:form>
	</fieldset>
	</div>
</body>
</html>