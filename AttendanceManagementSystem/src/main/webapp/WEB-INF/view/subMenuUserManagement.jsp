<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ管理</title>
<link href="<c:url value="/resources/css/subMenuUserManagement.css"/>" rel="stylesheet">
</head>
<body>
<br><br><br><br><br>
<h3>ユーザ管理画面</h3>
<form:form modelAttribute="userForm" method="get">
	<div class="tab-wrap">
   		<c:if test="${checkedFlg == 0}">
			<input id="TAB-01" type="radio" name="TAB" class="tab-switch" value="1"  checked="checked"  onClick="window.location.href='${pageContext.request.contextPath}/userDisplay'" />
       		<label class="tab-label" for="TAB-01">ユーザ一覧</label>
    		<input id="TAB-02" type="radio" name="TAB" class="tab-switch" onClick="window.location.href='${pageContext.request.contextPath}/userRegister'"/>
   			<label class="tab-label" for="TAB-02">新規登録</label>
   			<input id="TAB-03" type="radio" name="TAB" class="tab-switch" onClick="window.location.href='${pageContext.request.contextPath}/useruploads'"/>
   			<label class="tab-label" for="TAB-03">一括登録</label>
		</c:if>

		<c:if test="${checkedFlg == 1}">
			<input id="TAB-01" type="radio" name="TAB" class="tab-switch" value="2"  onClick="window.location.href='${pageContext.request.contextPath}/userDisplay'" />
       		<label class="tab-label" for="TAB-01">ユーザ一覧</label>
    		<input id="TAB-02" type="radio" name="TAB" class="tab-switch" checked="checked" onClick="window.location.href='${pageContext.request.contextPath}/userRegister'"/>
   			<label class="tab-label" for="TAB-02">新規登録</label>
   			<input id="TAB-03" type="radio" name="TAB" class="tab-switch" onClick="window.location.href='${pageContext.request.contextPath}/useruploads'"/>
   			<label class="tab-label" for="TAB-03">一括登録</label>
		</c:if>
		<c:if test="${checkedFlg == 2}">
			<input id="TAB-01" type="radio" name="TAB" class="tab-switch" value="3"  onClick="window.location.href='${pageContext.request.contextPath}/userDisplay'" />
       		<label class="tab-label" for="TAB-01">ユーザ一覧</label>
    		<input id="TAB-02" type="radio" name="TAB" class="tab-switch" onClick="window.location.href='${pageContext.request.contextPath}/userRegister'"/>
   			<label class="tab-label" for="TAB-02">新規登録</label>
   			<input id="TAB-03" type="radio" name="TAB" class="tab-switch" checked="checked" onClick="window.location.href='${pageContext.request.contextPath}/useruploads'"/>
   			<label class="tab-label" for="TAB-03">一括登録</label>
		</c:if>
	</div>
</form:form>
</body>
</html>
