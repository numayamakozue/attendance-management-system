<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>各社員勤怠管理画面</title>
<link href="<c:url value="/resources/css/userAttendance.css"/>" rel="stylesheet">
<script src="<c:url value="/resources/js/userDisplay.js"/>"></script>
<%@ include file="header.jsp"%><br>
<%@ include file="subMenuAttendanceManagement.jsp" %>
</head>

<body>
	<fieldset>
		<div class="mainTable">

				<form:form modelAttribute="userForm" method="get">
					<div class="button_wrapper">
					社員番号&nbsp;&nbsp;<form:input path="employeeNumber" />&nbsp;&nbsp;&nbsp;
					名前&nbsp;&nbsp;<form:input path="name" />&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;
					<input class="p1" type="submit" value="検索"><br />
					<div class ="btnLeft">
					<input type="hidden" name="pageNumber" value="${pageNumber}">
						<button  class="b1" type="submit" name="isPrevPage" value="isPrevPage">＜前のページ</button></div>
					<div class ="btnRight">
						<button  class="b1" type="submit" name="isNextPage" value="isNextPage">次のページ＞</button></div>
					</div>
				</form:form>

				<table>
				<thead>
					<tr>
						<th nowrap>社員番号</th>
						<th nowrap>名前</th>
						<th nowrap>部署</th>
						<th nowrap>役職</th>
						<th nowrap>権限</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach items="${users}" var="users">
						<c:if test="${users.isDelete == 0}">
							<tr>
								<td nowrap><div class ="idNo"><c:out value="${users.employeeNumber}"></c:out></div></td>
								<td nowrap><div class="name"><a href="${pageContext.request.contextPath}/attendanceRegistration?id=${users.id}">
								<c:out value="${users.name}"></c:out></a></div></td>
								<td nowrap><div class="department"><c:out value="${users.department}"></c:out></div></td>
								<td nowrap><div class="position"><c:out value="${users.position}"></c:out></div></td>
								<c:if test="${users.roleId==1}">
								<td nowrap><c:out value="〇"></c:out></td>
								</c:if>
								<c:if test="${users.roleId==2}">
								<td nowrap><c:out value="　"></c:out></td>
								</c:if>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</fieldset>
</body>
</html>
