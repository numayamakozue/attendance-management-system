<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>勤怠登録画面</title>
<link href="<c:url value="/resources/css/attendanceRegistration.css"/>" rel="stylesheet">
<script type="text/javascript" language="javascript">
function func() {
    var s = document.attendanceUpdate.approval1.checked;
    //true,falseで取得できます
    if (s == true) {
    sessionStorage.setItem('key', s);
    }
}

var monthName = ${monthName};
var day = ${day+1};
var mon =  ${mon};
var date = ${date};
window.onload = function nullCheck(){
	if (monthName < mon) {
		for (var i = 1; i < day; i++) {
			var nullCheckArea1 = document.getElementById('calendar' + i);
			var calStr = nullCheckArea1.textContent;
			if (calStr.length == 6) {
					var a = calStr.slice(0, 2)
					var b = '0'
					var c = calStr.slice(2)
					calStr = a + b + c
			};

			var dateResult = calStr.substr( 2, 2 );
			if (dateResult < day){
				var nullCheckArea2 = document.getElementById('selectBox' + i);
				var nullCheckArea3 = document.getElementById('start' + i);
				var nullCheckArea4 = document.getElementById('end' + i);
				var nullCheckArea5 = document.getElementById('breakstart' + i);
				var nullCheckArea6 = document.getElementById('breakend' + i);
				var nullCheckArea7 = document.getElementById('wkHours' + i);
				var nullCheckArea8 = document.getElementById('restTime' + i);
				var nullCheckArea9 = document.getElementById('nightTime' + i);
				var nullCheckArea10 = document.getElementById('remarks' + i);
				nullCheckArea1.style.background = 'LightPink';
				nullCheckArea2.style.background = 'LightPink';
				nullCheckArea3.style.background = 'LightPink';
				nullCheckArea4.style.background = 'LightPink';
				nullCheckArea5.style.background = 'LightPink';
				nullCheckArea6.style.background = 'LightPink';
				nullCheckArea7.style.background = 'LightPink';
				nullCheckArea8.style.background = 'LightPink';
				nullCheckArea9.style.background = 'LightPink';
				nullCheckArea10.style.background = 'LightPink';
			}
		}
	}
	if (monthName == mon) {
		for (var i = 1; i < date; i++) {
			var nullCheckArea1 = document.getElementById('calendar' + i);
			var calStr = nullCheckArea1.textContent;
			if (calStr.length == 6) {
					var a = calStr.slice(0, 2)
					var b = '0'
					var c = calStr.slice(2)
					calStr = a + b + c
			};

			var dateResult = calStr.substr( 2, 2 );
			if (dateResult < date){
				var nullCheckArea2 = document.getElementById('selectBox' + i);
				var nullCheckArea3 = document.getElementById('start' + i);
				var nullCheckArea4 = document.getElementById('end' + i);
				var nullCheckArea5 = document.getElementById('breakstart' + i);
				var nullCheckArea6 = document.getElementById('breakend' + i);
				var nullCheckArea7 = document.getElementById('wkHours' + i);
				var nullCheckArea8 = document.getElementById('restTime' + i);
				var nullCheckArea9 = document.getElementById('nightTime' + i);
				var nullCheckArea10 = document.getElementById('remarks' + i);
				nullCheckArea1.style.background = 'LightPink';
				nullCheckArea2.style.background = 'LightPink';
				nullCheckArea3.style.background = 'LightPink';
				nullCheckArea4.style.background = 'LightPink';
				nullCheckArea5.style.background = 'LightPink';
				nullCheckArea6.style.background = 'LightPink';
				nullCheckArea7.style.background = 'LightPink';
				nullCheckArea8.style.background = 'LightPink';
				nullCheckArea9.style.background = 'LightPink';
				nullCheckArea10.style.background = 'LightPink';
			}
		}
	}
};

//更新のゼロパディング
function startHUpdate(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function startMUpdate(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function endHUpdate(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function endMUpdate(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function breakHUpdate(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function breakMUpdate(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function endBreakHUpdate(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function endBreakMUpdate(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}

//登録のゼロパディング
function startH(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function startM(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function endH(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function endM(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function breakH(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function breakM(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function endBreakH(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
function endBreakM(numInput) {
  numInput.value = ("00" + numInput.value).slice(-2);
}
</script>
<%@ include file="header.jsp" %>
</head>

<body>
	<div class = "wrapper">
	<br><br><br><br><br><br>
	<div class = "title">
		<h3>勤怠登録画面</h3>
		&nbsp;&nbsp;&nbsp;&nbsp;表示中ユーザ：<c:out value="${users.employeeNumber}"></c:out>
		<c:out value="${users.name}"></c:out>

		<button type="submit" class="btn"
			onclick="location.href='${pageContext.request.contextPath}/csvDownloads?id=${users.id}'">CSV出力</button>
	</div>
	<fieldset>
	<div class="errMessage">
		<c:forEach items="${errMessage}" var="errMessage"><br>
			<c:out value="${errMessage}"></c:out>
		</c:forEach>
	</div>
	<br>
	<div class = "main">
		<div class = "subMain">
			<div class = "pullDown" >

				<!-- カレンダー情報を取得し、プルダウン表示 -->
				<div style="display:inline-flex">
				<form:form modelAttribute="selectMonth" method="get">
					<b class = "selectMonth"><span>カレンダー選択：</span></b>
					<select name="monthKey">
						<c:forEach items="${selectMonthPullDowns}" var="selectMonthPullDowns">
							<c:if test="${selectMonthPullDowns.nowMonth2 == selectMonthPullDowns.monthKey}">
								<option value="${selectMonthPullDowns.monthKey}" selected><c:out value="${selectMonthPullDowns.monthName2}"></c:out></option>
							</c:if>
							<c:if test="${selectMonthPullDowns.nowMonth2 != selectMonthPullDowns.monthKey}">
								<option value="${selectMonthPullDowns.monthKey}"><c:out value="${selectMonthPullDowns.monthName2}"></c:out></option>
							</c:if>
						</c:forEach>
					</select>&nbsp;
					<input type="hidden" name="id" value="${users.id}"/>
					<button  class="updateButton" type="submit" name="isUpdateMonth" value="isUpdateMonth">更新</button>
				</form:form>&nbsp;&nbsp;
				<form:form modelAttribute="selectMonth" method="get">
					<input type="hidden" name="id" value="${users.id}"/>
					<button  class="updateButton" type="submit" name="isPrevMonth" value="isPrevMonth">＜前月</button>
					&nbsp;&nbsp;
					<button  class="updateButton" type="submit" name="isNextMonth" value="isNextMonth">次月＞</button>
				</form:form>
				</div>
				<br>
			</div>

			<!-- 集計部（時間） -->
			<!-- 集計部（日数） -->
			<div class = "aggregateTime">
				<table class="tableRight" >
					<thead>
						<tr>
							<th nowrap><span>合計勤務時間</span></th>
							<th nowrap><span>深夜</span></th>
							<th nowrap><span>休憩時間</span></th>
							<th nowrap><span>合計勤務日数</span></th>
							<th nowrap><span>有給数</span></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><c:out value="${allTime.allActualWorkingTime}"></c:out></td>
							<td><c:out value="${allTime.allNightTime}"></c:out></td>
							<td><c:out value="${allTime.allRestTime}"></c:out></td>
							<td><c:out value="${allTime.allWorkingDay}"></c:out></td>
							<td><c:out value="${loginUser.paidHolidaysRemaining}"></c:out></td>
						</tr>
					</tbody>

				</table>
			</div>
		</div>

		<!-- 一括承認（承認） -->

		<!-- 勤務表表示部 -->
		<div class = "kintaiTable">
			<table class="tableKintai">
				<thead>
					<tr class="approvalForm">
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>
									<form:form modelAttribute="Approval1">
									<form:input type="hidden" path="createUser" value="${users.id}"/>
									<form:input type="hidden" path="matchMonthKey" value="${Approval.matchMonthKey}" />
									<input class = "syouninButton" type="submit" formaction="approvalForm1" value="一括1">
									</form:form></th>
						<th>
									<form:form modelAttribute="Approval2">
									<form:input type="hidden" path="createUser" value="${users.id}"/>
									<form:input type="hidden" path="matchMonthKey" value="${Approval.matchMonthKey}" />
									<input class = "syouninButton" type="submit" formaction="approvalForm2" value="一括2">
									</form:form></th>
					</tr>
					<tr class="thBackCollor">
						<th>日付</th>
						<th>勤務ID</th>
						<th class = "width90">始業時間</th>
						<th class = "width90">終業時間</th>
						<th class = "width90">休憩開始</th>
						<th class = "width90">休憩終了</th>
						<th>実働時間</th>
						<th>休憩時間</th>
						<th>深夜時間</th>
						<th>備考</th>
						<th>承認1</th>
						<th>承認2</th>
						<th>更新</th>
					</tr>
					</thead>
					</tbody>
					<% int i = 1;%>

					<c:forEach items="${attendances}" var="attendances">
					<tr>

					<c:if test="${attendances.id != 0}">
					<form:form modelAttribute="attendanceUpdate">

						<td><c:out value="${attendances.calendar}"></c:out></td>
						<td>
							<select name="dutyDivisionId">
								<c:forEach items="${dutyDivisions}" var="dutyDivisions">
									<c:if
										test="${dutyDivisions.id == attendances.dutyDivisionId}">
                                         <option value="0"></option>
                                       <option value="${dutyDivisions.id}" selected>${dutyDivisions.dutyDivision}</option>

									</c:if>
									<c:if test="${dutyDivisions.id != attendances.dutyDivisionId }">
										<option value="${dutyDivisions.id}">${dutyDivisions.dutyDivision }</option>
									</c:if>
								</c:forEach>
							</select>
						</td>
						<td class="timeBox">
							<form:input path="startHourTime" value="${attendances.startHourTime}" maxlength="2" onblur="startHUpdate(this)" />
							<c:out value="："></c:out>
							<form:input path="startMinuteTime" value="${attendances.startMinuteTime}" maxlength="2" onblur="startMUpdate(this)" />
						</td>
						<td class="timeBox">
							<form:input path="endHourTime" value="${attendances.endHourTime}" maxlength="2" onblur="endHUpdate(this)" />
							<c:out value="："></c:out>
							<form:input path="endMinuteTime" value="${attendances.endMinuteTime}" maxlength="2" onblur="endMUpdate(this)" />
						</td>
						<td class="timeBox">
							<form:input path="breakHourTimeStart" value="${attendances.breakHourTimeStart}" maxlength="2" onblur="breakHUpdate(this)" />
							<c:out value="："></c:out>
							<form:input path="breakMinuteTimeStart" value="${attendances.breakMinuteTimeStart}" maxlength="2" onblur="breakMUpdate(this)" />
						</td>
						<td class="timeBox">
							<form:input path="breakHourTimeEnd" value="${attendances.breakHourTimeEnd}" maxlength="2" onblur="endBreakHUpdate(this)" />
							<c:out value="："></c:out>
							<form:input path="breakMinuteTimeEnd" value="${attendances.breakMinuteTimeEnd}" maxlength="2" onblur="endBreakMUpdate(this)"/>
						</td>
						<td><c:out value="${attendances.actualWorkingHours}"></c:out></td>
						<td><c:out value="${attendances.restTime}"></c:out></td>
						<td><c:out value="${attendances.lateNightTime}"></c:out></td>
						<td><form:input path="remarks" value="${attendances.remarks}" /></td>
						<c:if test="${attendances.approval1 == true}">
							<td><form:checkbox class="checkBox" path="approval1" value="${attendances.approval1}" checked="checked"/></td>
						</c:if>
						<c:if test="${attendances.approval1 == false}">
							<td><form:checkbox class="checkBox" path="approval1" value="${attendances.approval1}" /></td>
						</c:if>
						<c:if test="${attendances.approval2 == true}">
							<td><form:checkbox class="checkBox" path="approval2" value="${attendances.approval2}" checked="checked"/></td>
						</c:if>
						<c:if test="${attendances.approval2 == false}">
							<td><form:checkbox class="checkBox" path="approval2" value="${attendances.approval2}" /></td>
						</c:if>
						<form:input type="hidden" path="id" value="${attendances.id}"/>
						<form:input type="hidden" path="matchDayKey" value="${attendances.matchDayKey}"/>
						<form:input type="hidden" path="matchMonthKey" value="${attendances.matchMonthKey}"/>
						<form:input type="hidden" path="matchYearKey" value="${attendances.matchYearKey}"/>
						<form:input type="hidden" path="updateUser" value="${users.id}"/>

						<td><input class = "kintaiButton" type="submit" formaction="attendanceUpdate" value="更新" onclick="func()" ></td>
					</form:form>
					</c:if>

					<c:if test="${attendances.id == 0}">
					<form:form modelAttribute="AttendanceForm">
					<c:choose>
										<c:when test="${(attendances.calendar).contains('(日)')}">
											<td style="color: red;" id = "calendar<%=i%>"><c:out
													value="${attendances.calendar}"></c:out></td>
										</c:when>
										<c:when test="${(attendances.calendar).contains('(土)')}">
											<td style="color: blue;" id = "calendar<%=i%>"><c:out
													value="${attendances.calendar}"></c:out></td>
										</c:when>
										<c:otherwise>
											<td style="color: black;" id = "calendar<%=i%>"><c:out
													value="${attendances.calendar}"></c:out></td>
										</c:otherwise>
									</c:choose>

						<td  id = "selectBox<%=i%>">


						<select name="dutyDivisionId">
						<option value="0"></option>
								<c:forEach items="${dutyDivisions}" var="dutyDivisions">
									<c:if
										test="${dutyDivisions.id == attendances.dutyDivisionId}">
										<option value="${dutyDivisions.id}" selected>${dutyDivisions.dutyDivision}</option>
									</c:if>
									<c:if test="${dutyDivisions.id != attendances.dutyDivisionId }">
										<option value="${dutyDivisions.id}">${dutyDivisions.dutyDivision}</option>
									</c:if>
								</c:forEach>
							</select>
						</td>

						<td class="timeBox" id= "start<%=i%>">
							<form:input path="startHourTime" value="${attendances.startHourTime}" maxlength="2" onblur="startH(this)"/>
							<c:out value="："></c:out>
							<form:input path="startMinuteTime" value="${attendances.startMinuteTime}" maxlength="2" onblur="startM(this)"/>
						</td>
						<td class="timeBox" id= "end<%=i%>">
							<form:input path="endHourTime" value="${attendances.endHourTime}" maxlength="2" onblur="endH(this)" />
							<c:out value="："></c:out>
							<form:input path="endMinuteTime" value="${attendances.endMinuteTime}" maxlength="2" onblur="endM(this)" />
						</td>
						<td class="timeBox" id= "breakstart<%=i%>">
							<form:input path="breakHourTimeStart" value="${attendances.breakHourTimeStart}" maxlength="2" onblur="breakH(this)" />
							<c:out value="："></c:out>
							<form:input path="breakMinuteTimeStart" value="${attendances.breakMinuteTimeStart}" maxlength="2" onblur="breakM(this)" />
						</td>
						<td class="timeBox" id= "breakend<%=i%>">
							<form:input path="breakHourTimeEnd"  value="${attendances.breakHourTimeEnd}" maxlength="2" onblur="endBreakH(this)" />
							<c:out value="："></c:out>
							<form:input path="breakMinuteTimeEnd"  value="${attendances.breakMinuteTimeEnd}" maxlength="2" onblur="endBreakM(this)" />
						</td>
						<td id = "wkHours<%=i%>"><c:out value="${attendances.actualWorkingHours}"></c:out></td>
						<td id = "restTime<%=i%>"><c:out value="${attendances.restTime}"></c:out></td>
						<td id = "nightTime<%=i%>"><c:out value="${attendances.lateNightTime}"></c:out></td>
						<td id = "remarks<%=i%>"><form:input path="remarks" value="${attendances.remarks}" /></td>
						<c:if test="${attendances.approval1 == true}">
							<td><form:checkbox path="approval1" value="${attendances.approval1}" checked="checked"/></td>
						</c:if>
						<c:if test="${attendances.approval1 == false}">
							<td><form:checkbox path="approval1" value="${attendances.approval1}" /></td>
						</c:if>
						<c:if test="${attendances.approval2 == true}">
							<td><form:checkbox path="approval2" value="${attendances.approval2}" checked="checked"/></td>
						</c:if>
						<c:if test="${attendances.approval2 == false}">
							<td><form:checkbox path="approval2" value="${attendances.approval2}" /></td>
						</c:if>

						<form:input type="hidden" path="id" value="${attendances.id}"/>
						<form:input type="hidden" path="matchDayKey" value="${attendances.matchDayKey}"/>
						<form:input type="hidden" path="matchMonthKey" value="${attendances.matchMonthKey}"/>
						<form:input type="hidden" path="matchYearKey" value="${attendances.matchYearKey}"/>
						<form:input type="hidden" path="createUser" value="${users.id}"/>
						<%i++; %>
						<td><div ><input class = "kintaiButton" type="submit" formaction="attendanceInsert" value="登録" onclick="func()"></div></td>
					</form:form>
					</c:if>
					</tr>
					</c:forEach>
				</tbody>
			</table><br><br><br><br><br>
		</div>
	</div>
	</fieldset>
	</div>
</body>
</html>
