<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>共通ヘッダ</title>
<link href="<c:url value="/resources/css/header.css"/>" rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="header-left">
					<h1><id="header-left-p">Project-O1</h1>
			<p id="header-left-p">
				<a href="${pageContext.request.contextPath}/attendanceRegistration" class="btn-square">ホーム</a>
				<c:if test="${loginUser.roleId == '1'}">
				<a href="${pageContext.request.contextPath}/userAttendance"class="btn-square">勤怠管理</a>
				<a href="${pageContext.request.contextPath}/userDisplay"class="btn-square">ユーザ管理</a>
				<a href="${pageContext.request.contextPath}/#"class="btn-square">システム管理</a>
				</c:if>
				</p>
		</div>
		<div class="header-clearfix">

	<div class="col-xs-12 col-sm-4 col-sm-offset-4">
      <div class="form-group">
      <div class="select-wrap select-circle select-circle-arrow">
          <div class="select-wrap select-circle select-circle-arrow">
				<!-- 管理者権限が1の人 -->
           <select name="" onChange="location.href=value;"id="">
            <c:if test="${loginUser.roleId == '1'}">
            <option value ="">▽</option>
       		<option onclick="selectCheck();" value="${pageContext.request.contextPath}/updatePassword">パスワード変更</option>
       		<option onclick="selectCheck();" value="${pageContext.request.contextPath}/logout">ログアウト</option>
		</c:if>
		<c:if test="${loginUser.roleId == '2'}">
		<option value ="">▽</option>
		<option onclick="selectCheck();" value="${pageContext.request.contextPath}/updatePassword">パスワード変更</option>
		<option onclick="selectCheck();" value="${pageContext.request.contextPath}/logout">ログアウト</option>
		</c:if>
          </select>
			<div class="header-right">
			<h2>ログインユーザ：<c:out value="${loginUser.name}"></c:out></h2>
			 </div>
           </div>
     </div>
 	 </div>
	</div>
 	</div>
	</div>
</body>
</html>