<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-cache");
response.addHeader("Cache-Control","no-store");
response.setDateHeader("Expires", 0);
%>
<html>
<head>
<meta charset="utf-8">
<title>パスワード変更</title>
<link href="<c:url value="/resources/css/updatePassword.css"/>" rel="stylesheet">
 <%@ include file="header.jsp"%>
</head>
<script type="text/javascript">
	function password1Error(){
		if(document.forms.userPasswordForm.password1.value==""){
			error.innerHTML = "現在のパスワードを入力してください";
		}
		if(document.forms.userPasswordForm.password1.value!=""){
			error.innerHTML = "";
		}
	}
	function password2Error(){
		if(document.forms.userPasswordForm.password2.value==""){
			error.innerHTML = "変更用パスワードを入力してください";
		}else if(document.forms.userPasswordForm.password2.value!=""){
			error.innerHTML = "";
		}
	}
</script>
<body>
<br><br><br><br><br><br>
<h3>パスワード変更画面</h3>
    <fieldset>
    		<h4>
			<c:out value="パスワード変更"></c:out>
			</h4>
			<form:form  modelAttribute="userPasswordForm">
			<div class="error" id="error">
			<b>${title}</b><br> <b>${unUpdatePasswordMessage}</b></div>
			<table>
				<tbody>
					<tr>
						<th>現在のパスワード</th>
						<td>
						<input name="password1" id="password1" onblur="password1Error();" type="password" value="${userPasswordForm.password1}" autofocus
							title="現在のパスワードは半角数字6文字以上20文字以内で入力してください"/>
						<f:errors path="password1" />
						</td>
					</tr>
					<tr>
						<th>変更後パスワード</th>
						<td>
						<input name="password2" id="password2" onblur="password2Error();" type="password" value="${userPasswordForm.password2}" autofocus
							 title="変更後パスワードは半角数字6文字以上20文字以内で入力してください"/>
						<f:errors path="password2" />
						</td>
					</tr>
				</tbody>
			</table>
			<div class="userPassword_button">
				<p>
				<form:input type="hidden" path="id" value="${loginUser.id}"/>
				<input class="p1" type="submit" value="変更">
				</p>
			</div>
		</form:form>
	</fieldset>
</body>
</html>

