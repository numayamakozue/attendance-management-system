package kintai.service;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kintai.dao.dto.DepartmentDto;
import kintai.dao.dto.PositionDto;
import kintai.dao.dto.RoleDto;
import kintai.dao.entity.DepartmentEntity;
import kintai.dao.entity.PositionEntity;
import kintai.dao.entity.RoleEntity;
import kintai.dao.mapper.DepartmentMapper;
import kintai.dao.mapper.PositionMapper;
import kintai.dao.mapper.RoleMapper;
import kintai.dao.mapper.UserRegisterMapper;
import kintai.form.UserRegisterForm;
import kintai.util.CipherPass;

@Service
public class UserRegisterService {
	@Autowired
	private UserRegisterMapper userRegisterMapper;
	@Autowired
	private DepartmentMapper departmentMapper;
	@Autowired
	private PositionMapper positionMapper;
	@Autowired
	private RoleMapper roleMapper;


	public void userRegister(UserRegisterForm form) throws ParseException{
        //formの日付のフォーマットを変換する
        String strDate = form.getDireDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse(strDate);
		String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
		//変換した日付フォーマットをformに詰めなおす
        form.setDireDate(str);
		//パスワードの暗号化
		String password = CipherPass.originalEncrypt(form.getPassword());
		//暗号化したパスワードをformに詰める
		form.setPassword(password);
        //入力情報と暗号化したパスワードをmapperに詰める
	    userRegisterMapper.userRegister(form);
	}
	public List<DepartmentDto> getDepartment(){
        //部署の情報をresultに返す
		List<DepartmentEntity> DepartmentList = departmentMapper.getDepartment();
		List<DepartmentDto> result = convertToDepartment(DepartmentList);
		return result;
	}
	public List<DepartmentDto> getDepartment(UserRegisterForm form){
        //部署の情報をresultに返す
		List<DepartmentEntity> DepartmentList = departmentMapper.getDepartment();
		List<DepartmentDto> result = convertToDepartment(DepartmentList);
		return result;
	}

	private List<DepartmentDto> convertToDepartment(List<DepartmentEntity> departmentList) {

		// 	entityの情報をdtoにコピー
		List<DepartmentDto> result = new ArrayList<DepartmentDto>();
		for( DepartmentEntity entity : departmentList ) {
			DepartmentDto dto = new DepartmentDto();
			BeanUtils.copyProperties(entity, dto);
			result.add(dto);
		}
		return result;
		
	}
	public List<PositionDto> getPosition(){
        //役職の情報をresultに返す
		List<PositionEntity> PositionList = positionMapper.getPosition();
		List<PositionDto> result = convertToPosition(PositionList);
		return result;
	}
	public List<PositionDto> getPosition(UserRegisterForm form){
        //役職の情報をresultに返す
		List<PositionEntity> PositionList = positionMapper.getPosition();
		List<PositionDto> result = convertToPosition(PositionList);
		return result;
	}
	
	private List<PositionDto> convertToPosition(List<PositionEntity> positionList) {

		// 	entityの情報をdtoにコピー
		List<PositionDto> result = new ArrayList<PositionDto>();
		for( PositionEntity entity : positionList ) {
			PositionDto dto = new PositionDto();
			BeanUtils.copyProperties(entity, dto);
			result.add(dto);
		}
		return result;
	}
	
	public List<RoleDto> getRole() {
		//権限の情報をresultに返す
		List<RoleEntity> RoleList = roleMapper.getRole();
		List<RoleDto> result = convertToRole(RoleList);
		return result;
	}
	public List<RoleDto> getRole(UserRegisterForm form){
        //権限の情報をresultに返す
		List<RoleEntity> RoleList = roleMapper.getRole();
		List<RoleDto> result = convertToRole(RoleList);
		return result;
	}
	
	private List<RoleDto> convertToRole(List<RoleEntity> roleList) {

		// 	entityの情報をdtoにコピー
		List<RoleDto> result = new ArrayList<RoleDto>();
		for( RoleEntity entity : roleList ) {
			RoleDto dto = new RoleDto();
			BeanUtils.copyProperties(entity, dto);
			result.add(dto);
		}
		return result;
	}


}
