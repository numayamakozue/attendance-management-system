package kintai.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kintai.dao.dto.UserDetailsDto;
import kintai.dao.entity.UserDetailsEntity;
import kintai.dao.mapper.UserDetailsMapper;
import kintai.form.UserInfoUpdateForm;
import kintai.util.CipherPass;

@Service
public class UserDetailsService {
    @Autowired
    private UserDetailsMapper userDetailsMapper;

    public UserDetailsDto getUserDetails(Integer id) {
    	UserDetailsDto dto = new UserDetailsDto();
        UserDetailsEntity entity = userDetailsMapper.getUserDetails(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public int updateProcess(UserInfoUpdateForm form) throws ParseException{
    	//formの日付のフォーマットを変換する
        String strDate = form.getDire_date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse(strDate);
		String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
		//変換した日付フォーマットをformに詰めなおす
        form.setDire_date(str);
    	String encPassword = CipherPass.originalEncrypt(form.getPassword());
    	form.setPassword(encPassword);
        int count = userDetailsMapper.updateProcess(form);
        return count;
    }

    public int deleteProcess(int id) {
        int count = userDetailsMapper.deleteProcess(id);
        return count;
    }
}