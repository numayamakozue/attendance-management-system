package kintai.service;

import java.text.ParseException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kintai.dao.dto.UserDisplayDto;
import kintai.dao.dto.UserPasswordDto;
import kintai.dao.entity.UserDisplayEntity;
import kintai.dao.entity.UserPasswordEntity;
import kintai.dao.mapper.UserPasswordMapper;
import kintai.form.UserPasswordForm;
import kintai.util.CipherPass;

@Service
public class UserPasswordService {
	@Autowired
    private UserPasswordMapper userPasswordMapper;
	        
	    //データをdtoに詰めなおす
		public UserDisplayDto getUserPassword(int id, String password1){
			UserDisplayDto dto = new UserDisplayDto();
	        String encPassword1 = CipherPass.originalEncrypt(password1);
	        UserDisplayEntity entity = userPasswordMapper.getUserPassword(id, encPassword1);
	        BeanUtils.copyProperties(entity, dto);
			return dto;

		}
	 
	
		public void update(UserPasswordForm form) throws ParseException{
			//パスワードの暗号化
			int isChangePw = (form.getIsChangePw());
			String encPassword2 = CipherPass.originalEncrypt(form.getPassword2());
			//UserPasswordDto loginUser;
			//暗号化したパスワードをformに詰める
			form.setIsChangePw(isChangePw);
			form.setPassword2(encPassword2);
	        //入力情報と暗号化したパスワードをmapperに詰める
		    userPasswordMapper.userPassword(form);
		}
		

		public void update(UserPasswordDto dto) {
			
			UserPasswordEntity update = new UserPasswordEntity();
			String encPassword1 = CipherPass.originalEncrypt(dto.getPassword1());
			String encPassword2 = CipherPass.originalEncrypt(dto.getPassword2());
			update.setPassword1(String.valueOf(encPassword1));
			update.setPassword2(String.valueOf(encPassword2));
			update.setId(dto.getId());
			update.setIsChangePw(dto.getIsChangePw());
			// DBにてupdate処理
			userPasswordMapper.update(update);
		}

		//パスワード変更処理後の情報を取得し、dto2に詰める
		public UserDisplayDto getChangePassword(int id, String password2){
			UserDisplayDto dto2 = new UserDisplayDto();
	        String encPassword2 = CipherPass.originalEncrypt(password2);
	        UserDisplayEntity entity2 = userPasswordMapper.getChangePassword(id, encPassword2);
	        BeanUtils.copyProperties(entity2, dto2);
			return dto2;

		}
		
		

		
	    
}

	

