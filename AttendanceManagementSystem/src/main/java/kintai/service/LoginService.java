package kintai.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kintai.dao.dto.UserDisplayDto;
import kintai.dao.entity.UserDisplayEntity;
import kintai.dao.mapper.UserMapper;
import kintai.util.CipherPass;

@Service
public class LoginService {

	@Autowired
	private UserMapper userMapper;
     //データをdtoに詰めなおす
	public UserDisplayDto getLoginUser(String employeeNumber, String password){
        UserDisplayDto dto = new UserDisplayDto();
        String encPassword = CipherPass.originalEncrypt(password);
        UserDisplayEntity entity = userMapper.getLoginUser(employeeNumber, encPassword);
        BeanUtils.copyProperties(entity, dto);
		return dto;

	}
}