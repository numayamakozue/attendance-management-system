package kintai.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kintai.dao.dto.AttendanceDto;
import kintai.dao.dto.DutyDivisionDto;
import kintai.dao.entity.AttendanceEntity;
import kintai.dao.entity.DutyDivisionEntity;
import kintai.dao.mapper.AttendanceMapper;
import kintai.dao.mapper.DutyDivisionMapper;
import kintai.form.AttendanceForm;

@Service
public class RegistrerAttendanceService {

	@Autowired
	private AttendanceMapper attendanceMapper;
	@Autowired
	private DutyDivisionMapper dutyDivisionMapper;

	public List<AttendanceDto> getAttendance(int id) {

		// DBから勤怠情報を取得
		List<AttendanceEntity> AttendanceList = attendanceMapper.getAttendance(id);
		List<AttendanceDto> result = convertToDto(AttendanceList);
		return result;
	}


	private List<AttendanceDto> convertToDto(List<AttendanceEntity> attendanceList) {

		// 	entityの情報を編集しdtoに保管
		List<AttendanceDto> result = new ArrayList<AttendanceDto>();
		for( AttendanceEntity entity : attendanceList ) {
			AttendanceDto dto = new AttendanceDto();
			dto.setId(entity.getId());
			dto.setDutyDivisionId(entity.getDutyDivisionId());
			if(entity.getStartTime() != null) {
				dto.setStartHourTime(entity.getStartTime().substring(8,10));
				dto.setStartMinuteTime(entity.getStartTime().substring(10,12));
			}
			if(entity.getEndTime() != null) {
				dto.setEndHourTime(entity.getEndTime().substring(8,10));
				dto.setEndMinuteTime(entity.getEndTime().substring(10,12));
			}
			if(entity.getBreakTimeStart() != null) {
				dto.setBreakHourTimeStart(entity.getBreakTimeStart().substring(8,10));
				dto.setBreakMinuteTimeStart(entity.getBreakTimeStart().substring(10,12));
			}
			if(entity.getBreakTimeEnd() != null) {
				dto.setBreakHourTimeEnd(entity.getBreakTimeEnd().substring(8,10));
				dto.setBreakMinuteTimeEnd(entity.getBreakTimeEnd().substring(10,12));
			}
			dto.setActualWorkingHours(entity.getActualWorkingHours());
			dto.setRestTime(entity.getRestTime());
			dto.setRemarks(entity.getRemarks());
			dto.setApproval1(entity.getApproval1());
			dto.setApproval2(entity.getApproval2());
			dto.setCreateUser(entity.getCreateUser());
			dto.setUpdateUser(entity.getUpdateUser());
			dto.setCreateDate(entity.getCreateDate());
			dto.setUpdateDate(entity.getUpdateDate());
			dto.setDutyDivision(entity.getDutyDivision());
			dto.setId(entity.getId());

			// ①月詰め
			// ②日付詰め
			// ③年詰め

			// ①
			if(entity.getStartTime() != null) {
				dto.setMatchMonthKey(Integer.parseInt(entity.getStartTime().substring(4,6)));
			}
			if(entity.getEndTime() != null) {
				dto.setMatchMonthKey(Integer.parseInt(entity.getEndTime().substring(4,6)));
			}
			if(entity.getBreakTimeStart() != null) {
				dto.setMatchMonthKey(Integer.parseInt(entity.getBreakTimeStart().substring(4,6)));
			}
			if(entity.getBreakTimeEnd() != null) {
				dto.setMatchMonthKey(Integer.parseInt(entity.getBreakTimeEnd().substring(4,6)));
			}

			// ②
			if(entity.getStartTime() != null) {
				dto.setMatchDayKey(Integer.parseInt(entity.getStartTime().substring(6,8)));
			}
			if(entity.getEndTime() != null) {
				dto.setMatchDayKey(Integer.parseInt(entity.getEndTime().substring(6,8)));
			}
			if(entity.getBreakTimeStart() != null) {
				dto.setMatchDayKey(Integer.parseInt(entity.getBreakTimeStart().substring(6,8)));
			}
			if(entity.getBreakTimeEnd() != null) {
				dto.setMatchDayKey(Integer.parseInt(entity.getBreakTimeEnd().substring(6,8)));
			}

			// ③
			if(entity.getStartTime() != null) {
				int year = Integer.parseInt(entity.getStartTime().substring(0,4));
				dto.setMatchYearKey(year);
				}
			if(entity.getEndTime() != null) {
				int year = Integer.parseInt(entity.getEndTime().substring(0,4));
				dto.setMatchYearKey(year);
			}
			if(entity.getBreakTimeStart() != null) {
				int year = Integer.parseInt(entity.getBreakTimeStart().substring(0,4));
				dto.setMatchYearKey(year);
			}
			if(entity.getBreakTimeEnd() != null) {
				int year = Integer.parseInt(entity.getBreakTimeEnd().substring(0,4));
				dto.setMatchYearKey(year);
			}
			result.add(dto);
		}
		return result;
	}


	public void insertDuty(AttendanceForm insertForm) {

		int startTime = 0;
		int endTime = 0;
		int restStartTime = 0;
		int restEndTime = 0;

		// 業務時間
		if(insertForm.getStartHourTime() != "" && insertForm.getStartMinuteTime() != "" && insertForm.getEndHourTime() != "" && insertForm.getEndMinuteTime() != "" ) {
			startTime = Integer.parseInt(insertForm.getStartHourTime() + insertForm.getStartMinuteTime());
			endTime = Integer.parseInt(insertForm.getEndHourTime() + insertForm.getEndMinuteTime());
		}

		// 休憩時間取得
		if(insertForm.getBreakHourTimeStart() != "" && insertForm.getBreakMinuteTimeStart() != "" && insertForm.getBreakHourTimeEnd() !="" && insertForm.getBreakMinuteTimeEnd() != "" ) {
		restStartTime = Integer.parseInt(insertForm.getBreakHourTimeStart() + insertForm.getBreakMinuteTimeStart());
		restEndTime = Integer.parseInt(insertForm.getBreakHourTimeEnd() + insertForm.getBreakMinuteTimeEnd());
		}

		// 実働時間の計算
		String allWorkTime = "0000";
		if(startTime != 0 || endTime != 0) {
			allWorkTime = hourCalculation(startTime, endTime, restStartTime, restEndTime);
		}

		//深夜労働時間の計算
		String lateWork = "0000";
		if(startTime != 0 || endTime != 0) {
			lateWork = lateHourCalculation(startTime, endTime);
			if(startTime == 9999 && endTime == 9999) {
				lateWork = "0000";

			}
		}

		//値の入れ替えform→entity
		AttendanceEntity insertKintai = new AttendanceEntity();

		// 時間部分
		if (allWorkTime != "0000" && lateWork != "0000") {
			int resultHour = Integer.parseInt(allWorkTime.substring(0,2));

			int lateResultHour = Integer.parseInt(lateWork.substring(0,2));

			// 分部分の抽出
			int resultMin = Integer.parseInt(allWorkTime.substring(2,4));
			int lateresultMin = Integer.parseInt(lateWork.substring(2,4));

			// 分を小数値に変換（実働時間、休憩時間、深夜時間）
			double decimalValue = conversionMin(resultMin);
			double lateDecimalValue = conversionMin(lateresultMin);

			// 合体（実働時間、休憩時間、深夜時間）
			double workingHours = resultHour+ decimalValue;
			double lateWorkHours = lateResultHour + lateDecimalValue;

			// 配置（実働時間、休憩時間、深夜時間）
			insertKintai.setActualWorkingHours(workingHours);
			insertKintai.setLateNightTime(lateWorkHours);
		}


		// 休憩時間計算
		String allRestTime = null;
		if( restStartTime != 0 && restEndTime != 0) {

			allRestTime = restTimeCalculation(restStartTime, restEndTime);

			// 時間部分
			int restHour = Integer.parseInt(allRestTime.substring(0,2));

			// 分部分の抽出
			int restMin = Integer.parseInt(allRestTime.substring(2,4));

			// 分を小数値に変換（実働時間、休憩時間、深夜時間）
			double restDecimalValue = conversionMin(restMin);

			// 合体（実働時間、休憩時間、深夜時間）
			double restHours = restHour+ restDecimalValue;

			// 配置（実働時間、休憩時間、深夜時間）
			insertKintai.setRestTime(restHours);
		}

		// 年月日と合体
		// todo 年を動的に取得しなくてはならない
		// ※ 現在は2020年4月を静的にくっつけてる。
		String selectDay = String.format("%02d", insertForm.getMatchDayKey());
		String selectMonth = String.format("%02d", insertForm.getMatchMonthKey());
		String selectYear = String.format("%d",insertForm.getMatchYearKey());
		// 始業時間に入力がない場合は登録しない
		if (insertForm.getStartHourTime() != "" && insertForm.getStartMinuteTime() != "" ) {
			String startTimeChar = insertForm.getStartHourTime() + insertForm.getStartMinuteTime();
			insertKintai.setStartTime(selectYear + selectMonth + selectDay + startTimeChar);
		}
		// 終業時間に入力がない場合は登録しない
		if (insertForm.getEndHourTime() != "" && insertForm.getEndMinuteTime() != "") {
			String endTimeChar = insertForm.getEndHourTime() + insertForm.getEndMinuteTime();
			insertKintai.setEndTime(selectYear + selectMonth + selectDay + endTimeChar);
		}
		// 休憩開始に入力がない場合は登録しない
		if (insertForm.getBreakHourTimeStart() != "" && insertForm.getBreakMinuteTimeStart() != "") {
			String restStartTimeChar = insertForm.getBreakHourTimeStart() + insertForm.getBreakMinuteTimeStart();
			insertKintai.setBreakTimeStart(selectYear + selectMonth + selectDay + restStartTimeChar);
		}
		// 休憩終了に入力がない場合は登録しない
		if (insertForm.getBreakHourTimeEnd() !="" && insertForm.getBreakMinuteTimeEnd() != "") {
			String restEndTimeChar = insertForm.getBreakHourTimeEnd() + insertForm.getBreakMinuteTimeEnd();
			insertKintai.setBreakTimeEnd(selectYear + selectMonth + selectDay + restEndTimeChar);
		}

		// 他の入力値を移し替える
		insertKintai.setId(insertForm.getId());
		insertKintai.setDutyDivisionId(insertForm.getDutyDivisionId());
		insertKintai.setRemarks(insertForm.getRemarks());
		insertKintai.setApproval1(insertForm.getApproval1());
		insertKintai.setApproval2(insertForm.getApproval2());
		insertKintai.setCreateUser(insertForm.getCreateUser());
		insertKintai.setUpdateUser(insertForm.getUpdateUser());

		// DBにて勤怠登録情報をinsert処理
		attendanceMapper.insertDuty(insertKintai);
	}


	private String hourCalculation(int startTime, int endTime, int restStartTime, int restEndTime) {
		String result = null;

		// 分単位に変換
		int startTimeMin = startTime / 100 * 60 + startTime % 100;
		int endTimeMin = endTime / 100 * 60 + endTime % 100;
		int startRestTimeMin = restStartTime / 100 * 60 + restStartTime % 100;
		int endRestTimeMin = restEndTime / 100 * 60 + restEndTime % 100;

		// (終業時間 - 始業時間) - (休憩開始時間 - 休憩終了時間)の計算
		int resultmin = (endTimeMin - startTimeMin) - (endRestTimeMin - startRestTimeMin);

		// 元の形式 ( hhmm ) に戻す → 実働時間
		int resultTime = resultmin / 60 * 100 + resultmin % 60;
		result = String.format("%04d", resultTime);
		return result;
	}


	private String lateHourCalculation(int startTime, int endTime) {
		String result = "0000";
		if(startTime < 2200) {
			startTime = 2200;
		}

		if(endTime > 2900) {
			endTime = 2900;
		}else if(endTime < 2200) {
			endTime = 2200;
		}

		// 分単位に変換
		int startTimeMin = startTime / 100 * 60 + startTime % 100;
		int endTimeMin = endTime / 100 * 60 + endTime % 100;

		// (深夜終業時間 - 深夜始業時間)の計算
		if(startTimeMin >= 3520 || endTimeMin <= 4640 ) {
		int resultmin = endTimeMin - startTimeMin;

		// 元の形式 ( hhmm ) に戻す → 実働時間
		int resultTime = resultmin / 60 * 100 + resultmin % 60;
		result = String.format("%04d", resultTime);
		}else {
			result = "0000";
		}

		return result;
	}


	private String restTimeCalculation(int restStartTime, int restEndTime) {
		String result = null;

		// 分単位に変換
		int startRestTimeMin = restStartTime / 100 * 60 + restStartTime % 100;
		int endRestTimeMin = restEndTime / 100 * 60 + restEndTime % 100;

		// 休憩総時間
		int restTimeMin = endRestTimeMin - startRestTimeMin;
		int resultTime = restTimeMin / 60 * 100 + restTimeMin % 60;
		result = String.format("%04d", resultTime);
		return result;
	}


	private double conversionMin(int resultMin) {

		double convertMin = 0;

		int fourteen = 14;
		int twentyNine = 29;
		int fortyFour = 44;
		int fiftyNine = 59;

		// 0分～14分の時は0
		if(resultMin <= fourteen) {
			convertMin = 0;

		// 15分～29分の時は0.25
		}else if (resultMin <= twentyNine) {
			convertMin = 0.25;

		// 30分～44分の時は0.5
		}else if (resultMin <= fortyFour) {
			convertMin = 0.5;

		// 45分～59分の時は0.75
		}else if (resultMin <= fiftyNine) {
			convertMin = 0.75;

		}
		return convertMin;
	}

	public List<DutyDivisionDto> getDutyDivision(){

		List<DutyDivisionEntity> DutyDivisionList = dutyDivisionMapper.getDutyDivision();
		List<DutyDivisionDto> result = convertToDutyDto(DutyDivisionList);
		return result;
	}

	private List<DutyDivisionDto> convertToDutyDto(List<DutyDivisionEntity> dutyDivisionList) {

		// 	entityの情報をdtoにコピー
		List<DutyDivisionDto> result = new ArrayList<DutyDivisionDto>();
		for( DutyDivisionEntity entity : dutyDivisionList ) {
			DutyDivisionDto dto = new DutyDivisionDto();
			BeanUtils.copyProperties(entity, dto);
			result.add(dto);
		}
		return result;
	}

	public void updateDuty(AttendanceDto dto) {

		int startTime = 0;
		int endTime = 0;
		int restStartTime = 0;
		int restEndTime = 0;

		// 業務時間
		if(dto.getStartHourTime() != "" && dto.getStartMinuteTime() != "" && dto.getEndHourTime() != "" && dto.getEndMinuteTime() != "" ) {
			startTime = Integer.parseInt(dto.getStartHourTime() + dto.getStartMinuteTime());
			endTime = Integer.parseInt(dto.getEndHourTime() + dto.getEndMinuteTime());
		}

		// 休憩時間取得
		if(dto.getBreakHourTimeStart() != "" && dto.getBreakMinuteTimeStart() != "" && dto.getBreakHourTimeEnd() !="" && dto.getBreakMinuteTimeEnd() != "" ) {
		restStartTime = Integer.parseInt(dto.getBreakHourTimeStart() + dto.getBreakMinuteTimeStart());
		restEndTime = Integer.parseInt(dto.getBreakHourTimeEnd() + dto.getBreakMinuteTimeEnd());
		}

		// 実働時間の計算
		String allWorkTime = "0000";
		if(startTime != 0 || endTime != 0) {
			allWorkTime = hourCalculation(startTime, endTime, restStartTime, restEndTime);
		}

		//深夜労働時間の計算
		String lateWork = "0000";
		if(startTime != 0 || endTime != 0) {
			lateWork = lateHourCalculation(startTime, endTime);
		}

		//値の入れ替えform→entity
		AttendanceEntity updateKintai = new AttendanceEntity();

		// 時間部分
		if (allWorkTime != "0000" && lateWork != "0000") {
			int resultHour = Integer.parseInt(allWorkTime.substring(0,2));

			int lateResultHour = Integer.parseInt(lateWork.substring(0,2));

			// 分部分の抽出
			int resultMin = Integer.parseInt(allWorkTime.substring(2,4));
			int lateresultMin = Integer.parseInt(lateWork.substring(2,4));

			// 分を小数値に変換（実働時間、休憩時間、深夜時間）
			double decimalValue = conversionMin(resultMin);
			double lateDecimalValue = conversionMin(lateresultMin);

			// 合体（実働時間、休憩時間、深夜時間）
			double workingHours = resultHour+ decimalValue;
			double lateWorkHours = lateResultHour + lateDecimalValue;

			// 配置（実働時間、休憩時間、深夜時間）
			updateKintai.setActualWorkingHours(workingHours);
			updateKintai.setLateNightTime(lateWorkHours);
		}


		// 休憩時間計算
		String allRestTime = null;
		if( restStartTime != 0 && restEndTime != 0) {

			allRestTime = restTimeCalculation(restStartTime, restEndTime);

			// 時間部分
			int restHour = Integer.parseInt(allRestTime.substring(0,2));

			// 分部分の抽出
			int restMin = Integer.parseInt(allRestTime.substring(2,4));

			// 分を小数値に変換（実働時間、休憩時間、深夜時間）
			double restDecimalValue = conversionMin(restMin);

			// 合体（実働時間、休憩時間、深夜時間）
			double restHours = restHour+ restDecimalValue;

			// 配置（実働時間、休憩時間、深夜時間）
			updateKintai.setRestTime(restHours);
		}

		// 年月日と合体
		// todo 年を動的に取得しなくてはならない
		// ※ 現在は2020年4月を静的にくっつけてる。
		String selectDay = String.format("%02d", dto.getMatchDayKey());
		String selectMonth = String.format("%02d", dto.getMatchMonthKey());
		String selectYear = String.format("%d",dto.getMatchYearKey());
		// 始業時間に入力がない場合は登録しない
		if (dto.getStartHourTime() != "" && dto.getStartMinuteTime() != "" ) {
			String startTimeChar = dto.getStartHourTime() + dto.getStartMinuteTime();
			updateKintai.setStartTime(selectYear + selectMonth + selectDay + startTimeChar);
		}

		// 終業時間に入力がない場合は登録しない
		if (dto.getEndHourTime() != "" && dto.getEndMinuteTime() != "") {
			String endTimeChar = dto.getEndHourTime() + dto.getEndMinuteTime();
			updateKintai.setEndTime(selectYear + selectMonth + selectDay + endTimeChar);
		}

		// 休憩開始に入力がない場合は登録しない
		if (dto.getBreakHourTimeStart() != "" && dto.getBreakMinuteTimeStart() != "") {
			String restStartTimeChar = dto.getBreakHourTimeStart() + dto.getBreakMinuteTimeStart();
			updateKintai.setBreakTimeStart(selectYear + selectMonth + selectDay + restStartTimeChar);
		}

		// 休憩終了に入力がない場合は登録しない
		if (dto.getBreakHourTimeEnd() !="" && dto.getBreakMinuteTimeEnd() != "") {
			String restEndTimeChar = dto.getBreakHourTimeEnd() + dto.getBreakMinuteTimeEnd();
			updateKintai.setBreakTimeEnd(selectYear + selectMonth + selectDay + restEndTimeChar);
		}

		// 他の入力値を移し替える
		updateKintai.setId(dto.getId());
		updateKintai.setDutyDivisionId(dto.getDutyDivisionId());
		updateKintai.setRemarks(dto.getRemarks());
		updateKintai.setApproval1(dto.getApproval1());
		updateKintai.setApproval2(dto.getApproval2());
		updateKintai.setCreateUser(dto.getCreateUser());
		updateKintai.setUpdateUser(dto.getUpdateUser());

		// DBにて勤怠登録情報をinsert処理
		attendanceMapper.updateDuty(updateKintai);
	}




	//public int approvalForm1(AttendanceDto dto) {
		//int count = attendanceMapper.approvalForm1(dto);
		//return count;


		//値の入れ替えform→entity
		//AttendanceEntity batchApproval = new AttendanceEntity();




	//public void batchApproval(int createUser, String  startTime) {

		//AttendanceEntity approvalForm1 = new AttendanceEntity();
		//AttendanceEntity approvalForm2 = new AttendanceEntity();
		//attendanceMapper.batchApproval(createUser, startTime);
	//}


	public void approvalForm1(AttendanceDto dto) {
		AttendanceEntity batchApproval = new AttendanceEntity();
		batchApproval.setStartTime(String.valueOf(dto.getMatchMonthKey()));
		batchApproval.setCreateUser(dto.getCreateUser());
		// DBにてupdate処理
		attendanceMapper.approvalForm1(batchApproval);
	}

	public void approvalForm2(AttendanceDto dto) {
		AttendanceEntity batchApproval = new AttendanceEntity();
		batchApproval.setStartTime(String.valueOf(dto.getMatchMonthKey()));
		batchApproval.setCreateUser(dto.getCreateUser());
		// DBにてupdate処理
		attendanceMapper.approvalForm2(batchApproval);
	}


	public List<String> getCount1(String monthName,String sum1,String sum2,String sum3,String sum4,String sum5,String sum6,String sum7,String sum8,String employeeNumber,String name,String actual,String late,String rest,int page) {

		List<String> CountList1 = attendanceMapper.getCount1(monthName,sum1,sum2,sum3,sum4,sum5,sum6,sum7,sum8,employeeNumber,name,actual,late,rest,page);
		return CountList1;
	}




}
