package kintai.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kintai.dao.dto.UserUploadsDto;
import kintai.dao.entity.DepartmentEntity;
import kintai.dao.entity.PositionEntity;
import kintai.dao.entity.UserDisplayEntity;
import kintai.dao.mapper.DepartmentMapper;
import kintai.dao.mapper.PositionMapper;
import kintai.dao.mapper.UserUploadsMapper;

@Service
public class UserUploadsService {

	@Autowired
	private UserUploadsMapper userUploadsMapper;
	@Autowired
	private DepartmentMapper departmentMapper;
	@Autowired
	private PositionMapper positionMapper;


	public void userUploads(UserUploadsDto uploadForm){
		userUploadsMapper.userUploads(uploadForm);
	}

	public List<UserUploadsDto> getUserEmployeeNumber() {
		List<UserDisplayEntity> userList = userUploadsMapper.getUserEmployeeNumber();
		List<UserUploadsDto> resultList = convertToDto(userList);
		return resultList;
	}

	private List<UserUploadsDto> convertToDto(List<UserDisplayEntity> userList){
		List<UserUploadsDto> resultList = new ArrayList<UserUploadsDto>();
		for(UserDisplayEntity entity : userList) {
			UserUploadsDto dto = new UserUploadsDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public void userUploadDepartmentName(UserUploadsDto uploadForm){
		departmentMapper.getDepartmentName(uploadForm);
	}

	public void userUploadPositionName(UserUploadsDto uploadForm){
		positionMapper.getPositionName(uploadForm);
	}

	public String getDepartmentName(String departmentId){
		DepartmentEntity department = departmentMapper.getDepartmentName(departmentId);
		String departmentName = department.getDepartment();
		return departmentName;
	}

	public String getPositionName(String positionId){
		PositionEntity position = positionMapper.getPositionName(positionId);
		String positionName = position.getPosition();
		return positionName;
	}

}