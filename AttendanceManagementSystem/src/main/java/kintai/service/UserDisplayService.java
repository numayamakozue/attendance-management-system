package kintai.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kintai.dao.dto.UserDisplayDto;
import kintai.dao.entity.UserDisplayEntity;
import kintai.dao.mapper.UserMapper;
import kintai.form.UserForm;

@Service
public class UserDisplayService {
	@Autowired
	private UserMapper userMapper;

	public List<UserDisplayDto> getUsers(String employeeNumber, String name, String isDelete,String isChangePw, String id, int page) {
		List<UserDisplayEntity> userList = userMapper.getUsers(employeeNumber, name, isDelete, isChangePw, id, page);
		List<UserDisplayDto> resultList = convertToDto(userList);
		return resultList;
	}

	private List<UserDisplayDto> convertToDto(List<UserDisplayEntity> userList){
		List<UserDisplayDto> resultList = new ArrayList<UserDisplayDto>();
		for(UserDisplayEntity entity : userList) {
			UserDisplayDto dto = new UserDisplayDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public int getUpdate(UserForm form) {
		int count = userMapper.getUpdate(form);
		return count;
	}
	public int getDelete(UserForm form) {
		int count = userMapper.getDelete(form);
		return count;
	}

}
