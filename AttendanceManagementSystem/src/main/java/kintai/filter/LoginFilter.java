package kintai.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kintai.dao.dto.UserDisplayDto;
@WebFilter(urlPatterns={"/attendanceRegistration/*", "/userDetails/*","/userDisplay" , "/userRegister", "/header", "/analysis/*","/userAttendance","/updatePassword"})
public class LoginFilter implements Filter{


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException{
		HttpServletRequest requests = (HttpServletRequest) request;
		HttpServletResponse responses = (HttpServletResponse) response;

		HttpSession session = requests.getSession();

		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");

		// ログインユーザのセッションがない場合
		if (loginUser == null) {

			List <String> messages = new ArrayList<String>();
			messages.add("セッションタイムアウト");
			session.setAttribute("errorMessage", messages);
			responses.sendRedirect("/AttendanceManagementSystem/login");

		} else {
			chain.doFilter(request, response);
		}
	}//アプリケーション起動時にコールされる。パラメータの初期化などを行う
	public void init(FilterConfig Config) throws ServletException{

	}//アプリケーション起動時にコールされる。パラメータの初期化などを行う
	public void destroy() {

	}
}
