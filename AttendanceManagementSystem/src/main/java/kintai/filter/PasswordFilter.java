package kintai.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import kintai.dao.dto.UserDisplayDto;


@WebFilter(urlPatterns={"/attendanceRegistration", "/userDetails","/userDisplay" , "/userRegister", "/analysis","/header"})
@Component
public class PasswordFilter implements Filter{
	

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException{
		HttpServletRequest requests = (HttpServletRequest) request;
		HttpServletResponse responses = (HttpServletResponse) response;

		HttpSession session = requests.getSession();
        
		UserDisplayDto loginUserInfo = (UserDisplayDto) session.getAttribute("loginUser");
		session.setAttribute("loginUser", loginUserInfo);
		
		if (loginUserInfo.getIsChangePw() == 1) {
			responses.sendRedirect("/AttendanceManagementSystem/updatePassword");
		
		} else {
			
			chain.doFilter(request, response);
		}
	}
	public void init(FilterConfig Config) throws ServletException{
	}
		
	public void destroy() {
	}
	
	

}
