package kintai.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 暗号化ユーティリティー
 */
public class CipherPass {

	/**
	 * SHA-256で暗号化し、バイト配列をBase64エンコーディングします。
	 *
	 * @param target
	 *            暗号化対象の文字列
	 *
	 * @return 暗号化された文字列
	 */
//	public static MessageDigest encrypt(String target) {
//
//		try {
//			MessageDigest md = MessageDigest.getInstance("SHA-256");
//			md.update(target.getBytes());
//			//Base64Utils.encodeToString(md.digest());=>version非対応
//			return md;
//		} catch (NoSuchAlgorithmException e) {
//			throw new RuntimeException(e);
//		}
//	}

	public static String originalEncrypt(String pass) {
		String[] strArray = pass.split("");
		String algo = "";
		for (String s : strArray) {
			String sixteen = main16Algorithum(s);
			String bin2 = main2Algorithum(sixteen);
			algo += bin2;
		}
		int length = algo.length();
		if (length % 6 != 0) {
			int a = length % 6;
			for (int i = 1; i <= a; i++) {
				if (a % 2 == 0) {
					algo += "0";
				} else {
					algo += "1";
				}
			}
		}
		int count = 1;
		String[] algoArray = algo.split("");
		List<String> sixArray = new ArrayList<String>();
		List<String> base64 = new ArrayList<String>();
		for (String s : algoArray) {
			sixArray.add(s);
			count++;
			if (count == 7) {
				String base64Param = sixArray.get(0) + sixArray.get(1) + sixArray.get(2) + sixArray.get(3)
						+ sixArray.get(4) + sixArray.get(5);
				base64.add(base64Param);
				sixArray.clear();
				count = 1;
			}
		}
		//6桁の数字を文字に変換
		String answer = "";
		for (String str : base64) {
			answer += base64Algorithum(str);
		}
		return answer;
	}

	private static String base64Algorithum(String str) {
		String dec = "42";
		if (str.equals("000000")) {
			dec = "A";
		}
		if (str.equals("000001")) {
			dec = "B";
		}
		if (str.equals("000010")) {
			dec = "C";
		}
		if (str.equals("000011")) {
			dec = "D";
		}
		if (str.equals("000100")) {
			dec = "E";
		}
		if (str.equals("000101")) {
			dec = "F";
		}
		if (str.equals("000110")) {
			dec = "G";
		}
		if (str.equals("000111")) {
			dec = "H";
		}
		if (str.equals("001000")) {
			dec = "I";
		}
		if (str.equals("001001")) {
			dec = "J";
		}
		if (str.equals("001010")) {
			dec = "K";
		}
		if (str.equals("001011")) {
			dec = "L";
		}
		if (str.equals("001100")) {
			dec = "M";
		}
		if (str.equals("001101")) {
			dec = "N";
		}
		if (str.equals("001110")) {
			dec = "O";
		}
		if (str.equals("001111")) {
			dec = "P";
		}
		if (str.equals("010000")) {
			dec = "Q";
		}
		if (str.equals("010001")) {
			dec = "R";
		}
		if (str.equals("010010")) {
			dec = "S";
		}
		if (str.equals("010011")) {
			dec = "T";
		}
		if (str.equals("010100")) {
			dec = "U";
		}
		if (str.equals("010101")) {
			dec = "V";
		}
		if (str.equals("010110")) {
			dec = "W";
		}
		if (str.equals("010111")) {
			dec = "X";
		}
		if (str.equals("011000")) {
			dec = "Y";
		}
		if (str.equals("011001")) {
			dec = "Z";
		}
		if (str.equals("011010")) {
			dec = "a";
		}
		if (str.equals("011011")) {
			dec = "b";
		}
		if (str.equals("011100")) {
			dec = "c";
		}
		if (str.equals("011101")) {
			dec = "d";
		}
		if (str.equals("011110")) {
			dec = "e";
		}
		if (str.equals("011111")) {
			dec = "f";
		}
		if (str.equals("100000")) {
			dec = "g";
		}
		if (str.equals("100001")) {
			dec = "h";
		}
		if (str.equals("100010")) {
			dec = "i";
		}
		if (str.equals("100011")) {
			dec = "j";
		}
		if (str.equals("100100")) {
			dec = "k";
		}
		if (str.equals("100101")) {
			dec = "l";
		}
		if (str.equals("100110")) {
			dec = "m";
		}
		if (str.equals("100111")) {
			dec = "n";
		}
		if (str.equals("101000")) {
			dec = "o";
		}
		if (str.equals("101001")) {
			dec = "p";
		}
		if (str.equals("101010")) {
			dec = "q";
		}
		if (str.equals("101011")) {
			dec = "r";
		}
		if (str.equals("101100")) {
			dec = "s";
		}
		if (str.equals("101101")) {
			dec = "t";
		}
		if (str.equals("101110")) {
			dec = "u";
		}
		if (str.equals("101111")) {
			dec = "v";
		}
		if (str.equals("110000")) {
			dec = "w";
		}
		if (str.equals("110001")) {
			dec = "x";
		}
		if (str.equals("110010")) {
			dec = "y";
		}
		if (str.equals("110011")) {
			dec = "z";
		}
		if (str.equals("110100")) {
			dec = "0";
		}
		if (str.equals("110101")) {
			dec = "1";
		}
		if (str.equals("110110")) {
			dec = "2";
		}
		if (str.equals("110111")) {
			dec = "3";
		}
		if (str.equals("111000")) {
			dec = "4";
		}
		if (str.equals("111001")) {
			dec = "5";
		}
		if (str.equals("111010")) {
			dec = "6";
		}
		if (str.equals("111011")) {
			dec = "7";
		}
		if (str.equals("111100")) {
			dec = "8";
		}
		if (str.equals("111101")) {
			dec = "9";
		}
		if (str.equals("111110")) {
			dec = "+";
		}
		if (str.equals("111111")) {
			dec = "/";
		}
		return dec;
	}

	private static String main16Algorithum(String str) {
		int dec = 42;
		if (str.equals("a")) {
			dec = 1;
		}
		if (str.equals("b")) {
			dec = 2;
		}
		if (str.equals("c")) {
			dec = 3;
		}
		if (str.equals("d")) {
			dec = 4;
		}
		if (str.equals("e")) {
			dec = 5;
		}
		if (str.equals("f")) {
			dec = 6;
		}
		if (str.equals("g")) {
			dec = 7;
		}
		if (str.equals("h")) {
			dec = 8;
		}
		if (str.equals("i")) {
			dec = 9;
		}
		if (str.equals("j")) {
			dec = 10;
		}
		if (str.equals("k")) {
			dec = 11;
		}
		if (str.equals("l")) {
			dec = 12;
		}
		if (str.equals("m")) {
			dec = 13;
		}
		if (str.equals("n")) {
			dec = 14;
		}
		if (str.equals("o")) {
			dec = 15;
		}
		if (str.equals("p")) {
			dec = 16;
		}
		if (str.equals("q")) {
			dec = 17;
		}
		if (str.equals("r")) {
			dec = 18;
		}
		if (str.equals("s")) {
			dec = 19;
		}
		if (str.equals("t")) {
			dec = 20;
		}
		if (str.equals("u")) {
			dec = 21;
		}
		if (str.equals("v")) {
			dec = 22;
		}
		if (str.equals("w")) {
			dec = 23;
		}
		if (str.equals("x")) {
			dec = 24;
		}
		if (str.equals("y")) {
			dec = 25;
		}
		if (str.equals("z")) {
			dec = 26;
		}
		if (str.equals("A")) {
			dec = 27;
		}
		if (str.equals("B")) {
			dec = 28;
		}
		if (str.equals("C")) {
			dec = 29;
		}
		if (str.equals("D")) {
			dec = 30;
		}
		if (str.equals("E")) {
			dec = 31;
		}
		if (str.equals("F")) {
			dec = 32;
		}
		if (str.equals("G")) {
			dec = 33;
		}
		if (str.equals("H")) {
			dec = 34;
		}
		if (str.equals("I")) {
			dec = 35;
		}
		if (str.equals("J")) {
			dec = 36;
		}
		if (str.equals("K")) {
			dec = 37;
		}
		if (str.equals("L")) {
			dec = 38;
		}
		if (str.equals("M")) {
			dec = 39;
		}
		if (str.equals("N")) {
			dec = 40;
		}
		if (str.equals("O")) {
			dec = 41;
		}
		if (str.equals("P")) {
			dec = 42;
		}
		if (str.equals("Q")) {
			dec = 43;
		}
		if (str.equals("R")) {
			dec = 44;
		}
		if (str.equals("S")) {
			dec = 45;
		}
		if (str.equals("T")) {
			dec = 46;
		}
		if (str.equals("U")) {
			dec = 47;
		}
		if (str.equals("V")) {
			dec = 48;
		}
		if (str.equals("W")) {
			dec = 49;
		}
		if (str.equals("X")) {
			dec = 50;
		}
		if (str.equals("Y")) {
			dec = 51;
		}
		if (str.equals("Z")) {
			dec = 52;
		}
		if (str.equals("0")) {
			dec = 53;
		}
		if (str.equals("1")) {
			dec = 54;
		}
		if (str.equals("2")) {
			dec = 55;
		}
		if (str.equals("3")) {
			dec = 56;
		}
		if (str.equals("4")) {
			dec = 57;
		}
		if (str.equals("5")) {
			dec = 58;
		}
		if (str.equals("6")) {
			dec = 59;
		}
		if (str.equals("7")) {
			dec = 60;
		}
		if (str.equals("8")) {
			dec = 61;
		}
		if (str.equals("9")) {
			dec = 62;
		}
		String bin = Integer.toHexString(dec);
		return bin;
	}

	private static String main2Algorithum(String str) {
		int dec = 42;
		if (str.equals("a")) {
			dec = 1;
		}
		if (str.equals("b")) {
			dec = 2;
		}
		if (str.equals("c")) {
			dec = 3;
		}
		if (str.equals("d")) {
			dec = 4;
		}
		if (str.equals("e")) {
			dec = 5;
		}
		if (str.equals("f")) {
			dec = 6;
		}
		if (str.equals("g")) {
			dec = 7;
		}
		if (str.equals("h")) {
			dec = 8;
		}
		if (str.equals("i")) {
			dec = 9;
		}
		if (str.equals("j")) {
			dec = 10;
		}
		if (str.equals("k")) {
			dec = 11;
		}
		if (str.equals("l")) {
			dec = 12;
		}
		if (str.equals("m")) {
			dec = 13;
		}
		if (str.equals("n")) {
			dec = 14;
		}
		if (str.equals("o")) {
			dec = 15;
		}
		if (str.equals("p")) {
			dec = 16;
		}
		if (str.equals("q")) {
			dec = 17;
		}
		if (str.equals("r")) {
			dec = 18;
		}
		if (str.equals("s")) {
			dec = 19;
		}
		if (str.equals("t")) {
			dec = 20;
		}
		if (str.equals("u")) {
			dec = 21;
		}
		if (str.equals("v")) {
			dec = 22;
		}
		if (str.equals("w")) {
			dec = 23;
		}
		if (str.equals("x")) {
			dec = 24;
		}
		if (str.equals("y")) {
			dec = 25;
		}
		if (str.equals("z")) {
			dec = 26;
		}
		if (str.equals("A")) {
			dec = 27;
		}
		if (str.equals("B")) {
			dec = 28;
		}
		if (str.equals("C")) {
			dec = 29;
		}
		if (str.equals("D")) {
			dec = 30;
		}
		if (str.equals("E")) {
			dec = 31;
		}
		if (str.equals("F")) {
			dec = 32;
		}
		if (str.equals("G")) {
			dec = 33;
		}
		if (str.equals("H")) {
			dec = 34;
		}
		if (str.equals("I")) {
			dec = 35;
		}
		if (str.equals("J")) {
			dec = 36;
		}
		if (str.equals("K")) {
			dec = 37;
		}
		if (str.equals("L")) {
			dec = 38;
		}
		if (str.equals("M")) {
			dec = 39;
		}
		if (str.equals("N")) {
			dec = 40;
		}
		if (str.equals("O")) {
			dec = 41;
		}
		if (str.equals("P")) {
			dec = 42;
		}
		if (str.equals("Q")) {
			dec = 43;
		}
		if (str.equals("R")) {
			dec = 44;
		}
		if (str.equals("S")) {
			dec = 45;
		}
		if (str.equals("T")) {
			dec = 46;
		}
		if (str.equals("U")) {
			dec = 47;
		}
		if (str.equals("V")) {
			dec = 48;
		}
		if (str.equals("W")) {
			dec = 49;
		}
		if (str.equals("X")) {
			dec = 50;
		}
		if (str.equals("Y")) {
			dec = 51;
		}
		if (str.equals("Z")) {
			dec = 52;
		}
		if (str.equals("0")) {
			dec = 53;
		}
		if (str.equals("1")) {
			dec = 54;
		}
		if (str.equals("2")) {
			dec = 55;
		}
		if (str.equals("3")) {
			dec = 56;
		}
		if (str.equals("4")) {
			dec = 57;
		}
		if (str.equals("5")) {
			dec = 58;
		}
		if (str.equals("6")) {
			dec = 59;
		}
		if (str.equals("7")) {
			dec = 60;
		}
		if (str.equals("8")) {
			dec = 61;
		}
		if (str.equals("9")) {
			dec = 62;
		}
		String bin = Integer.toBinaryString(dec);
		return bin;
	}
}