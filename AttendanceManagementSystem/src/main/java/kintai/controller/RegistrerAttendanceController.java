package kintai.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kintai.dao.dto.AttendanceDto;
import kintai.dao.dto.DutyDivisionDto;
import kintai.dao.dto.UserDetailsDto;
import kintai.dao.dto.UserDisplayDto;
import kintai.dao.dto.csvDto;
import kintai.form.AllTimesForm;
import kintai.form.AttendanceForm;
import kintai.form.SelectMonthForm;
import kintai.service.RegistrerAttendanceService;
import kintai.service.UserDetailsService;
import kintai.validation.KintaiValidation;

@Controller
public class RegistrerAttendanceController {
	@Autowired
	private RegistrerAttendanceService registrerAttendanceService;
	@Autowired
	private HttpSession session;
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private HttpServletRequest request;

	@RequestMapping(value = "/attendanceRegistration", method = RequestMethod.GET )
	public String registrerAttendance(Model model, SelectMonthForm form, @RequestParam(defaultValue = "0") int id) {

		// セッションからログインユーザ情報の取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		model.addAttribute("loginUser", loginUser);

		//未ログインユーザはログイン画面へ遷移させる
		if (session == null){
			return "redirect:login";
			} else if(loginUser == null) {
				return "redirect:login";
			}
		List<AttendanceDto> attendances = new ArrayList<AttendanceDto>();

		// カレンダー取得(今月のみ)
		Calendar cal = Calendar.getInstance();
		Calendar calendar = Calendar.getInstance();
		String mon = String.valueOf(calendar.get(Calendar.MONTH)+1);
		String date = String.valueOf(calendar.get(Calendar.DATE));
		int day = cal.getActualMaximum(Calendar.DATE);
		model.addAttribute("day", day);
		model.addAttribute("mon", mon);
		model.addAttribute("date", date);

		//勤怠表、セレクトボックスの初期表示
		List<SelectMonthForm> selectMonthPullDowns = new ArrayList<SelectMonthForm>();
		int nowYear = cal.get(Calendar.YEAR); //nowYear=現在の年
		int nowMonth = cal.get(Calendar.MONTH) +1; //nowMonth=現在の月
		String isPrevMonth = request.getParameter("isPrevMonth");
		String isNextMonth = request.getParameter("isNextMonth");
		String isUpdateMonth = request.getParameter("isUpdateMonth");
		SelectMonthForm selectMonth = (SelectMonthForm) session.getAttribute("selectMonth");
		if (selectMonth == null) { //初期表示の場合
			SelectMonthForm selectMonth2 = new SelectMonthForm();
			selectMonth2.setYearKey(nowYear); //yearKey=現在の年
			selectMonth2.setMonthKey(nowMonth); //monthKey=現在の月
			session.setAttribute("selectMonth", selectMonth2); //sessionに値を保持
		}

		//月更新の処理
		if (selectMonth != null) { //初期表示ではない場合
			nowYear = selectMonth.getYearKey(); //yearKey=表示中の年
			nowMonth = selectMonth.getMonthKey(); //monthKey=表示中の月
			if("isUpdateMonth".equals(isUpdateMonth)) {
				nowMonth = form.getMonthKey(); //form.getMonthKey=選択した月
				selectMonth.setMonthKey(nowMonth);
			}
			if("isPrevMonth".equals(isPrevMonth)) {
				selectMonth.setMonthKey(nowMonth-1);
				if (nowMonth == 1) { //1月の前月が12月になるように設定
					nowMonth = 12;
					selectMonth.setMonthKey(nowMonth);
					selectMonth.setYearKey(nowYear-1); //1月の前月操作時は年を-1
				}
			}else if("isNextMonth".equals(isNextMonth)) {
				selectMonth.setMonthKey(nowMonth+1);
				if (nowMonth == 12) { //12月の次月が1月になるように設定
					nowMonth = 1;
					selectMonth.setMonthKey(nowMonth);
					selectMonth.setYearKey(nowYear+1); //12月の次月操作時は年を+1
				}
			}
		}

		//セレクトボックスの表示（1月～12月）
		Date timeSet = new Date();
		cal.setTime(timeSet);
		cal.set(Calendar.MONTH, 0);
		//セレクトボックスの初期表示
		if (selectMonth == null) {
			for (int i = 1; i <= 12; i++) {
				SelectMonthForm selectMonthPullDown = new SelectMonthForm();
				selectMonthPullDown.setNowMonth2(nowMonth); //nouMonth2=現在の月
				selectMonthPullDown.setMonthKey(i);
				selectMonthPullDown.setYearKey(nowYear); //yearKey=現在の年
				selectMonthPullDown.setNowYear(nowYear); //nowYear=現在の年
				timeSet = cal.getTime();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy'年'MM'月'"); //（現在の年）年（1～12）月にフォーマット設定
				selectMonthPullDown.setMonthName2(sdf.format(timeSet)); //monthName2=（現在の年）年（1～12）月の文字列
				cal.add(Calendar.MONTH, 1); //formにセットした後に月を+1
				selectMonthPullDowns.add(selectMonthPullDown);
			}
		}
		//選択した年月のセレクトボックス内更新処理
		if (selectMonth != null) {
			nowYear = selectMonth.getYearKey(); //noyYear=表示中の年
			nowMonth = selectMonth.getMonthKey(); //nowMonth=表示中の月
			//カレンダークラスの年を更新
			int updateYear = nowYear - cal.get(Calendar.YEAR); //何年分更新するか計算 updateYear=表示中の年-現在の年 例）2021-2020=1、2019-2020=-1
			cal.add(Calendar.YEAR, updateYear); //現在の年に更新する年数分プラス
			for (int i = 1; i <= 12; i++) {
				SelectMonthForm selectMonthPullDown = new SelectMonthForm();
				selectMonthPullDown.setNowMonth2(nowMonth); //nouMonth2=表示中の月
				selectMonthPullDown.setMonthKey(i);
				selectMonthPullDown.setYearKey(nowYear); //yearKey=表示中の年
				selectMonthPullDown.setNowYear(nowYear); //yearKey=表示中の年
				timeSet = cal.getTime();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy'年'MM'月'"); //（表示中の年）年（1～12）月にフォーマット設定
				selectMonthPullDown.setMonthName2(sdf.format(timeSet)); //monthName2=（表示中の年）年（1～12）月の文字列
				cal.add(Calendar.MONTH, 1); //formにセットした後に月を+1
				selectMonthPullDowns.add(selectMonthPullDown);
			}
		}
		model.addAttribute("selectMonthPullDowns", selectMonthPullDowns); //プルダウン設置
		model.addAttribute("selectMonth",selectMonth);

		//選択年月の日数取得
		String monthName = String.valueOf(cal.get(Calendar.MONTH) +1);
		int maxDate = cal.getActualMaximum(Calendar.DATE);
		monthName = String.valueOf(nowMonth);
		cal.set(Calendar.MONTH, nowMonth - 1);
		maxDate = cal.getActualMaximum(Calendar.DATE);
		List<AttendanceForm> aprilCalendar = new ArrayList<AttendanceForm>();
		cal.setTimeInMillis(timeSet.getTime());
		cal.set(Calendar.MONTH, nowMonth - 1);
	    cal.set(Calendar.DAY_OF_MONTH,1);
	    SimpleDateFormat sdf = new SimpleDateFormat("M'/'d'('E')'");
		for (int i =1; i <= maxDate; ++i) {
			AttendanceForm AttendanceBeen = new AttendanceForm();
			timeSet = cal.getTime();
			AttendanceBeen.setCalendar(sdf.format(timeSet));
			cal.add(Calendar.DAY_OF_MONTH, 1);
			AttendanceBeen.setMatchMonthKey(nowMonth);
			AttendanceBeen.setMatchDayKey(i);
			AttendanceBeen.setMatchYearKey(nowYear);
			aprilCalendar.add(AttendanceBeen);
		}
		//管理者権限のユーザが他ユーザの勤怠画面を表示
		try {
			if (id == 0) {
				id = loginUser.getId();
				model.addAttribute("users", loginUser);
			} else {
				//UserDetailsDtoからユーザ一覧情報の取得
				UserDetailsDto users = userDetailsService.getUserDetails(id);
				model.addAttribute("users", users);
			}
			attendances = registrerAttendanceService.getAttendance(id);
		}catch(NullPointerException e) {
			return "redirect:login";
		}
		for (int i = 0; i < attendances.size(); ++i) {
			for (int n = 0; n < aprilCalendar.size(); ++n) {
				//セッションから入力エラーform情報の取得
				AttendanceForm attendanceForm = (AttendanceForm) session.getAttribute("attendanceForm");
				//セッション情報に入っている入力エラーのフォームの年月日がカレンダーの年月日と一致する場合をチェック
				if (attendanceForm != null && (attendanceForm.getMatchMonthKey() == aprilCalendar.get(n).getMatchMonthKey()
						&& attendanceForm.getMatchDayKey() == aprilCalendar.get(n).getMatchDayKey()
						&& attendanceForm.getMatchYearKey() == aprilCalendar.get(n).getMatchYearKey())) {
				aprilCalendar.get(n).setId(attendanceForm.getId());
				aprilCalendar.get(n).setDutyDivisionId(attendanceForm.getDutyDivisionId());
				aprilCalendar.get(n).setStartHourTime(attendanceForm.getStartHourTime());
				aprilCalendar.get(n).setStartMinuteTime(attendanceForm.getStartMinuteTime());
				aprilCalendar.get(n).setEndHourTime(attendanceForm.getEndHourTime());
				aprilCalendar.get(n).setEndMinuteTime(attendanceForm.getEndMinuteTime());
				aprilCalendar.get(n).setBreakHourTimeStart(attendanceForm.getBreakHourTimeStart());
				aprilCalendar.get(n).setBreakMinuteTimeStart(attendanceForm.getBreakMinuteTimeStart());
				aprilCalendar.get(n).setBreakHourTimeEnd(attendanceForm.getBreakHourTimeEnd());
				aprilCalendar.get(n).setBreakMinuteTimeEnd(attendanceForm.getBreakMinuteTimeEnd());
				aprilCalendar.get(n).setActualWorkingHours(attendanceForm.getActualWorkingHours());
				aprilCalendar.get(n).setRestTime(attendanceForm.getRestTime());
				aprilCalendar.get(n).setLateNightTime(attendanceForm.getLateNightTime());
				aprilCalendar.get(n).setRemarks(attendanceForm.getRemarks());
				aprilCalendar.get(n).setApproval1(attendanceForm.getApproval1());
				aprilCalendar.get(n).setApproval2(attendanceForm.getApproval2());
				continue; // エラー値をセットしたら次の日付の処理へスキップ
				}
				if ((aprilCalendar.get(n).getMatchDayKey() == attendances.get(i).getMatchDayKey())
						&& (aprilCalendar.get(n).getMatchMonthKey() == attendances.get(i).getMatchMonthKey())
						&& (aprilCalendar.get(n).getMatchYearKey() == attendances.get(i).getMatchYearKey())) {
				aprilCalendar.get(n).setId(attendances.get(i).getId());
				aprilCalendar.get(n).setDutyDivisionId(attendances.get(i).getDutyDivisionId());
				aprilCalendar.get(n).setStartHourTime(attendances.get(i).getStartHourTime());
				aprilCalendar.get(n).setStartMinuteTime(attendances.get(i).getStartMinuteTime());
				aprilCalendar.get(n).setEndHourTime(attendances.get(i).getEndHourTime());
				aprilCalendar.get(n).setEndMinuteTime(attendances.get(i).getEndMinuteTime());
				aprilCalendar.get(n).setBreakHourTimeStart(attendances.get(i).getBreakHourTimeStart());
				aprilCalendar.get(n).setBreakMinuteTimeStart(attendances.get(i).getBreakMinuteTimeStart());
				aprilCalendar.get(n).setBreakHourTimeEnd(attendances.get(i).getBreakHourTimeEnd());
				aprilCalendar.get(n).setBreakMinuteTimeEnd(attendances.get(i).getBreakMinuteTimeEnd());
				aprilCalendar.get(n).setActualWorkingHours(attendances.get(i).getActualWorkingHours());
				aprilCalendar.get(n).setRestTime(attendances.get(i).getRestTime());
				aprilCalendar.get(n).setLateNightTime(attendances.get(i).getLateNightTime());
				aprilCalendar.get(n).setRemarks(attendances.get(i).getRemarks());
				aprilCalendar.get(n).setApproval1(attendances.get(i).getApproval1());
				aprilCalendar.get(n).setApproval2(attendances.get(i).getApproval2());
					if(aprilCalendar.get(n).getStartHourTime().equals("99") && aprilCalendar.get(n).getStartMinuteTime().equals("99") && aprilCalendar.get(n).getEndHourTime().equals("99") && aprilCalendar.get(n).getEndMinuteTime().equals("99") && aprilCalendar.get(n).getBreakHourTimeStart().equals("99") && aprilCalendar.get(n).getBreakMinuteTimeStart().equals("99") && aprilCalendar.get(n).getBreakHourTimeEnd().equals("99") && aprilCalendar.get(n).getBreakMinuteTimeStart().equals("99")){
						aprilCalendar.get(n).setStartHourTime("");
						aprilCalendar.get(n).setStartMinuteTime("");
						aprilCalendar.get(n).setEndHourTime("");
						aprilCalendar.get(n).setEndMinuteTime("");
						aprilCalendar.get(n).setBreakHourTimeStart("");
						aprilCalendar.get(n).setBreakMinuteTimeStart("");
						aprilCalendar.get(n).setBreakHourTimeEnd("");
						aprilCalendar.get(n).setBreakMinuteTimeEnd("");
					}
				}
			}
		}
		model.addAttribute("attendances",aprilCalendar);

		//総時間の計算(合計勤務時間, 深夜, 休憩時間)
		AllTimesForm allTime = new AllTimesForm();
		double workTime = 0;
		double nightTime = 0;
		double restTime = 0;
		for(int i = 0;  i < aprilCalendar.size(); ++i	) {
			workTime = workTime + aprilCalendar.get(i).getActualWorkingHours();
			nightTime = nightTime + aprilCalendar.get(i).getLateNightTime();
			restTime = restTime + aprilCalendar.get(i).getRestTime();
		}

		// 実働時間が0でないとき、勤務日数を加算する
		double allWorkingDay = 0;
		for (int i = 0; i < aprilCalendar.size(); ++i) {
			if (aprilCalendar.get(i).getActualWorkingHours() != 0) {
				allWorkingDay = allWorkingDay + 1;
			}
		}

		//総時間の配置(合計勤務時間, 深夜, 休憩時間, 合計勤務日数)
		allTime.setAllActualWorkingTime(workTime);
		allTime.setAllNightTime(nightTime);
		allTime.setAllRestTime(restTime);
		allTime.setAllWorkingDay(allWorkingDay);

		// 合計勤務時間を表示
		model.addAttribute("allTime",allTime);

		//勤怠区分マスタから値取得
		List<DutyDivisionDto> dutyDivisions = new ArrayList<DutyDivisionDto>();
		dutyDivisions = registrerAttendanceService.getDutyDivision();
		model.addAttribute("dutyDivisions", dutyDivisions);

		//勤怠編集処理のForm作成
		AttendanceForm updateForm = new AttendanceForm();
        model.addAttribute("attendanceUpdate",updateForm);

		// 入力フォームの表示
		AttendanceForm insertForm = new AttendanceForm();
		model.addAttribute("AttendanceForm", insertForm);
		model.addAttribute("monthName" , monthName);
		model.addAttribute("yearName" , nowYear);

		AttendanceForm approvalForm1 = new AttendanceForm();
		model.addAttribute("Approval1", approvalForm1);

		AttendanceForm approvalForm2 = new AttendanceForm();
		model.addAttribute("Approval2", approvalForm2);

		session.removeAttribute("attendanceForm");
		return "attendanceRegistration";
		}

	@RequestMapping(value = "/attendanceInsert", method = RequestMethod.POST)
	public String registerInsert(@ModelAttribute AttendanceForm form, Model model, @RequestParam ("createUser")int id) {
		session.removeAttribute("errMessage");

		// エラー処理
		List<String> errMessage = new ArrayList<String>();
		errMessage = KintaiValidation.inputValidation(form, errMessage) ;
		int dutyDivisionId = form.getDutyDivisionId();
		if (!errMessage.isEmpty()) {
			session.setAttribute("errMessage", errMessage);
			session.setAttribute("attendanceForm", form);
			return "redirect:attendanceRegistration" + "?id=" + id;
		}
		if (dutyDivisionId == 2 || dutyDivisionId == 3 || dutyDivisionId == 4 || dutyDivisionId == 6 || dutyDivisionId == 7 || dutyDivisionId == 8) {
			form.setStartHourTime("99");
			form.setStartMinuteTime("99");
			form.setEndHourTime("99");
			form.setEndMinuteTime("99");
			form.setBreakHourTimeStart("99");
			form.setBreakMinuteTimeStart("99");
			form.setBreakHourTimeEnd("99");
			form.setBreakMinuteTimeEnd("99");
		}
		registrerAttendanceService.insertDuty(form);
		return "redirect:attendanceRegistration" + "?id=" + id;
	}

	@RequestMapping(value = "/attendanceUpdate", method = RequestMethod.POST)
	public String attendanceUpdate(Model model, @ModelAttribute AttendanceForm form,@RequestParam ("updateUser")int id) {

		// エラー処理
		List<String> errMessage = new ArrayList<String>();
		errMessage = KintaiValidation.inputValidation(form, errMessage) ;
		int dutyDivisionId = form.getDutyDivisionId();

		// エラーメッセージがあれば表示する。
		if (!errMessage.isEmpty()) {
			session.setAttribute("errMessage", errMessage);
			session.setAttribute("attendanceForm", form);
			return "redirect:attendanceRegistration" + "?id=" + id;
		}
		if (dutyDivisionId == 2 || dutyDivisionId == 3 || dutyDivisionId == 4 || dutyDivisionId == 6 || dutyDivisionId == 7 || dutyDivisionId == 8) {
			form.setStartHourTime("99");
			form.setStartMinuteTime("99");
			form.setEndHourTime("99");
			form.setEndMinuteTime("99");
			form.setBreakHourTimeStart("99");
			form.setBreakMinuteTimeStart("99");
			form.setBreakHourTimeEnd("99");
			form.setBreakMinuteTimeEnd("99");
		}
		AttendanceDto dto = new AttendanceDto();
		BeanUtils.copyProperties(form, dto);
		registrerAttendanceService.updateDuty(dto);
		return "redirect:attendanceRegistration" + "?id=" + id;
	}

	@RequestMapping(value = "/approvalForm1", method = RequestMethod.POST)
	public String approvalForm1(Model model, @ModelAttribute AttendanceForm form,@RequestParam ("createUser")int id) {
		session.getAttribute("loginUser");

		//始業年月までの取得
		Calendar cal = Calendar.getInstance();
		String monthName = String.valueOf(cal.get(Calendar.MONTH) +1);
		String yearName = String.valueOf(cal.get(Calendar.YEAR));
		SelectMonthForm selectMonth = (SelectMonthForm) session.getAttribute("selectMonth");
		if(selectMonth != null) {
			monthName = String.valueOf(selectMonth.getMonthKey());
			yearName = String.valueOf(selectMonth.getYearKey());
		}
		int monthName1= Integer.parseInt(monthName);
		if (monthName1>=1 && monthName1<= 9) {
			monthName = yearName + 0 + monthName;
        } else if (monthName1>= 10 && monthName1<= 12) {
			monthName = yearName + monthName;
        }
		form.setMatchMonthKey(Integer.parseInt(monthName));
		AttendanceDto dto = new AttendanceDto();
		BeanUtils.copyProperties(form, dto);
		registrerAttendanceService.approvalForm1(dto);
		return "redirect:attendanceRegistration" + "?id=" + id;
		}

	@RequestMapping(value = "/approvalForm2", method = RequestMethod.POST)
	public String approvalForm2(Model model, @ModelAttribute AttendanceForm form,@RequestParam ("createUser")int id) {
		session.getAttribute("loginUser");

		//始業年月までの取得
		Calendar cal = Calendar.getInstance();
		String monthName = String.valueOf(cal.get(Calendar.MONTH) +1);
		String yearName = String.valueOf(cal.get(Calendar.YEAR));
		SelectMonthForm selectMonth = (SelectMonthForm) session.getAttribute("selectMonth");
		if(selectMonth != null) {
			monthName = String.valueOf(selectMonth.getMonthKey());
			yearName = String.valueOf(selectMonth.getYearKey());
		}
		int monthName1= Integer.parseInt(monthName);
		if (monthName1>=1 && monthName1<= 9) {
			monthName = yearName + 0 + monthName;
        } else if (monthName1>= 10 && monthName1<= 12) {
			monthName = yearName + monthName;
        }
		form.setMatchMonthKey(Integer.parseInt(monthName));
		AttendanceDto dto = new AttendanceDto();
		BeanUtils.copyProperties(form, dto);
		registrerAttendanceService.approvalForm2(dto);
		return "redirect:attendanceRegistration" + "?id=" + id;
		}

	@RequestMapping(value = "/selectMonth", method = RequestMethod.POST)
	public String selectMonth(@ModelAttribute SelectMonthForm form, Model model) {
		session.removeAttribute("errMessage");
		session.setAttribute("selectMonth", form);
		return "redirect:attendanceRegistration";
	}

	@RequestMapping(value = "/csvDownloads", method = RequestMethod.GET)
    public String csvDownload(HttpServletRequest request, HttpServletResponse response , @ModelAttribute SelectMonthForm form,Model model,
    		@RequestParam(defaultValue = "0") int id){

    	//文字コードと出力するCSVファイル名を設定
    	response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE + ";charset=SJIS");
    	response.setHeader("Content-Disposition", "attachment; filename=\"attendance.csv\"");

    	// カレンダー取得
    	Calendar cal = Calendar.getInstance();
    	int year = cal.get(Calendar.YEAR);
    	int month = cal.get(Calendar.MONTH) +1;

    	//月を更新した際に表示月の値を取得
    	SelectMonthForm selectMonth = (SelectMonthForm) session.getAttribute("selectMonth");
		if(selectMonth != null) {
			month = selectMonth.getMonthKey();
			year = selectMonth.getYearKey();
		}

    	//表示月の日付取得
    	if(month > 12) {
			month = 12;
			}
			if(month < 1) {
			month = 1;
			}
		cal.set(Calendar.MONTH, month - 1);
    	int maxDate = cal.getActualMaximum(Calendar.DATE);

    	List<AttendanceDto> threeInfo;
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		String employeeNumber;
		String name;
			if (id == 0) {
				id = loginUser.getId();
				//出力する名前等をログインユーザで設定
				employeeNumber = loginUser.getEmployeeNumber();
				name = loginUser.getName();
			} else {
				//UserDetailsDtoからユーザ一覧情報の取得
				UserDetailsDto users = userDetailsService.getUserDetails(id);
				id = users.getId();
				//出力する名前等を表示ユーザで設定
				employeeNumber = users.getEmployeeNumber();
				name = users.getName();
			}
			//取得したユーザの勤怠情報を登録
			threeInfo = registrerAttendanceService.getAttendance(id);

			//出力させるためのフォーマットを作成
    		List<csvDto> csvForms = new ArrayList<csvDto>();
    		for (int i= 1; i<=maxDate; i++ ){
        		csvDto csvForm = new csvDto();
        		csvForm.setEmployeeNumber(employeeNumber);
        		csvForm.setName(name);
        		String yearName = String.format("%d", year);
        		String monthName = String.format("%02d", month);
        		String dateName = String.format("%02d", i);
        		csvForm.setDays(yearName + monthName + dateName);
        		csvForm.setMatchYearKey(year);
        		csvForm.setMatchMonthKey(month);
        		csvForm.setMatchDayKey(i);
        		csvForms.add(csvForm);
    		}

    		//threeInfo内の月日とセットしたフォーマットのの月日が同じなら勤怠情報を取得し、csvFormsにセット
    		for(int i = 0; i < csvForms.size(); i++) {
    			for(int n = 0; n < threeInfo.size(); n++) {
    				if(threeInfo.get(n).getMatchMonthKey() == csvForms.get(i).getMatchMonthKey() &&
    						threeInfo.get(n).getMatchDayKey() == csvForms.get(i).getMatchDayKey()
    						&& threeInfo.get(n).getMatchYearKey() == csvForms.get(i).getMatchYearKey()){

    					csvForms.get(i).setDutyDivision(threeInfo.get(n).getDutyDivision());
    					//nullだった場合は空白に変換
    					csvForms.get(i).setStartHourTime(threeInfo.get(n).getStartHourTime() + ":") ;
    					//欠勤などで始業時間が"99:"だった場合、空白で表示する
    					if(csvForms.get(i).getStartHourTime().equals("99:")) {
    						csvForms.get(i).setStartHourTime("");
    					}
    					csvForms.get(i).setStartMinuteTime(threeInfo.get(n).getStartMinuteTime()) ;
    					//欠勤などで始業分が99だった場合、空白で表示する
    					if(csvForms.get(i).getStartMinuteTime().equals("99")) {
    						csvForms.get(i).setStartMinuteTime("");
    					}
    					csvForms.get(i).setEndHourTime(threeInfo.get(n).getEndHourTime() + ":") ;
    					if(csvForms.get(i).getEndHourTime().equals("99:")) {
    						csvForms.get(i).setEndHourTime("");
    					}
    					csvForms.get(i).setEndMinuteTime(threeInfo.get(n).getEndMinuteTime()) ;
    					if(csvForms.get(i).getEndMinuteTime().equals("99")) {
    						csvForms.get(i).setEndMinuteTime("");
    					}
    					csvForms.get(i).setBreakHourTimeStart(threeInfo.get(n).getBreakHourTimeStart() + ":") ;
    					if(csvForms.get(i).getBreakHourTimeStart().equals("99:")) {
    						csvForms.get(i).setBreakHourTimeStart("");
    					}
    					csvForms.get(i).setBreakMinuteTimeStart(threeInfo.get(n).getBreakMinuteTimeStart()) ;
    					if(csvForms.get(i).getBreakMinuteTimeStart().equals("99")) {
    						csvForms.get(i).setBreakMinuteTimeStart("");
    					}
    					csvForms.get(i).setBreakHourTimeEnd(threeInfo.get(n).getBreakHourTimeEnd() + ":") ;
    					if(csvForms.get(i).getBreakHourTimeEnd().equals("99:")) {
    						csvForms.get(i).setBreakHourTimeEnd("");
    					}
    					csvForms.get(i).setBreakMinuteTimeEnd(threeInfo.get(n).getBreakMinuteTimeEnd()) ;
    					if(csvForms.get(i).getBreakMinuteTimeEnd().equals("99")) {
    						csvForms.get(i).setBreakMinuteTimeEnd("");
    					}
    					csvForms.get(i).setActualWorkingHoursString(String.valueOf(threeInfo.get(n).getActualWorkingHours())) ;
    					//勤務時間が"0.0"の場合は空白で表示
    					if(csvForms.get(i).getActualWorkingHoursString().equals("0.0")) {
    						csvForms.get(i).setActualWorkingHoursString("");
    					}
    					csvForms.get(i).setLateNightTimeString(String.valueOf(threeInfo.get(i).getLateNightTime())) ;
    					if(csvForms.get(i).getLateNightTimeString().equals("0.0")) {
    						csvForms.get(i).setLateNightTimeString("");
    					}
    					csvForms.get(i).setRestTimeString(String.valueOf(threeInfo.get(i).getRestTime())) ;
    					if(csvForms.get(i).getRestTimeString().equals("0.0")) {
    						csvForms.get(i).setRestTimeString("");
    					}
    					csvForms.get(i).setRemarks(threeInfo.get(n).getRemarks()) ;
    					csvForms.get(i).setApproval1(threeInfo.get(n).getApproval1()) ;
    					// true,folseを0,1で表示
    					if(csvForms.get(i).getApproval1() == true) {
    						csvForms.get(i).setApprovalString1("1");
    					} else {
    						csvForms.get(i).setApprovalString1("0");
    					}
    					csvForms.get(i).setApproval2(threeInfo.get(n).getApproval2()) ;
    					if(csvForms.get(i).getApproval2() == true) {
    						csvForms.get(i).setApprovalString2("1");
    					} else {
    						csvForms.get(i).setApprovalString2("0");
    					}
    				}
    			}
    		}

    		//CSV出力処理
    		try (PrintWriter pw = response.getWriter()) {
    			//1行目の出力
        		String headerString = "社員番号" + "," + "名前" + "," + "年月日" + "," +"勤務区分" + ","
        				+ "始業時間" + "," + "終業時間" + "," + "休憩開始" + "," + "休憩終了" + "," + "実働時間" + ","
        				+ "休憩時間" + "," + "深夜時間" + "," + "備考" + "," + "承認1" + "," + "承認2" +"\r\n";
        		pw.print(headerString);

        		//勤怠情報の出力
        		for(int i = 0; i < csvForms.size(); i++) {
        			//nullだった場合は空白に変換
        			if (csvForms.get(i).getDutyDivision() == null){
        				csvForms.get(i).setDutyDivision("");
        			}
        			if (csvForms.get(i).getStartHourTime() == null){
        				csvForms.get(i).setStartHourTime("");
        			}
        			if (csvForms.get(i).getStartMinuteTime() == null){
        				csvForms.get(i).setStartMinuteTime("");
        			}
        			if (csvForms.get(i).getEndHourTime() == null){
        				csvForms.get(i).setEndHourTime("");
						}
        			if (csvForms.get(i).getEndMinuteTime() == null){
        				csvForms.get(i).setEndMinuteTime("");
        			}
        			if (csvForms.get(i).getBreakHourTimeStart() == null){
        				csvForms.get(i).setBreakHourTimeStart("");
        			}
        			if (csvForms.get(i).getBreakMinuteTimeStart() == null){
        				csvForms.get(i).setBreakMinuteTimeStart("");
        			}
        			if (csvForms.get(i).getBreakHourTimeEnd() == null){
						csvForms.get(i).setBreakHourTimeEnd("");
        			}
        			if (csvForms.get(i).getBreakMinuteTimeEnd() == null){
        				csvForms.get(i).setBreakMinuteTimeEnd("");
        			}
        			if (csvForms.get(i).getActualWorkingHoursString() == null){
        				csvForms.get(i).setActualWorkingHoursString("");
        			}
        			if (csvForms.get(i).getLateNightTimeString() == null){
        				csvForms.get(i).setLateNightTimeString("");
        			}
        			if (csvForms.get(i).getRestTimeString() == null){
        				csvForms.get(i).setRestTimeString("");
        			}
        			if (csvForms.get(i).getRemarks() == null){
						csvForms.get(i).setRemarks("");
        			}
        			if (csvForms.get(i).getApprovalString1() == null){
						csvForms.get(i).setApprovalString1("");
        			}
        			if (csvForms.get(i).getApprovalString2() == null){
						csvForms.get(i).setApprovalString2("");
        			}
        			String outputString = csvForms.get(i).getEmployeeNumber() + "," + csvForms.get(i).getName() + ","
        					+ csvForms.get(i).getDays() + "," + csvForms.get(i).getDutyDivision() + ","
        					+ csvForms.get(i).getStartHourTime() + csvForms.get(i).getStartMinuteTime() + ","
        					+ csvForms.get(i).getEndHourTime() + csvForms.get(i).getEndMinuteTime() + ","
        					+ csvForms.get(i).getBreakHourTimeStart() + csvForms.get(i).getBreakMinuteTimeStart() + ","
        					+ csvForms.get(i).getBreakHourTimeEnd() + csvForms.get(i).getBreakMinuteTimeEnd() + ","
        					+ csvForms.get(i).getActualWorkingHoursString() + ","+ csvForms.get(i).getLateNightTimeString() + ","
        					+ csvForms.get(i).getRestTimeString() + "," + csvForms.get(i).getRemarks() + ","
        					+ csvForms.get(i).getApprovalString1() + "," + csvForms.get(i).getApprovalString2() + ","
        					+ "\r\n" ;

        			pw.print(outputString);
    		}
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	return "attendanceRegistration";
    }
}

 /*   	try {
    		StringBuilder CSV= new StringBuilder();
    		AttendanceDto a = new AttendanceDto();
    		CSV.append(a.getDutyDivision());
    		CSV.append(a.getStartHourTime());
    		CSV.append(a.getStartMinuteTime());
    		CSV.append(a.getEndHourTime());
    		CSV.append(a.getEndMinuteTime());
    		CSV.append(a.getBreakHourTimeStart());
    		CSV.append(a.getBreakMinuteTimeStart());
    		CSV.append(a.getBreakHourTimeEnd());
    		CSV.append(a.getBreakMinuteTimeEnd());
    		CSV.append(a.getActualWorkingHours());
    		CSV.append(a.getRestTime());
    		CSV.append(a.getLateNightTime());
    		CSV.append(a.getRemarks());
    		CSV.append(a.getApproval1());
    		CSV.append(a.getApproval2());

            CSVData =CSV.toString();

    	} catch(Exception e){
            System.out.println(e);
            CSVData = null;
        }
        return new ResponseEntity<>(CSVData.getBytes("utf-8"), h, HttpStatus.OK);
*/

