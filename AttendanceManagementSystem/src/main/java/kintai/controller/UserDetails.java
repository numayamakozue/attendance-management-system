package kintai.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kintai.dao.dto.DepartmentDto;
import kintai.dao.dto.PositionDto;
import kintai.dao.dto.RoleDto;
import kintai.dao.dto.UserDetailsDto;
import kintai.dao.dto.UserDisplayDto;
import kintai.form.UserDetailsForm;
import kintai.form.UserInfoUpdateForm;
import kintai.service.UserDetailsService;
import kintai.service.UserRegisterService;


@Controller
public class UserDetails {

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private HttpSession session;

	//▼ユーザ詳細画面の初期画面を表示する処理
	@RequestMapping(value = "/userDetails/{id}", method = RequestMethod.GET)
	public String userDetails(Model model, @PathVariable int id) {
		//セッションからログインユーザ情報取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");

		//未ログインユーザはログイン画面へ遷移させる
		if (session == null){
			return "redirect:login";
			} else if(loginUser == null) {
				return "redirect:login";
			}

		//編集対象のユーザの情報を取得
		UserDetailsDto userDetails = userDetailsService.getUserDetails(id);
		//管理者以外はログイン画面にリダイレクト
		if(loginUser.getRoleId() == 1 && userDetails.getIs_delete() == 0) {
			model.addAttribute("loginUser", loginUser);
			model.addAttribute("details", userDetails);
			return "userDetails";
		}
		return "redirect:/attendanceRegistration";
	}

	//▼ユーザ詳細画面の編集ボタンを押下してuserDetailsEditを表示する処理
	@RequestMapping(value = "/userDetails/update/{id}", method = RequestMethod.GET)
	public String userDetailsUpdate(Model model, @PathVariable int id) {
		//セッションからログインユーザ情報取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");

		//未ログインユーザはログイン画面へ遷移させる
		if (session == null){
			return "redirect:login";
			} else if(loginUser == null) {
				return "redirect:login";
			}

		//ユーザ編集画面テキストボックス用のユーザ情報を取得
		UserDetailsDto updateBeforeInfo = userDetailsService.getUserDetails(id);
		//[管理者以外,ログインユーザ自身の編集画面に遷移した場合] は勤怠管理画面にリダイレクト
		if(loginUser.getRoleId() == 1 && loginUser.getId() != id && updateBeforeInfo.getIs_delete() == 0) {
			//ユーザ編集画面セレクトボックス用の部署、役職マスタ取得
			List<DepartmentDto> departments = new ArrayList<DepartmentDto>();
			List<PositionDto> positions = new ArrayList<PositionDto>();
			List<RoleDto> roles = new ArrayList<RoleDto>();
			departments = userRegisterService.getDepartment();
			positions = userRegisterService.getPosition();
			roles = userRegisterService.getRole();
			model.addAttribute("departments", departments);
			model.addAttribute("positions", positions);
			model.addAttribute("roles", roles);
			model.addAttribute("userInfoUpdateForm", updateBeforeInfo);
			return "userDetailsEdit";
		}
		return "redirect:/attendanceRegistration";
	}

	//▼UPDATE処理を行ってユーザ詳細画面にリダイレクトする処理
	@RequestMapping(value = "/userDetails/update/{id}/", method = RequestMethod.POST)
	public String userDetailsUpdate(@Valid @ModelAttribute UserInfoUpdateForm form, BindingResult result, Model model) {
		//セッションからログインユーザ情報取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		model.addAttribute("loginUser", loginUser);

		//セッションにセレクトボックスの中身(役職&部署)を格納している
		List<DepartmentDto> departments = new ArrayList<DepartmentDto>();
		List<PositionDto> positions = new ArrayList<PositionDto>();
		List<RoleDto> roles = new ArrayList<RoleDto>();
		departments = userRegisterService.getDepartment();
		positions = userRegisterService.getPosition();
		roles = userRegisterService.getRole();

		//ユーザ編集画面のバリデーションに検知された時の処理
		if(result.hasErrors()) {
			model.addAttribute("departments", departments);
			model.addAttribute("positions", positions);
			model.addAttribute("roles", roles);
			//セッションにエラー用の文字列を格納
			session.setAttribute("userInfoUpdateForm" , form);
			model.addAttribute("error_message","入力エラー");
			return "userDetailsEdit";
		}else {
			try {
			//編集内容をDBに登録するメソッド
			userDetailsService.updateProcess(form);
			//共通ヘッダのユーザ名で編集後の名前を表示するためにセッションからユーザ情報取得
			model.addAttribute("loginUser", loginUser);
			return "redirect:/userDisplay";
			}catch(IllegalArgumentException e){
				model.addAttribute("UserDetailsForm", new UserDetailsForm());
				model.addAttribute("departments", departments);
				model.addAttribute("positions", positions);
				model.addAttribute("roles", roles);
				model.addAttribute("error_message", "不正な値です");
				return "userDetailsEdit";
				//同じ社員番号を登録しようとした場合など
			}catch(DuplicateKeyException e) {
				model.addAttribute("UserDetailsForm" , new UserDetailsForm());
				model.addAttribute("departments", departments);
				model.addAttribute("positions", positions);
				model.addAttribute("roles", roles);
				model.addAttribute("error_message" , "既に存在している社員番号です");
				return "userDetailsEdit";
			//エラーがある場合
			}catch(Exception e) {
				model.addAttribute("UserDetailsForm", new  UserDetailsForm());
				model.addAttribute("departments", departments);
				model.addAttribute("positions", positions);
				model.addAttribute("roles", roles);
				model.addAttribute("error_message", "登録できませんでした");
				return "userDetailsEdit";
			}
		}
	}

	//▼ユーザ詳細画面から削除ボタンを押下してユーザ削除画面へ遷移する処理
//	@RequestMapping(value = "/userDetails/delete/{id}", method = RequestMethod.GET)
//	public String userDetailsDelete(Model model, @PathVariable int id) {
//		//セッションからログインユーザ情報取得
//		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
//		//ユーザ削除画面テキスト用のユーザ情報を取得
//		UserDetailsDto deleteBeforeInfo = userDetailsService.getUserDetails(id);
//		//[管理者以外,自分自身のユーザ情報を削除しようとした場合] は、勤怠管理画面にリダイレクト
//		if(loginUser.getRoleId() == 1 && loginUser.getId() != id && deleteBeforeInfo.getIs_delete() == 0) {
//			model.addAttribute("deleteForm", deleteBeforeInfo);
//			return "userDetailsDelete";
//		}
//		return "redirect:/attendanceRegistration";
//	}

	//▼update処理(論理削除フラグを1にする)を行ってリダイレクトする処理
//	@RequestMapping(value = "/userDetails/delete/{id}/", method = RequestMethod.POST)
//	public String userDetailsDelete(Model model, @ModelAttribute UserInfoDeleteForm form) {
//		userDetailsService.deleteProcess(form.getId());
//		return "redirect:/userDisplay";
//	}
}