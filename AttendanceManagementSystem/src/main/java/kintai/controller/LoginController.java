package kintai.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kintai.dao.dto.UserDisplayDto;
import kintai.form.LoginForm;
import kintai.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	private LoginService loginService;
	@Autowired
	private HttpSession session;


	//ログイン画面からの情報を受け取る
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("loginForm", new LoginForm());
		return "login";
	}


	//ログイン画面で入力したい値を送信
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String getLoginnfo(@Valid @ModelAttribute("loginForm")LoginForm form, BindingResult result, Model model) {
		// ログインエラー
		if (result.hasErrors()) {
			model.addAttribute("errormessage", "認証に失敗しました");
			session.setAttribute("LoginForm", form);
		//ログイン成功
		}else {
					try {
						UserDisplayDto loginUserInfo = loginService.getLoginUser(form.getEmployeeNumber(), form.getPassword());
						session.setAttribute("loginUser", loginUserInfo);

						//削除されたユーザはログインできない
						if (loginUserInfo.getIsDelete() == 1 ) {
							session.setAttribute("errormessage", "認証に失敗しました。");
							return "redirect:/login";
						}
						if(loginUserInfo.getId()==0) {
							return "redirect:/login";
						}else {
							return "redirect:/attendanceRegistration";
						}
						//誤った情報を入力
			}catch(IllegalArgumentException e){
				model.addAttribute("errormessage", "認証に失敗しました");
				System.out.println(e);
				System.out.println(form);
				session.setAttribute("LoginForm", form);
				return "login";
			}catch(Exception e) {
				model.addAttribute("errormessage", "認証に失敗しました");
				System.out.println(e);
				session.setAttribute("LoginForm", form);
				return "login";
			}
		}
		return "login";
	}


	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutFunction(@ModelAttribute LoginForm form, Model model) {
		session.invalidate();
		return "redirect:/login";
	}
}