package kintai.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kintai.dao.dto.DepartmentDto;
import kintai.dao.dto.PositionDto;
import kintai.dao.dto.RoleDto;
import kintai.dao.dto.UserDisplayDto;
import kintai.form.UserRegisterForm;
import kintai.service.UserRegisterService;



@Controller
public class UserRegisterController {
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private HttpSession session;


	@RequestMapping(value = "/userRegister", method = RequestMethod.GET)
	public String userRegister(Model model) {
		//jspのラジオボックスのチェック
		String checkedFlg = "1";
		model.addAttribute("checkedFlg", checkedFlg);
		//ログインしているユーザのセッションを受け取る
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");

		//未ログインユーザはログイン画面へ遷移させる
		if (session == null){
			return "redirect:login";
			} else if(loginUser == null) {
				return "redirect:login";
			}

		//権限判定
		try {
			if(loginUser.getRoleId() != 1) {
				session.setAttribute("errMessage", "権限がありません");
				return "redirect:/attendanceRegistration";
			}else {
				model.addAttribute("loginUser" , loginUser);
			}
			session.removeAttribute("errMessage");
		}catch(Exception e) {
			return "login";
		}
        //フォームをインスタンス化
		UserRegisterForm form = new UserRegisterForm();
		//勤怠区分マスタから値取得
		List<DepartmentDto> departments = new ArrayList<DepartmentDto>();
		List<PositionDto> positions = new ArrayList<PositionDto>();
		List<RoleDto> roles = new ArrayList<RoleDto>();
		departments = userRegisterService.getDepartment(form);
		positions = userRegisterService.getPosition(form);
		roles = userRegisterService.getRole(form);
		//入力情報を受け取り、セッション(フォームはモデル)で保持
	    session.setAttribute("departments", departments);
		session.setAttribute("positions" , positions);
		session.setAttribute("roles" , roles);
		model.addAttribute("userRegisterForm", form);
		return "userRegister";
	}
	@RequestMapping(value = "/userRegister", method = RequestMethod.POST)
	public String userRegister(@Valid @ModelAttribute UserRegisterForm form,BindingResult result, Model model)throws ParseException, NumberFormatException {
		//入力情報にエラー
		if (result.hasErrors()) {
			List<DepartmentDto> departments = new ArrayList<DepartmentDto>();
			List<PositionDto> positions = new ArrayList<PositionDto>();
			List<RoleDto> roles = new ArrayList<RoleDto>();
			departments = userRegisterService.getDepartment(form);
			positions = userRegisterService.getPosition(form);
			roles = userRegisterService.getRole(form);
		    session.setAttribute("departments", departments);
			session.setAttribute("positions" , positions);
			session.setAttribute("roles" , roles);
			session.setAttribute("userRegisterForm", form);
			//model.addAttribute("userRegisterForm", form);
			model.addAttribute("unRegisterMessage", "登録に失敗しました");
			return "userRegister";
			//正しい文字が入力
		}else {
			try {
				//入力に誤り等なければ登録
				userRegisterService.userRegister(form);
				List<DepartmentDto> departments = new ArrayList<DepartmentDto>();
				List<PositionDto> positions = new ArrayList<PositionDto>();
				List<RoleDto> roles = new ArrayList<RoleDto>();
				departments = userRegisterService.getDepartment(form);
				positions = userRegisterService.getPosition(form);
				roles = userRegisterService.getRole(form);
				session.setAttribute("departments", departments);
				session.setAttribute("positions" , positions);
				session.setAttribute("roles" , roles);
				model.addAttribute("userRegisterMessage", "新規ユーザー登録完了!!");
				return "redirect:/userDisplay";
				//不正な引数など渡された場合
			}catch(IllegalArgumentException e){
				List<DepartmentDto> departments = new ArrayList<DepartmentDto>();
				List<PositionDto> positions = new ArrayList<PositionDto>();
				List<RoleDto> roles = new ArrayList<RoleDto>();
				departments = userRegisterService.getDepartment(form);
				positions = userRegisterService.getPosition(form);
				roles = userRegisterService.getRole(form);
				session.setAttribute("departments", departments);
				session.setAttribute("positions" , positions);
				session.setAttribute("roles" , roles);
				session.setAttribute("userRegisterForm", form);
				//model.addAttribute("userRegisterForm", form);
				//model.addAttribute("userRegisterForm", new UserRegisterForm());
				model.addAttribute("unRegisterMessage", "登録できませんでした");
				return "userRegister";
				//同じ社員番号を登録しようとした場合など
			}catch(DuplicateKeyException e) {
				List<DepartmentDto> departments = new ArrayList<DepartmentDto>();
				List<PositionDto> positions = new ArrayList<PositionDto>();
				List<RoleDto> roles = new ArrayList<RoleDto>();
				departments = userRegisterService.getDepartment(form);
				positions = userRegisterService.getPosition(form);
				roles = userRegisterService.getRole(form);
				session.setAttribute("departments", departments);
				session.setAttribute("positions" , positions);
				session.setAttribute("roles" , roles);
				session.setAttribute("userRegisterForm", form);
				//model.addAttribute("userRegisterForm", form);
				//model.addAttribute("userRegisterForm" , new UserRegisterForm());
				model.addAttribute("unRegisterMessage" , "社員番号が重複しています");
				return "userRegister";
				//エラーがある場合
			}catch(Exception e) {
				List<DepartmentDto> departments = new ArrayList<DepartmentDto>();
				List<PositionDto> positions = new ArrayList<PositionDto>();
				List<RoleDto> roles = new ArrayList<RoleDto>();
				departments = userRegisterService.getDepartment(form);
				positions = userRegisterService.getPosition(form);
				roles = userRegisterService.getRole(form);
				session.setAttribute("departments", departments);
				session.setAttribute("positions" , positions);
				session.setAttribute("roles" , roles);
				session.setAttribute("userRegisterForm", form);
				//model.addAttribute("userRegisterForm", new UserRegisterForm());
				//model.addAttribute("userRegisterForm", form);
				model.addAttribute("unRegisterMessage", "登録できませんでした");
				return "userRegister";
			}
		}
	}
}
