package kintai.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import kintai.dao.dto.UserDisplayDto;
import kintai.dao.dto.UserUploadsDto;
import kintai.form.FileUploadForm;
import kintai.service.UserDisplayService;
import kintai.service.UserUploadsService;

@Controller
public class UserUploadsController {

	@ModelAttribute(value = "UploadForm")
	public FileUploadForm initForm(){
		FileUploadForm fileUploadForm = new FileUploadForm();
	        /*
	        初期設定
	        */
	        return fileUploadForm;
	    }

	@Autowired
	private UserDisplayService userDisplayService;
	@Autowired
	private UserUploadsService userUploadsService;
	@Autowired
	private HttpSession session;
	@Autowired
	private HttpServletRequest request;




	@RequestMapping(value = "/useruploads", method = RequestMethod.GET)
    public String userUpload(FileUploadForm uploadForm, Model model) {
		//jspのラジオボックスのチェック
		String checkedFlg = "2";
		model.addAttribute("checkedFlg", checkedFlg);

		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		//未ログインユーザはログイン画面へ遷移させる
		if (session == null){
			return "redirect:login";
			} else if(loginUser == null) {
				return "redirect:login";
			}
		model.addAttribute("loginUser",loginUser);

	    return "userUploads";
	}

	@RequestMapping(value = "/useruploads", method = RequestMethod.POST)
	public String upload(@RequestParam("upload_file") MultipartFile multipartFile,Model model) throws ParseException{
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		BufferedReader br = null ;
		List<String> list = new ArrayList<>();
		List<UserUploadsDto> register = new ArrayList<>();



		try {
			File file = new File(multipartFile.getOriginalFilename());
			InputStream stream = multipartFile.getInputStream();
			//ファイルの拡張子チェック
			if(file.getPath().endsWith(".csv")) {
				Reader reader = new InputStreamReader(stream);
				br = new BufferedReader(reader);
				String line ;
				//CSVファイル内のデータを読み込み
				while((line = br.readLine()) != null){
					UserUploadsDto uploadForm = new UserUploadsDto();
					//listに追加されたCSVのデータを,で区切る
					list = Arrays.asList(line.split(",",-1));

					//区切られているデータ数を確認
					if(list.size() != 8) {
						model.addAttribute("unRegisterMessage", "登録データは8つあります。ファイル内容を確認してください。");
						return "userUploads";
					} else {
						//リスト内の1行目をスキップ
						if(list.get(0).matches("^[^0-9]*$")) {
							continue;
						}
						//リスト内のデータをDtoへ詰め込み
						uploadForm.setId(Integer.parseInt(String.valueOf(list.get(0))));

						List<UserUploadsDto> userEmployeeNumber = userUploadsService.getUserEmployeeNumber();
						//社員番号が重複しているかDBから社員番号と照らし合わせる
						for(int n = 0; n < userEmployeeNumber.size(); n++) {
							if(String.valueOf(userEmployeeNumber.get(n).getEmployeeNumber()).equals(list.get(1))) {
								model.addAttribute("unRegisterMessage" , "重複した社員番号が存在します。");
								return "userUploads";
							}
						}
						if(list.get(1).equals("")) {
							model.addAttribute("unRegisterMessage", "社員番号の記載がないデータがあります。");
							return "userUploads";
						} else if(list.get(1).matches("^[0-9]{6}$")){
						uploadForm.setEmployeeNumber(String.valueOf(list.get(1)));
						} else {
						model.addAttribute("unRegisterMessage", "社員番号が半角数字6桁ではないデータがあります。");
						return "userUploads";
						}

						if(list.get(2).equals("")) {
							model.addAttribute("unRegisterMessage", "名前の記載がないデータがあります。");
							return "userUploads";
						} else if(list.get(2).matches("^[^ぁ-んァ-ン一-龥ー・]{1,30}")){
							model.addAttribute("unRegisterMessage", "名前が全角文字1文字以上30文字以内ではないデータがあります。");
							return "userUploads";
						}
						uploadForm.setName(String.valueOf(list.get(2)));

						//仮パスワードとして"123456"を設定
						uploadForm.setPassword("123456");

						if(list.get(3).equals("")) {
							model.addAttribute("unRegisterMessage", "入社日の記載がないデータがあります。");
							return "userUploads";
						} else if(list.get(3).matches("\\d{4}\\/\\d{2}\\/\\d{2}")){
							model.addAttribute("unRegisterMessage", "入社日の形式が\"YYYY/MM/DD\"ではないデータがあります。");
							return "userUploads";
						}
						//日付をDate型へ変換
						String strDate = String.valueOf(list.get(3));
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
						Date date = dateFormat.parse(strDate);
						uploadForm.setDireDate(date);
						//登録後の表示形式を"yyyy-MM-dd"へ変換
						SimpleDateFormat stringDateFormat = new SimpleDateFormat("yyyy-MM-dd");
						String strDate2 = stringDateFormat.format(date);
						uploadForm.setStringDireDate(strDate2);

						if(list.get(4).equals("")) {
							model.addAttribute("unRegisterMessage", "部署名の記載がないデータがあります。");
							return "userUploads";
						} else if(list.get(4).matches("^[^1-7]{1}")){
							model.addAttribute("unRegisterMessage", "部署名が範囲外の数字を指定しているデータがあります。");
							return "userUploads";
						}
						uploadForm.setDepartmentId(Integer.parseInt(String.valueOf(list.get(4))));

						if(list.get(5).equals("")) {
							model.addAttribute("unRegisterMessage", "役職名の記載がないデータがあります。");
							return "userUploads";
						} else if(list.get(5).matches("^[^1-5]{1}")){
							model.addAttribute("unRegisterMessage", "役職名が範囲外の数字を指定しているデータがあります。");
							return "userUploads";
						}
						uploadForm.setPositionId(Integer.parseInt(String.valueOf(list.get(5))));

						if(list.get(6).equals("")) {
							model.addAttribute("unRegisterMessage", "権限の記載がないデータがあります。");
							return "userUploads";
						} else if(list.get(6).matches("^[^1-2]{1}")){
							model.addAttribute("unRegisterMessage", "権限が範囲外の数字を指定しているデータがあります。");
							return "userUploads";
						}
						uploadForm.setRoleId(Integer.parseInt(String.valueOf(list.get(6))));

						//createUserId,updateUserIdへログインユーザを登録
						uploadForm.setCreateUserId(loginUser.getId());
						uploadForm.setUpdateUserId(loginUser.getId());

						//paid_holidays_remainingが空白なら0を登録
						if(list.get(7).equals("")) {
						uploadForm.setPaidHolidaysRemaining(0);
						} else if(list.get(7) != ""){
							uploadForm.setPaidHolidaysRemaining(Integer.parseInt(String.valueOf(list.get(7))));
						}

						//DBから部署名と役職名を取得する
						userUploadsService.userUploadDepartmentName(uploadForm);
						userUploadsService.userUploadPositionName(uploadForm);
						String department = userUploadsService.getDepartmentName(String.valueOf(uploadForm.getDepartmentId()));
						String position = userUploadsService.getPositionName(String.valueOf(uploadForm.getPositionId()));
						uploadForm.setDepartment(department);
						uploadForm.setPosition(position);

						register.add(uploadForm);
					}
				}
				model.addAttribute("register", register);
				model.addAttribute("userRegisterMessage", register.size() + "名のユーザの登録が完了しました。");

			} else if(file.getPath().equals("")) {
				model.addAttribute("unRegisterMessage" , "ファイルを選択してください。");
				return "userUploads";
			} else if(!(file.getPath().endsWith(".csv"))){
				model.addAttribute("unRegisterMessage" , "ファイル形式は\".csv\"のみ対応しています。");
				return "userUploads";
			}
/*		} catch(DuplicateKeyException e) {
			model.addAttribute("unRegisterMessage" , "重複した社員番号が存在します。");
			return "userUploads";
*/
		} catch (IOException e) {
			model.addAttribute("unRegisterMessage" , register.size() +"名のユーザの登録が完了しましたが、残りのユーザの登録はできませんでした。\r\nファイル内容を確認してください。");
			return "userUploads";

		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					;
				}
			}
		}
		for(int i = 0; i<register.size(); i++) {
			userUploadsService.userUploads(register.get(i));
		}
		return "userUploads";
	}
}

