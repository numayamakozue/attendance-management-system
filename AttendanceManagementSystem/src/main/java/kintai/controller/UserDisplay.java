package kintai.controller;



import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kintai.dao.dto.UserDisplayDto;
import kintai.form.UserForm;
import kintai.service.UserDisplayService;

@Controller
public class UserDisplay {

	@Autowired
	private UserDisplayService userDisplayService;
	@Autowired
	private HttpSession session;
	@Autowired
	private HttpServletRequest request;




	@RequestMapping(value = "/userDisplay", method = RequestMethod.GET)
	public String users(Model model,UserForm userForm, @RequestParam(defaultValue = "0") int pageNumber)  {
		//jspのラジオボックスのチェック
		String checkedFlg = "0";
		model.addAttribute("checkedFlg", checkedFlg);

		// セッションからログインユーザ情報の取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		model.addAttribute("loginUser", loginUser);

		//未ログインユーザはログイン画面へ遷移させる
		if (session == null){
			return "redirect:login";
			} else if(loginUser == null) {
				return "redirect:login";
			}

		//検索の値取得
		String name = request.getParameter("name");
		String employeeNumber = request.getParameter("employeeNumber");
		String isDelete = request.getParameter("isDelete");
		String id = request.getParameter("id");
		String isChangePw = request.getParameter("isChangePw");

		//ページごとのインデックス番号の取得、offsetに送る処理
		String isPrevPage = request.getParameter("isPrevPage");
		String isNextPage = request.getParameter("isNextPage");
		if ("isPrevPage".equals(isPrevPage) && pageNumber > 0){
			pageNumber = pageNumber-1;
		} else if ("isNextPage".equals(isNextPage)) {
			pageNumber = pageNumber+1;
		} else {
			pageNumber = 0;
		}

		int page = pageNumber * 50;

		List<UserDisplayDto> users = userDisplayService.getUsers(employeeNumber, name, isDelete,isChangePw, id, page);
		if(users.size() == 0) {
			pageNumber = 0;
			page = pageNumber * 50;
			users = userDisplayService.getUsers(employeeNumber, name, isDelete,isChangePw, id, page);
		    }

		model.addAttribute("pageNumber", pageNumber);
		session.setAttribute("isDelete", isDelete);
		session.setAttribute("isChangePw", isChangePw);
		model.addAttribute("users", users);


		return "userDisplay";
	}

	@RequestMapping(value = "/restorastion", method = RequestMethod.POST)
	public String restoration(Model model,UserForm userForm )  {
		//セッションからログインユーザ情報取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		model.addAttribute("loginUser", loginUser);

		//取得したIDから削除ユーザーのupdate(復元)処理
//		String id = request.getParameter("id");
		userDisplayService.getUpdate(userForm);

		return "redirect:/userDisplay";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(Model model,UserForm userForm )  {
		//セッションからログインユーザ情報取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		model.addAttribute("loginUser", loginUser);
		//取得したIDから既存ユーザーの論理削除処理
//		String id = request.getParameter("id");
		userDisplayService.getDelete(userForm);

		return "redirect:/userDisplay";
	}

}
