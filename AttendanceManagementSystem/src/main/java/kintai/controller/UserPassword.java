package kintai.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kintai.dao.dto.UserDisplayDto;
import kintai.dao.dto.UserPasswordDto;
import kintai.form.UserPasswordForm;
import kintai.service.UserPasswordService;

@Controller
public class UserPassword {
	@Autowired
	private UserPasswordService userPasswordService;
	@Autowired
	private HttpSession session;
	@Autowired
	private HttpServletRequest request;
	@RequestMapping(value = "/updatePassword", method = RequestMethod.GET)
	public String getUserPassword(Model model,UserPasswordForm form) {
		// セッションからログインユーザ情報の取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");

		//未ログインユーザはログイン画面へ遷移させる
		if (session == null){
			return "redirect:login";
			} else if(loginUser == null) {
				return "redirect:login";
			}

		model.addAttribute("loginUser",loginUser);
		model.addAttribute("UserPasswordForm",form);
		return "updatePassword";
	}

	//▼UPDATE処理を行って勤怠登録画面にリダイレクトする処理
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public String update(@Valid @ModelAttribute UserPasswordForm form, BindingResult result, Model model) {
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");
		session.getAttribute("loginUser");
		//エラー
		if (result.hasErrors()) {
			model.addAttribute("unUpdatePasswordMessage", "変更できませんでした");
			return "updatePassword";
			//更新成功
		}else  {
			try {
				UserDisplayDto userPassword = userPasswordService.getUserPassword(loginUser.getId(), form.getPassword1());
				session.setAttribute("userPassword",userPassword);

				//変更できない場合
				if (userPassword == null) {
					session.setAttribute("errormessage", "変更できませんでした");
					return "updatePassword";

				}else {
					UserPasswordDto dto = new UserPasswordDto();
					BeanUtils.copyProperties(form, dto);
					userPasswordService.update(dto);

					UserDisplayDto changePassword = userPasswordService.getChangePassword(loginUser.getId(), form.getPassword2());
					session.setAttribute("loginUser",changePassword);
					model.addAttribute("updatePasswordMessage", "パスワード変更完了");
					return "redirect:attendanceRegistration";

				}
				//不正な引数など渡された場合
			}catch(IllegalArgumentException e){
				model.addAttribute("userPasswordForm", new UserPasswordForm());
				model.addAttribute("unUpdatePasswordMessage", "変更できませんでした");
				return "updatePassword";
				//入力情報が誤っている場合など
			}catch(DuplicateKeyException e) {
				model.addAttribute("userPasswordForm" , new UserPasswordForm());
				model.addAttribute("unUpdatePasswordMessage", "変更できませんでした");
				return "updatePassword";
				//}catch(Exception e) {
				//model.addAttribute("userPasswordForm" , new UserPasswordForm());
				//model.addAttribute("unUpdatePasswordMessage", "変更できませんでした");
				//return "updatePassword";
			}
		}
	}
}
