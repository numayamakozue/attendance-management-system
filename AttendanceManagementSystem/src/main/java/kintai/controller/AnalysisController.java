package kintai.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kintai.dao.dto.AttendanceDto;
import kintai.dao.dto.UserDisplayDto;
import kintai.form.AttendanceForm;
import kintai.form.SelectMonthForm;
import kintai.form.UserForm;
import kintai.service.RegistrerAttendanceService;

@Controller
public class AnalysisController {

	@Autowired
	private RegistrerAttendanceService registrerAttendanceService;

	@Autowired
	private HttpSession session;

	@Autowired
	private HttpServletRequest request;


	@RequestMapping(value = "/analysis", method = RequestMethod.GET )
	public String analysis(Model model, SelectMonthForm form,UserForm userForm, @RequestParam(defaultValue = "0") int pageNumber) {

		//jspのラジオボックスのチェック
		String checkedFlg = "4";
		model.addAttribute("checkedFlg", checkedFlg);

		// セッションからログインユーザ情報の取得
		UserDisplayDto loginUser = (UserDisplayDto) session.getAttribute("loginUser");

		//未ログインユーザはログイン画面へ遷移させる
		if (session == null){
			return "redirect:login";
			} else if(loginUser == null) {
				return "redirect:login";
			}

		model.addAttribute("loginUser", loginUser);
		List<AttendanceDto> attendances = new ArrayList<AttendanceDto>();
		// カレンダー取得(4月のみ)
		Calendar cal = Calendar.getInstance();
		String mon = String.valueOf(cal.get(Calendar.MONTH)+1);
		model.addAttribute("mon", mon);

		// 月選択プルダウンの表示
		List<SelectMonthForm> selectMonthPullDowns = new ArrayList<SelectMonthForm>();
		int month = cal.get(Calendar.MONTH) +1;


		//分析画面のプルダウンと勤怠画面のプルダウンを分ける

		SelectMonthForm selectMonth1 = (SelectMonthForm) session.getAttribute("selectMonth1");
		if(selectMonth1 != null) {
			month = selectMonth1.getMonthKey();
		}

		if(month > 12) {
			month = 12;
			}
			if(month < 1) {
			month = 1;
			}

		for (int i = 1; i <= 12; ++i) {
			SelectMonthForm selectMonthPullDown = new SelectMonthForm();
			selectMonthPullDown.setId(i);
			selectMonthPullDown.setNowMonth(month);
			selectMonthPullDown.setMonthKey(i);
			selectMonthPullDown.setMonthName("2020年" + i + "月");
			selectMonthPullDowns.add(selectMonthPullDown);
		}
		model.addAttribute("selectMonthPullDowns", selectMonthPullDowns);


		String monthName = String.valueOf(cal.get(Calendar.MONTH) +1);
		int maxDate = cal.getActualMaximum(Calendar.DATE);

		// 選択月の日数取得
		if (selectMonth1 != null) {
			month = selectMonth1.getMonthKey();
			monthName = String.valueOf(selectMonth1.getMonthKey());
			cal.set(Calendar.MONTH, month - 1);
			maxDate = cal.getActualMaximum(Calendar.DATE);
		}

		model.addAttribute("monthName" , monthName);
		//●●●●●calに月初の日付をセット
		cal.set(Calendar.DATE,1);
		//●●●●●Calendar.DAY_OF_WEEKでcalの曜日番号を取得
		int todayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		int day = 0;
		int time = 0;

		List<AttendanceForm> aprilCalendar = new ArrayList<AttendanceForm>();
		for (int i =1; i <= maxDate; ++i) {
			AttendanceForm AttendanceBeen = new AttendanceForm();
			//●●●●●DAY_OF_WEEKは1~7の整数なので、TodayOfWeekが7より大きくなったら、1に初期化
			if (todayOfWeek > 7) {
				todayOfWeek = 1;
			}

			if(todayOfWeek != 1 && todayOfWeek != 7) {
				day += 1;
				time += 8;
			}


			//●●●●●getDayOfTheWeekShort関数にTodayOfWeekを渡す
			String dayOfTheWeek = getDayOfTheWeekShort(todayOfWeek);
			//●●●●●曜日を変える為、プラス1
			todayOfWeek++;


			model.addAttribute("preday", day);
			model.addAttribute("pretime", time);


			AttendanceBeen.setCalendar(monthName+"/" + i + dayOfTheWeek);
			AttendanceBeen.setMatchMonthKey(Integer.parseInt(monthName));
			AttendanceBeen.setMatchDayKey(i);
			aprilCalendar.add(AttendanceBeen);
		}
		int over1 = time + 30;
		int over2 = time + 45;

		model.addAttribute("over1", over1);
		model.addAttribute("over2", over2);


      //勤怠区分の日数の加算

				if(selectMonth1 != null) {
					monthName = String.valueOf(selectMonth1.getMonthKey());
				}
				int monthName1= Integer.parseInt(monthName);
				if (monthName1>=1 && monthName1<= 9) {
		          monthName = 20200 + monthName;
		        } else if (monthName1>= 10 && monthName1<= 12) {
		          monthName = 2020 + monthName;
		        }

				String name = request.getParameter("name");
				String employeeNumber = request.getParameter("employeeNumber");
				String isDelete = request.getParameter("isDelete");
				String sum1 = request.getParameter("sum1");
				String sum2 = request.getParameter("sum2");
				String sum3 = request.getParameter("sum3");
				String sum4 = request.getParameter("sum4");
				String sum5 = request.getParameter("sum5");
				String sum6 = request.getParameter("sum6");
				String sum7 = request.getParameter("sum7");
				String sum8 = request.getParameter("sum8");
				String actual = request.getParameter("actual");
				String late = request.getParameter("late");
				String rest = request.getParameter("rest");


				//ページごとのインデックス番号の取得、offsetに送る処理
				String isPrevPage = request.getParameter("isPrevPage");
				String isNextPage = request.getParameter("isNextPage");
				if ("isPrevPage".equals(isPrevPage) && pageNumber > 0){
					pageNumber = pageNumber-1;
				} else if ("isNextPage".equals(isNextPage)) {
					pageNumber = pageNumber+1;
				} else {
					pageNumber = 0;
				}

				int page = pageNumber * 50;

				session.setAttribute("isDelete", isDelete);

				//各ユーザーの情報

				List<String> size = registrerAttendanceService.getCount1(monthName,sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8, employeeNumber, name, actual, late, rest, page);
				if(size.size() == 0) {
					pageNumber = 0;
					page = pageNumber * 50;
					size = registrerAttendanceService.getCount1(monthName,sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8, employeeNumber, name, actual, late, rest, page);
				    }

				model.addAttribute("size", size);

				SelectMonthForm createUser = new SelectMonthForm();
		        model.addAttribute("selectMonth1",createUser);

		return "analysis";
		}

	@RequestMapping(value = "/selectMonth1", method = RequestMethod.POST)
	public String selectMonth1(@ModelAttribute SelectMonthForm form, Model model) {
		session.setAttribute("selectMonth1", form);

		return "redirect:analysis";
	}


	public static String getDayOfTheWeekShort(int TodayOfWeek) {
		switch (TodayOfWeek) {
		case Calendar.SUNDAY: return "(日)";
		case Calendar.MONDAY: return "(月)";
		case Calendar.TUESDAY: return "(火)";
		case Calendar.WEDNESDAY: return "(水)";
		case Calendar.THURSDAY: return "(木)";
		case Calendar.FRIDAY: return "(金)";
		case Calendar.SATURDAY: return "(土)";
		}
		throw new IllegalStateException();
	}

}
