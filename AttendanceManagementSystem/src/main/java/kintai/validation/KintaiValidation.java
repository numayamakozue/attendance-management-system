package kintai.validation;

import java.util.List;

import kintai.form.AttendanceForm;


public class KintaiValidation {


	//勤怠管理画面における時間のバリデーションチェック
	public static List<String> inputValidation(AttendanceForm form, List<String> message) {



		String remarks = form.getRemarks();
		String hourStartTime1 = form.getStartHourTime();
		String minuteStartTime1 = form.getStartMinuteTime();
		String hourEndTime1 = form.getEndHourTime();
		String minuteEndTime1 = form.getEndMinuteTime();
		String restStartHourTime1 = form.getBreakHourTimeStart();
		String restStartMinuteTime1 = form.getBreakMinuteTimeStart();
		String restEndHourTime1 = form.getBreakHourTimeEnd();
		String restEndMinuteTime1 = form.getBreakMinuteTimeEnd();
		int dutyDivisionId1 = form.getDutyDivisionId();

		if(dutyDivisionId1 == 0) {
			if(!hourStartTime1.isEmpty() || !minuteStartTime1.isEmpty() || !hourEndTime1.isEmpty() || !minuteEndTime1.isEmpty() || !restStartHourTime1.isEmpty() || !restStartMinuteTime1.isEmpty() || !restEndHourTime1.isEmpty() || !restEndMinuteTime1.isEmpty()) {
			message.add("勤務IDを選択してください");
			}
			if(hourStartTime1.isEmpty() && minuteStartTime1.isEmpty() && hourEndTime1.isEmpty() || minuteEndTime1.isEmpty() || restStartHourTime1.isEmpty() || restStartMinuteTime1.isEmpty() || restEndHourTime1.isEmpty() || restEndMinuteTime1.isEmpty()) {
				hourStartTime1 = null;
				minuteStartTime1 = null;
				hourEndTime1 = null;
				minuteEndTime1 = null;
				restStartHourTime1 = null;
				restStartMinuteTime1 = null;
				restEndHourTime1 = null;
				restEndMinuteTime1 = null;

				message.add("勤務IDを選択してください");
			}
			if(message.size() != 0) {
				return message;
		}
		}

		if(dutyDivisionId1 == 1) {

			if(hourStartTime1.isEmpty() || minuteStartTime1.isEmpty()) {
				message.add("始業時間を入力してください");
			}
			if(hourEndTime1.isEmpty() || minuteEndTime1.isEmpty()) {
				message.add("終業時間を入力してください");
			}
			if(restStartHourTime1.isEmpty() || restStartMinuteTime1.isEmpty()) {
				message.add("休憩開始時間を入力してください");
			}
			if(restEndHourTime1.isEmpty() || restEndMinuteTime1.isEmpty()) {
				message.add("休憩終了時間を入力してください");
			}
			if(message.size() != 0) {
				return message;
			}
		}

		if( dutyDivisionId1 == 5) {
			if(hourStartTime1.isEmpty() || minuteStartTime1.isEmpty()) {
				message.add("始業時間を入力してください");
			}
			if(hourEndTime1.isEmpty() || minuteEndTime1.isEmpty()) {
				message.add("終業時間を入力してください");
			}
			if(restStartHourTime1.isEmpty() && restStartMinuteTime1.isEmpty() && restEndHourTime1.isEmpty() || restEndMinuteTime1.isEmpty()) {
				restStartHourTime1 = null;
				restStartMinuteTime1 = null;
				restEndHourTime1 = null;
				restEndMinuteTime1 = null;

				return message;
			}
			if(message.size() != 0) {
					return message;
			}
		}

			// ①備考に51文字以上入力されたときメッセージ表示
		if (remarks.length() > 50) {
			message.add("備考は50文字以下で入力してください");
		}

		if(dutyDivisionId1 == 1 || dutyDivisionId1 == 5) {
			//半角文字チェック
			if (!hourStartTime1.matches("^[0-9]{2}")) {
				message.add("始業時間の「時」は半角数字2桁で入力してください");
			}
			if(!minuteStartTime1.matches("^[0-9]{2}")) {
				message.add("始業時間の「分」は半角数字2桁で入力してください");
			}
			if(!hourEndTime1.matches("^[0-9]{2}")) {
				message.add("終業時間の「時」は半角数字2桁で入力してください");
			}
			if(!minuteEndTime1.matches("^[0-9]{2}")) {
				message.add("終業時間の「分」は半角数字2桁で入力してください");
			}
			if(!restStartHourTime1.matches("^[0-9]{2}")) {
				message.add("休憩開始時間の「時」は半角数字2桁で入力してください");
			}
			if(!restStartMinuteTime1.matches("^[0-9]{2}")) {
				message.add("休憩開始時間の「分」は半角数字2桁で入力してください");
			}
			if(!restEndHourTime1.matches("^[0-9]{2}")) {
				message.add("休憩終了時間の「時」は半角数字2桁で入力してください");
			}
			if(!restEndMinuteTime1.matches("^[0-9]{2}")) {
				message.add("休憩終了時間の「分」は半角数字2桁で入力してください");
			}
			if(message.size() != 0) {
				return message;
			}
		}

		if (dutyDivisionId1 == 2 || dutyDivisionId1 == 3 || dutyDivisionId1 == 4 || dutyDivisionId1 == 6 || dutyDivisionId1 == 7 || dutyDivisionId1 == 8) {
			if(!hourStartTime1.isEmpty() || !minuteStartTime1.isEmpty()) {
				message.add("始業時間を空白にしてください");
			}
			if(!hourEndTime1.isEmpty() || !minuteEndTime1.isEmpty()) {
				message.add("終業時間を空白にしてください");
			}
			if(!restStartHourTime1.isEmpty() || !restStartMinuteTime1.isEmpty()) {
				message.add("休憩開始時間を空白にしてください");
			}
			if(!restEndHourTime1.isEmpty() || !restEndMinuteTime1.isEmpty()) {
				message.add("休憩終了時間を空白にしてください");
			}
			if(message.size() != 0) {
					return message;
			}
			if(hourStartTime1.isEmpty() && minuteStartTime1.isEmpty() && hourEndTime1.isEmpty() && minuteEndTime1.isEmpty() && restStartHourTime1.isEmpty() && restStartMinuteTime1.isEmpty() && restEndHourTime1.isEmpty() && restEndMinuteTime1.isEmpty()) {
				hourStartTime1 = null;
				minuteStartTime1 = null;
				hourEndTime1 = null;
				minuteEndTime1 = null;
				restStartHourTime1 = null;
				restStartMinuteTime1 = null;
				restEndHourTime1 = null;
				restEndMinuteTime1 = null;

				return message;
			}

			if(message.size() != 0) {
					return message;
			}
		}



			int StartHourTime2 = Integer.parseInt(form.getStartHourTime());
			int StartMinuteTime2 = Integer.parseInt(form.getStartMinuteTime());
			int EndHourTime2 = Integer.parseInt(form.getEndHourTime());
			int EndMinuteTime2 = Integer.parseInt(form.getEndMinuteTime());
			int BreakMinuteTimeStart2 = Integer.parseInt(form.getBreakMinuteTimeStart());
			int BreakHourTimeStart2 = Integer.parseInt(form.getBreakHourTimeStart());
			int BreakMinuteTimeEnd2 = Integer.parseInt(form.getBreakMinuteTimeEnd());
			int BreakHourTimeEnd2 = Integer.parseInt(form.getBreakHourTimeEnd());

			if(dutyDivisionId1 == 1 || dutyDivisionId1 == 5) {

			if(StartMinuteTime2 > 59 || EndMinuteTime2 > 59 || BreakMinuteTimeStart2 > 59 || BreakMinuteTimeEnd2 >59) {
				message.add("「分」は00から59以下で入力して下さい");
			}
			if(StartHourTime2 > 23 || EndHourTime2 > 23 || BreakHourTimeStart2 > 23 || BreakHourTimeEnd2 > 23) {
				message.add("「時」は00から23以下で入力して下さい");
			}
			if(message.size() != 0) {
				return message;
			}
			}

			//時間のテスト
			int startSum =  Integer.parseInt(hourStartTime1 + minuteStartTime1);
			int endSum =  Integer.parseInt(hourEndTime1 + minuteEndTime1);
			int breakStartSum =  Integer.parseInt(restStartHourTime1 + restStartMinuteTime1);
			int breakEndSum =  Integer.parseInt(restEndHourTime1 + restEndMinuteTime1);
	//▼下記のコメントは、正常な場合の条件。実際のIF文では異常な場合を検知する条件としているため、正常系の条件とは逆の条件を指定している
//			始業時間のチェック: 始業時間 < 休憩開始時間 && 始業時間 < 休憩終了時間 && 始業時間 < 終業時間
//			終業時間のチェック: 終業時間 > 始業時間 && 終業時間 > 休憩開始時間 && 終業時間 > 休憩終了時間
//			休憩開始時間のチェック: 休憩開始時間 > 始業時間 && 休憩開始時間 < 休憩終了時間 && 休憩開始時間 < 終業時間
//			休憩終了時間のチェック: 休憩終了時間 > 始業時間 && 休憩終了時間 > 休憩開始時間 && 休憩終了時間 < 終業時間

			if(dutyDivisionId1 ==1) {

				if (startSum > breakStartSum || breakEndSum > endSum  || breakStartSum > breakEndSum) {
					message.add("始業時間＜休憩開始時間＜休憩終了時間＜終業時間で入力してください");
				}
			}
			if(dutyDivisionId1==5) {
				if(startSum > endSum) {
					message.add("始業時間＜終業時間で入力してください");
				}
				if(breakStartSum > breakEndSum) {
					message.add("休憩開始時間＜休憩終了時間で入力してください");
				}
			}
			return message;

		}


}