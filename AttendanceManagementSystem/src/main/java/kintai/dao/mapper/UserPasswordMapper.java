package kintai.dao.mapper;

import kintai.dao.dto.UserPasswordDto;
import kintai.dao.entity.UserDisplayEntity;
import kintai.dao.entity.UserPasswordEntity;
import kintai.form.UserPasswordForm;

public interface UserPasswordMapper {

	UserDisplayEntity getUserPassword(int id,  String encPassword1);

	void userPassword(UserPasswordForm form);

	void update(UserPasswordEntity userPassword);

	void update(UserPasswordDto dto);

	UserDisplayEntity getChangePassword(int id,  String encPassword2);
	

}
