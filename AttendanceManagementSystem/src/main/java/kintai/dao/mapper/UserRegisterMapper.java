package kintai.dao.mapper;

import kintai.form.UserRegisterForm;

public interface UserRegisterMapper {
	void userRegister(UserRegisterForm form);

}
