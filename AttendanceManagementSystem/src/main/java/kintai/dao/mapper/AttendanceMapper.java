package kintai.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kintai.dao.entity.AttendanceEntity;

	public interface AttendanceMapper{

		// ログインユーザの勤怠登録情報を取得
		List<AttendanceEntity> getAttendance(int id);

		void insertDuty(AttendanceEntity insertForm);

		void updateDuty(AttendanceEntity updateDuty);

		//void approvalForm1(AttendanceEntity batchApproval);

		//int approvalForm1(AttendanceDto dto);

		//int approvalForm2(AttendanceDto dto);


		void approvalForm2(AttendanceEntity batchApproval);

		void approvalForm1(AttendanceEntity batchApproval);


		List<String> getCount1(@Param(value="monthName")String monthName,
				@Param(value="sum1")String sum1,@Param(value="sum2")String sum2,
				@Param(value="sum3")String sum3,@Param(value="sum4")String sum4,
				@Param(value="sum5")String sum5,@Param(value="sum6")String sum6,
				@Param(value="sum7")String sum7,@Param(value="sum8")String sum8,
				@Param(value="employeeNumber")String employeeNumber,
				@Param(value="name")String name,@Param(value="actual")String actual,@Param(value="late")String late,
				@Param(value="rest")String rest,@Param(value="page") int page);


	}



