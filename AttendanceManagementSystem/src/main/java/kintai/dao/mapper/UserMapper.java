package kintai.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kintai.dao.entity.UserDisplayEntity;
import kintai.form.UserForm;

public interface UserMapper {

	List<UserDisplayEntity>
	getUsers(@Param(value="employeeNumber")String employeeNumber,
			@Param(value="name") String name, @Param(value="isDelete") String isDelete,
			@Param(value="id") String id,  @Param(value="isChabgePw") String isChangePw,
			@Param(value="page") int page);

	UserDisplayEntity getLoginUser(String employeeNumber, String password);

	int getUpdate(UserForm form);
	int getDelete(UserForm form);

}
