package kintai.dao.mapper;

import kintai.dao.entity.UserDetailsEntity;
import kintai.form.UserInfoUpdateForm;

public interface UserDetailsMapper {
    UserDetailsEntity getUserDetails(int id);


    int updateProcess(UserInfoUpdateForm form);

    int deleteProcess(int id);

}