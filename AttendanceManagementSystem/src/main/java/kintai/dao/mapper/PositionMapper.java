package kintai.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kintai.dao.dto.UserUploadsDto;
import kintai.dao.entity.PositionEntity;

public interface PositionMapper {
	List<PositionEntity> getPosition();

	List<PositionEntity> getPositionName(UserUploadsDto uploadForm);

	PositionEntity getPositionName(@Param(value="id")String positionId);
}