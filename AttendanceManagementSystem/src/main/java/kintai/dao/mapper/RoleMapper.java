package kintai.dao.mapper;

import java.util.List;

import kintai.dao.entity.RoleEntity;

public interface RoleMapper {
	List<RoleEntity> getRole();
}
