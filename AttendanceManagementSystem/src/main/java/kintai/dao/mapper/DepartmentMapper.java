package kintai.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kintai.dao.dto.UserUploadsDto;
import kintai.dao.entity.DepartmentEntity;

public interface DepartmentMapper {
	List<DepartmentEntity> getDepartment();

	List<DepartmentEntity> getDepartmentName(UserUploadsDto uploadForm);

	DepartmentEntity getDepartmentName(@Param(value="id")String DepartmentId);

}