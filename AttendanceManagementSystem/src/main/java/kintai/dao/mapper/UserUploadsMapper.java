package kintai.dao.mapper;

import java.util.List;

import kintai.dao.dto.UserUploadsDto;
import kintai.dao.entity.UserDisplayEntity;

public interface UserUploadsMapper {
	void userUploads(UserUploadsDto userUploads);

	List<UserDisplayEntity> getUserEmployeeNumber();
}
