package kintai.dao.mapper;

import java.util.List;

import kintai.dao.entity.DutyDivisionEntity;

public interface DutyDivisionMapper {

	List<DutyDivisionEntity> getDutyDivision();

}
