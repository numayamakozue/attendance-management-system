package kintai.dao.entity;

public class DutyDivisionEntity {

	private int id;
	private String DutyDivision;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDutyDivision() {
		return DutyDivision;
	}
	public void setDutyDivision(String dutyDivision) {
		DutyDivision = dutyDivision;
	}
}
