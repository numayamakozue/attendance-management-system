package kintai.dao.entity;

public class UserPasswordEntity {

	private int id;
	private int isChangePw;
	
	private String  password1;
	private String  encPassword1;
	private String  password2;
	private String  encPassword2;
	
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	public String getEncPassword2() {
		return encPassword2;
	}
	public void setEncPassword2(String encPassword2) {
		this.encPassword2 = encPassword2;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	
	

	public String getEncPassword1() {
		return encPassword1;
	}
	public void setEncPassword1(String encPassword1) {
		this.encPassword1 = encPassword1;
	}
	public int getIsChangePw() {
		return isChangePw;
	}
	public void setIsChangePw(int isChangePw) {
		this.isChangePw = isChangePw;
	}
	
}
