package kintai.dao.entity;

import java.util.Date;

public class AttendanceEntity {

	private int id;
	private String employeeNumber;
	private String name;
	private int dutyDivisionId;
	private String startTime;
	private String endTime;
	private String breakTimeStart;
	private String breakTimeEnd;
	private double actualWorkingHours;
	private double lateNightTime;
	private double restTime;
	private String remarks;
	private boolean approval1;
	private boolean approval2;
	private int createUser;
	private int updateUser;
	private Date createDate;
	private Date updateDate;
	private String dutyDivision;
	private String calendar;
	private int matchDayKey;
	private int matchMonthKey;
	private double getCount1;
	private String sum1;
	private String sum2;
	private String sum3;
	private String sum4;
	private String sum5;
	private String sum6;
	private String sum7;
	private String sum8;
	private String actual;
	private String late;
	private String rest;

	public boolean getApproval2() {
		return approval2;
	}
	public void setApproval2(boolean approval2) {
		this.approval2 = approval2;
	}
	public boolean getApproval1() {
		return approval1;
	}
	public void setApproval1(boolean approval1) {
		this.approval1 = approval1;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDutyDivisionId() {
		return dutyDivisionId;
	}
	public void setDutyDivisionId(int dutyDivisionId) {
		this.dutyDivisionId = dutyDivisionId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getBreakTimeStart() {
		return breakTimeStart;
	}
	public void setBreakTimeStart(String breakTimeStart) {
		this.breakTimeStart = breakTimeStart;
	}
	public String getBreakTimeEnd() {
		return breakTimeEnd;
	}
	public void setBreakTimeEnd(String breakTimeEnd) {
		this.breakTimeEnd = breakTimeEnd;
	}
	public double getActualWorkingHours() {
		return actualWorkingHours;
	}
	public void setActualWorkingHours(double actualWorkingHours) {
		this.actualWorkingHours = actualWorkingHours;
	}
	public double getLateNightTime() {
		return lateNightTime;
	}
	public void setLateNightTime(double lateNightTime) {
		this.lateNightTime = lateNightTime;
	}
	public double getRestTime() {
		return restTime;
	}
	public void setRestTime(double restTime) {
		this.restTime = restTime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public int getCreateUser() {
		return createUser;
	}
	public void setCreateUser(int createUser) {
		this.createUser = createUser;
	}
	public int getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(int updateUser) {
		this.updateUser = updateUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getDutyDivision() {
		return dutyDivision;
	}
	public void setDutyDivision(String dutyDivision) {
		this.dutyDivision = dutyDivision;
	}
	public String getCalendar() {
		return calendar;
	}
	public void setCalendar(String calendar) {
		this.calendar = calendar;
	}
	public int getMatchDayKey() {
		return matchDayKey;
	}
	public void setMatchDayKey(int matchDayKey) {
		this.matchDayKey = matchDayKey;
	}
	public int getMatchMonthKey() {
		return matchMonthKey;
	}
	public void setMatchMonthKey(int matchMonthKey) {
		this.matchMonthKey = matchMonthKey;
	}

	public double getGetCount1() {
		return getCount1;
	}
	public void setGetCount1(double getCount1) {
		this.getCount1 = getCount1;
	}

	public String getSum1() {
		return sum1;
	}
	public void setSum1(String sum1) {
		this.sum1 = sum1;
	}

	public String getSum2() {
		return sum2;
	}
	public void setSum2(String sum2) {
		this.sum2 = sum2;
	}
	public String getSum3() {
		return sum3;
	}
	public void setSum3(String sum3) {
		this.sum3 = sum3;
	}
	public String getSum4() {
		return sum4;
	}
	public void setSum4(String sum4) {
		this.sum4 = sum4;
	}
	public String getSum5() {
		return sum5;
	}
	public void setSum5(String sum5) {
		this.sum5 = sum5;
	}
	public String getSum6() {
		return sum6;
	}
	public void setSum6(String sum6) {
		this.sum6 = sum6;
	}
	public String getSum7() {
		return sum7;
	}
	public void setSum7(String sum7) {
		this.sum7 = sum7;
	}
	public String getSum8() {
		return sum8;
	}
	public void setSum8(String sum8) {
		this.sum8 = sum8;
	}
	public String getActual() {
		return actual;
	}
	public void setActual(String actual) {
		this.actual = actual;
	}

	public String getLate() {
		return late;
	}
	public void setLate(String late) {
		this.late = late;
	}

	public String getRest() {
		return rest;
	}
	public void setRest(String rest) {
		this.rest = rest;
	}


}


