package kintai.dao.dto;

public class DutyDivisionDto {

	private int id;
	private String dutyDivision;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDutyDivision() {
		return dutyDivision;
	}
	public void setDutyDivision(String dutyDivision) {
		this.dutyDivision = dutyDivision;
	}


}
