package kintai.dao.dto;

import java.util.Date;

public class UserUploadsDto {

	private int id;
	private String employeeNumber;
	private String name;
	private String password;
	private Date direDate;
	private String stringDireDate;
	private int departmentId;
	private int positionId;
	private int roleId;
	private int paidHolidaysRemaining;
	private String department;
	private String position;
	private int createUserId;
	private int updateUserId;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDireDate() {
		return direDate;
	}
	public void setDireDate(Date direDate) {
		this.direDate = direDate;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getPaidHolidaysRemaining() {
		return paidHolidaysRemaining;
	}
	public void setPaidHolidaysRemaining(int paidHolidaysRemaining) {
		this.paidHolidaysRemaining = paidHolidaysRemaining;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}
	public int getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}
	public String getStringDireDate() {
		return stringDireDate;
	}
	public void setStringDireDate(String stringDireDate) {
		this.stringDireDate = stringDireDate;
	}

}


