package kintai.dao.dto;

import java.sql.Date;

public class UserDisplayDto {
	private int id;
	private String employeeNumber;
	private String password;
	private String name;
	private Date direDate;
	private int departmentId;
	private int positionId;
	private int roleId;
	private int isDelete;
	private int paidHolidaysRemaining;
	private int createUserId;
	private int isChangePw;
	private Date createAt;
	private int updateUserId;
	private Date updateAt;
	private int mDepartmentId;
	private String department;
	private int loginUser;

	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	public int getmPositionId() {
		return mPositionId;
	}
	public void setmPositionId(int mPositionId) {
		this.mPositionId = mPositionId;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	private int mPositionId;
	private String position;

	public int getmDepartmentId() {
		return mDepartmentId;
	}
	public void setmDepartmentId(int mDepartmentId) {
		this.mDepartmentId = mDepartmentId;
	}

	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDireDate() {
		return direDate;
	}
	public void setDireDate(Date direDate) {
		this.direDate = direDate;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getPaidHolidaysRemaining() {
		return paidHolidaysRemaining;
	}
	public void setPaidHolidaysRemaining(int paidHolidaysRemaining) {
		this.paidHolidaysRemaining = paidHolidaysRemaining;
	}
	public int getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public int getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}
	public int getIsChangePw() {
		return isChangePw;
	}
	public void setIsChangePw(int isChangePw) {
		this.isChangePw = isChangePw;
	}
	public int getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(int loginUser) {
		this.loginUser = loginUser;
	}

}
