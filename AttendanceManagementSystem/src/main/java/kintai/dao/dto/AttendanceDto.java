package kintai.dao.dto;

import java.util.Date;

public class AttendanceDto {

	private int id;
	private int dutyDivisionId;
	private String startHourTime;
	private String startMinuteTime;
	private String endHourTime;
	private String endMinuteTime;
	private String breakHourTimeStart;
	private String breakMinuteTimeStart;
	private String breakHourTimeEnd;
	private String breakMinuteTimeEnd;
	private double actualWorkingHours;
	private double lateNightTime;
	private double restTime;
	private String remarks;
	private boolean approval1;
	private boolean approval2;
	private int createUser;
	private int updateUser;
	private Date createDate;
	private Date updateDate;
	private String dutyDivision;
	private String calendar;
	private int matchYearKey;
	private int matchDayKey;
	private int matchMonthKey;
	private int matchDayOfWeek;
	private String startTime;
	private double getCount1;

	public boolean getApproval1() {
		return approval1;
	}
	public void setApproval1(boolean approval1) {
		this.approval1 = approval1;
	}
	public int getMatchDayOfWeek() {
		return matchDayOfWeek;
	}
	public void setMatchDayOfWeek(int matchDayOfWeek) {
		this.matchDayOfWeek = matchDayOfWeek;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDutyDivisionId() {
		return dutyDivisionId;
	}
	public void setDutyDivisionId(int dutyDivisionId) {
		this.dutyDivisionId = dutyDivisionId;
	}
	public String getStartHourTime() {
		return startHourTime;
	}
	public void setStartHourTime(String startHourTime) {
		this.startHourTime = startHourTime;
	}
	public String getStartMinuteTime() {
		return startMinuteTime;
	}
	public void setStartMinuteTime(String startMinuteTime) {
		this.startMinuteTime = startMinuteTime;
	}
	public String getEndHourTime() {
		return endHourTime;
	}
	public void setEndHourTime(String endHourTime) {
		this.endHourTime = endHourTime;
	}
	public String getEndMinuteTime() {
		return endMinuteTime;
	}
	public void setEndMinuteTime(String endMinuteTime) {
		this.endMinuteTime = endMinuteTime;
	}
	public String getBreakHourTimeStart() {
		return breakHourTimeStart;
	}
	public void setBreakHourTimeStart(String breakHourTimeStart) {
		this.breakHourTimeStart = breakHourTimeStart;
	}
	public String getBreakMinuteTimeStart() {
		return breakMinuteTimeStart;
	}
	public void setBreakMinuteTimeStart(String breakMinuteTimeStart) {
		this.breakMinuteTimeStart = breakMinuteTimeStart;
	}
	public String getBreakHourTimeEnd() {
		return breakHourTimeEnd;
	}
	public void setBreakHourTimeEnd(String breakHourTimeEnd) {
		this.breakHourTimeEnd = breakHourTimeEnd;
	}
	public String getBreakMinuteTimeEnd() {
		return breakMinuteTimeEnd;
	}
	public void setBreakMinuteTimeEnd(String breakMinuteTimeEnd) {
		this.breakMinuteTimeEnd = breakMinuteTimeEnd;
	}
	public double getActualWorkingHours() {
		return actualWorkingHours;
	}
	public void setActualWorkingHours(double actualWorkingHours) {
		this.actualWorkingHours = actualWorkingHours;
	}
	public double getLateNightTime() {
		return lateNightTime;
	}
	public void setLateNightTime(double lateNightTime) {
		this.lateNightTime = lateNightTime;
	}
	public double getRestTime() {
		return restTime;
	}
	public void setRestTime(double restTime) {
		this.restTime = restTime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public boolean getApproval2() {
		return approval2;
	}
	public void setApproval2(boolean approval2) {
		this.approval2 = approval2;
	}
	public int getCreateUser() {
		return createUser;
	}
	public void setCreateUser(int createUser) {
		this.createUser = createUser;
	}
	public int getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(int updateUser) {
		this.updateUser = updateUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getDutyDivision() {
		return dutyDivision;
	}
	public void setDutyDivision(String dutyDivision) {
		this.dutyDivision = dutyDivision;
	}
	public String getCalendar() {
		return calendar;
	}
	public void setCalendar(String calendar) {
		this.calendar = calendar;
	}
	public int getMatchDayKey() {
		return matchDayKey;
	}
	public void setMatchDayKey(int matchDayKey) {
		this.matchDayKey = matchDayKey;
	}
	public int getMatchMonthKey() {
		return matchMonthKey;
	}
	public void setMatchMonthKey(int matchMonthKey) {
		this.matchMonthKey = matchMonthKey;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public double getGetCount1() {
		return getCount1;
	}
	public void setGetCount1(double getCount1) {
		this.getCount1 = getCount1;
	}
	public int getMatchYearKey() {
		return matchYearKey;
	}
	public void setMatchYearKey(int matchYearKey) {
		this.matchYearKey = matchYearKey;
	}




}


