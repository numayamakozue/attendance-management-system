package kintai.form;

import java.sql.Timestamp;

public class UserDetailsForm {
	private String  password;
	private String employeeNumber;
	private String name;
	private int id;
	private Timestamp dire_date;
	private String department;
	private String position;

	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Timestamp getDire_date() {
		return dire_date;
	}
	public void setDire_date(Timestamp dire_date) {
		this.dire_date = dire_date;
	}

}
