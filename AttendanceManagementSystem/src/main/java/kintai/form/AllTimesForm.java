package kintai.form;

public class AllTimesForm {

	private double allActualWorkingTime;	// 合計勤務時間
	private double allNightTime;			// 深夜
	private double allRestTime;				// 休憩時間
	private double allWorkingDay;			// 合計勤務日数
											// ※ 有給数はセッションから取得
	public double getAllActualWorkingTime() {
		return allActualWorkingTime;
	}
	public void setAllActualWorkingTime(double allActualWorkingTime) {
		this.allActualWorkingTime = allActualWorkingTime;
	}
	public double getAllNightTime() {
		return allNightTime;
	}
	public void setAllNightTime(double allNightTime) {
		this.allNightTime = allNightTime;
	}
	public double getAllRestTime() {
		return allRestTime;
	}
	public void setAllRestTime(double allRestTime) {
		this.allRestTime = allRestTime;
	}
	public double getAllWorkingDay() {
		return allWorkingDay;
	}
	public void setAllWorkingDay(double allWorkingDay) {
		this.allWorkingDay = allWorkingDay;
	}
	
	

}
