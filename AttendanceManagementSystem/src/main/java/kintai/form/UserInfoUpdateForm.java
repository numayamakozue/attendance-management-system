package kintai.form;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

public class UserInfoUpdateForm {
	//6桁の整数を検知
	@NotBlank(message = "社員番号を入力してください")
	@Pattern(regexp = "^[0-9]{6}",message="社員番号は半角数字6文字で入力してください\r\n")
	private String employeeNumber;
	//空白もしくはNULLを検知
	@NotBlank(message = "名前は必須です")
	@Pattern(regexp="^[ぁ-んァ-ン一-龥ー・]{1,30}",message="名前は全角文字1文字以上30文字以内で入力してください\r\n")
	private String name;
	//6文字以上20文字以下の半角英数字(記号を含む)を検知
	@NotBlank(message = "パスワードを入力してください")
	@Pattern(regexp = "^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}",message="パスワードは半角英数字(記号含む)6文字以上20文字以内で入力してください\r\n")
	private String  password;
	@NotNull(message="役職を選択してください")
	@DecimalMin(value = "1", message="役職を選択してください")
	@DecimalMax(message="役職を選択してください", value = "5")
	private int  position_id;
	//NULLを検知
	@NotNull
	//1以上7以下の数値を検知
	@DecimalMin(value = "1", message="部署を選択してください")
	@DecimalMax(value = "7", message="部署を選択してください")
	private int department_id;
	@NotBlank(message="入社日を入力してください")
	@Pattern(regexp="([0-9]{4}-[0-9]{1,2}-[0-9]{1,2})|{0}", message="入社日はyyyy-mmm-ddの形式で入力してください")
	private String dire_date;
	private int id;
	private int loginUser;

/*	@NotNull
	//1以上2以下の数値を検知
	@DecimalMin(value = "1", message="権限を選択してください")
	@DecimalMax(value = "2", message="権限を選択してください")
*/
	private int role_id;

	public String getDire_date() {
		return dire_date;
	}
	public void setDire_date(String dire_date) {
		this.dire_date = dire_date;
	}

	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}
	public int getPosition_id() {
		return position_id;
	}
	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(int loginUser) {
		this.loginUser = loginUser;
	}
}
