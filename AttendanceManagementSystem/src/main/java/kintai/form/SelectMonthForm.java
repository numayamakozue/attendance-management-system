package kintai.form;

import java.util.Date;

public class SelectMonthForm {

	private int id;				// valueに使用
	private int nowMonth;		// 現在の月（例：4）
	private int monthKey;		// 選択した月名に使用（例：4）
	private String monthName;	// プルダウンんに表示する名称に使用（例：2020年4月）
	private int createUser;

	private int nowMonth2;		// 現在の月（例：4）
	private Date monthKey2;		// 選択した月名に使用（例：4）
	private String monthName2;
	private int nowYear;		// 現在の年（例：2020）
	private int yearKey;		// 選択した年に使用（例：2020）


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNowMonth() {
		return nowMonth;
	}
	public void setNowMonth(int nowMonth) {
		this.nowMonth = nowMonth;
	}
	public int getMonthKey() {
		return monthKey;
	}
	public void setMonthKey(int monthKey) {
		this.monthKey = monthKey;
	}
	public String getMonthName() {
		return monthName;
	}
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}
	public int getCreateUser() {
		return createUser;
	}
	public void setCreateUser(int createUser) {
		this.createUser = createUser;
	}
	public int getNowMonth2() {
		return nowMonth2;
	}
	public void setNowMonth2(int nowMonth2) {
		this.nowMonth2 = nowMonth2;
	}
	public Date getMonthKey2() {
		return monthKey2;
	}
	public void setMonthKey2(Date monthKey2) {
		this.monthKey2 = monthKey2;
	}
	public String getMonthName2() {
		return monthName2;
	}
	public void setMonthName2(String monthName2) {
		this.monthName2 = monthName2;
	}
	public int getNowYear() {
		return nowYear;
	}
	public void setNowYear(int nowYear) {
		this.nowYear = nowYear;
	}
	public int getYearKey() {
		return yearKey;
	}
	public void setYearKey(int yearKey) {
		this.yearKey = yearKey;
	}
}
