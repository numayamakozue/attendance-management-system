package kintai.form;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

public class UserPasswordForm {
	
	@NotBlank(message = "現在のパスワードを入力してください")
	@Pattern(regexp = "^[a-zA-Z0-9 -~]{6,20}",message="パスワードは半角数字6文字以上20文字以内で入力してください")
	//@Memo(messege="現在のパスワードが一致していません。正しく入力内容を確認してください。")
	private String password1;
	@NotBlank(message = "変更用パスワードを入力してください")
	@Pattern(regexp = "^[a-zA-Z0-9 -~]{6,20}",message="パスワードは全角文字6文字以上20文字以内で入力してください")
	private String  password2;
	
	private int id;
	private int isChangePw;
	
	
	
	
	public int getIsChangePw() {
		return isChangePw;
	}
	public void setIsChangePw(int isChangePw) {
		this.isChangePw = isChangePw;
	}
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
