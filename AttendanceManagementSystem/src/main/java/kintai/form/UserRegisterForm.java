package kintai.form;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

public class UserRegisterForm {
	private int id;
	@NotBlank(message = "社員番号を入力してください")
	@Pattern(regexp = "^[0-9]{6}",message="社員番号は半角数字6文字で入力してください\r\n")
	private String employeeNumber;
	@NotBlank(message = "パスワードを入力してください")
	@Pattern(regexp = "^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}",message="パスワードは半角英数字(記号含む)6文字以上20文字以内で入力してください\r\n")
	private String  password;
	@NotBlank(message = "名前は必須です")
	@Pattern(regexp="^[ぁ-んァ-ン一-龥ー・]{1,30}",message="名前は全角文字1文字以上30文字以内で入力してください")
	private String name;
	private String direDate;
	private int departmentId;
	private int  positionId;
	private int roleId;
	private int createUserId;
	private int updateUserId;
	public int getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}
	public int getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDireDate() {
		return direDate;
	}
	public void setDireDate(String direDate) {
		this.direDate = direDate;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
}
