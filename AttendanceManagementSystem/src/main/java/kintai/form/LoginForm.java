package kintai.form;

import java.sql.Date;

import org.hibernate.validator.constraints.NotBlank;

public class LoginForm {
	private int id;
	//社員番号に何も入っていない場合
	@NotBlank(message ="認証に失敗しました")
	private String employeeNumber;
	//パスワードに何も入っていない場合
    @NotBlank(message ="認証に失敗しました")
	private String password;
	private String name;
	private Date direDate;
	private int departmentId;
	private int positionId;
	private int roleId;
	private boolean isDelete;
	private boolean isChangePw;
	
	private int paidHolidaysRemaining;
	private int createUserId;
	private Date createAt;
	private int updateUserId;
	private Date updateAt;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDireDate() {
		return direDate;
	}
	public void setDireDate(Date direDate) {
		this.direDate = direDate;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public int getPaidHolidaysRemaining() {
		return paidHolidaysRemaining;
	}
	public void setPaidHolidaysRemaining(int paidHolidaysRemaining) {
		this.paidHolidaysRemaining = paidHolidaysRemaining;
	}
	public int getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public int getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}
	public boolean isChangePw() {
		return isChangePw;
	}
	public void setChangePw(boolean isChangePw) {
		this.isChangePw = isChangePw;
	}
	
}
